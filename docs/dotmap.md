# Dot Map

This map is a temporal Dot Map, i.e., for each moment in time has a thematic map where each point is drawn with the associated color and/or shape.

Visual variable mappings:

* A numerical variable can be mapped to a sequencial color scheme through a set of classes supplied or computed using any of the available methods;
* Categorical variable can be mapped to a categorical color scheme or to a set of finite shapes;
* It is also possible to simultaneously have two variables: one numerical mapped to color and one categorical mapped to shapes; or two categorical, one to color and the other using shapes.

Example 1: 

Source: 2015 Street Tree Census - Tree Data [LINK](https://data.cityofnewyork.us/Environment/2015-Street-Tree-Census-Tree-Data/pi5s-9p35/data)

The tree status is mapped to a qualitative color scheme (provided by colorbrewer) through a set of categories (Alive, Stump, Dead).

## **Image** 

![I](dotmap.png)

## **Code**
```js
let parsingOptions = {
    variableDeclarations: [
        {
            internalName: 'Latitude da arvore',
            externalName: 'Latitude'
        },
        {
            internalName: 'Longitude da arvore',
            externalName: 'longitude'
        },
        {
            internalName: 'Tree Status',
            externalName: 'status'
        }
    ],
    urls: {
        dataURL: 'nytrees'
    }
};
let variableUsage = {
    spatialInformation: {
        longitude: { internalName: 'Longitude da arvore' },
        latitude: { internalName: 'Latitude da arvore' }
    },
    thematicInformation: {
        color: {
            internalName: 'Tree Status',
            mappingMethod: 'colorbrewer-qualitative'
        }
    }
};

let globalOptionsNYCity = {
    bounds: {
        ny: {
            NE: { lng: -74.30809, lat: 40.604569 },
            SW: { lng: -73.727188, lat: 40.938414 }
        }
    },
    container: 'gisplay2',
    provider: 'GM',
    showLoader: true
};

Gisplay.makeDotMap(parsingOptions, variableUsage, globalOptionsNYCity);
}
```
