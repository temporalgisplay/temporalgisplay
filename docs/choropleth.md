# Choropleth

This map is a temporal Choropleth, i.e., for each moment in time has a thematic map where each polygon is plotted with the associated color and/or a pattern.

Visual variable mappings:

* A numerical variable can be mapped to a sequencial color scheme through a set of classes supplied or computed using any of the available methods;
* Categorical variable can be mapped to a categorical color scheme or to finite set of filling patterns;
* It is also possible to simultaneously have two variables: one numerical mapped to color and on categorical mapped do patterns; or two categorical, one to color and the other using patterns.

Example 1: Worldwide employment rate in percent for population 14-25 years old.

Source: Gapminder world data

The employment rate in percent mapped to a sequencial color scheme (provided by colorbrewer) through a set of classes for the quantiles.

## **Image** 

![I](choropleth.png)

## **Code**

```js
let parsingOptions = {
    variableDeclarations: [
        {
            internalName: 'Employment 15 to 24 yo (%)',
            externalName: 'aged_15_24_employment_rate_percent'
        },
        {
            internalName: 'Country',
            externalName: 'geo',
        },
        {
            internalName: 'Ano',
            externalName: 'time',
        }
    ],
    urls: {
        dataURL: 'employmentFileCSV',
        geospatialURL: 'worldFile2',
        idOnDataURL: 'geo',
        idOnGeoSpatialURL: 'ISO_A3'
    }
};
let variableUsage = { 
    temporalInformation: {
        internalName: 'Ano',
        granularity: 'year'
    },
    thematicInformation: {
        color: {
            internalName: 'Employment 15 to 24 yo (%)',
            classBreaksMethod: 'quantiles',
            mappingMethod: 'colorbrewer-sequential'
        }
    }
};

let globalOptionsWorld = {
    bounds: {
        world: {
            NE: { lng: 180, lat: 90 },
            SW: { lng: -180, lat: -90 }
        }
    },
    container: 'gisplay2',
    provider: 'GM',
    showLoader: true
};
Gisplay.makeChoropleth(parsingOptions, variableUsage, globalOptionsWorld);
```