# Proportional Symbols

This map is a temporal Proportional Symbols, i.e., for each moment in time has a thematic map where each point is plotted with the associated size and color.

Visual variable mappings:

* A numerical variable can be mapped to a sequencial color scheme or size through a set of classes supplied or computed using any of the available methods;
* It is also possible to simultaneously have two variables: one numerical or categorical mapped to color and another numerical mapped to size.

Example 1: 

Source: USA Accidents by county

The accidents are mapped to it's size through a set of classes for the quantiles.

## **Image** 

![I](propsymbols.png)

## **Code**

```js
let parsingOptions = {
    variableDeclarations: [
        {
            internalName: 'Number of deaths',
            externalName: 'f1'
        },
        {
            internalName: 'State',
            externalName: 'f2',
        },
        {
            internalName: 'County',
            externalName: 'f3',
        }
    ],
    urls: {
        dataURL: 'fileCentroid',
    }
};
let mappingOptions = {
    thematicInformation: {
        size: {
            internalName: 'Number of deaths',
            mapping: [30, 50, 100],
            classBreaksMethod: 'quantiles'
        }
    }
};

let globalOptionsUSAOneBound = {
    bounds: {
        usa: { 
            NE: { lng: -57.578144, lat: 71.521815 },
            SW: { lng: -175.351582, lat: 4.205206 }
        }
    },
    container: 'gisplay2',
    provider: 'HM',
    showLoader: true
};
Gisplay.makeProportionalSymbolsMap(parsingOptions, mappingOptions, globalOptionsUSAOneBound);
```