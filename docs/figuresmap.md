# Figures Map

This map is a temporal Figures Map, i.e., for each moment in time has a thematic map where each point is plotted with the associated color and/or size and/or orientation.

Visual variable mappings:

* A numerical variable can be mapped to a sequencial color scheme through a set of classes supplied or computed using any of the available methods;
* A numerical variable can be mapped to a set of orientation values;
* Categorical variable can be mapped to a categorical color scheme or to a set of orientation values;
* It is also possible to simultaneously have two or three variables, using combinations of the three available visual variables.

Example 1: 

Source: 


![I]()

```js

```