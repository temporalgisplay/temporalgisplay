# Lines Map

This map is a temporal Lines Map, i.e., for each moment in time has a thematic map where each line is plotted with the associated color.

Visual variable mappings:

* A numerical variable can be mapped to a sequencial color scheme through a set of classes supplied or computed using any of the available methods;
* Categorical variable can be mapped to a categorical color scheme;

Example 1: 

Source: World Roads

Each road is mapped to a categorical color scheme (provided by colorbrewer).

## **Image** 


![](linesmap.png)


## **Code**


```js
let parsingOptions = {
    variableDeclarations: [
        {
            internalName: 'Tipo de via',
            externalName: 'type',
        }
    ],
    urls: {
        dataURL: 'worldLines',
    }
};
let variableUsage = {
    thematicInformation: {
        color: {
            internalName: 'Tipo de via',
            mappingMethod: 'colorbrewer-qualitative'
        }
    }
};

let globalWorldOpts = {
    bounds: {
        world: { 
            NE: { lng: 180, lat: 90 },
            SW: { lng: -180, lat: -90 }
        }
    },
    container: 'gisplay2',
    provider: 'MBGL',
    showLoader: true
};
Gisplay.makeLinesMap(parsingOptions, variableUsage, globalWorldOpts);
```