# **Options**

There are four possible kinds of options:

1. Parsing options, this includes the declaration of useful variables and urls where to get the data
2. Variable usage, which tells the API how to use the declared variables
3. Global options, like background map provider and layout
4. Map options, which are options only related to the map that we want to create

## 1.Parsing options:

The parsing options are split into three categories:

* variableDeclarations - Declare all variables to be used. Given an external name which is the name of the variable in the dataset and an internal name that can be seen as an alias to this variable.
* urls - Specific urls where to get the data to be used
* csv - CSV options (Will be deprecated in future versions)


## 2.Variable usage:

Again, the variable usage section is split into three categories:

* spatialInformation - Where the programmer is able to tell which are the latitude and longitude collumns. (Only used for CSV files with points)
* temporalInformation - This is where the programmer is able to give the granularity to be used to process the dataset (Only used when we want to deal with the temporal component)
* thematicInformation - Give information about each the thematic variable to be used. Right now the API is able to use the visual variables color, texture, shape, orientation and size. 

## 3.Global options

Here the programmer is able to give the options for the layout, background map provider, the div container etc.

* bounds - The bounds for the visualization.
* layout - The layout in case we want multiple projections.
* container - The div that will contain the Temporal Gisplay map.
* provider - The background map provider name or short name (Google Maps - GM)
* showLoader - Show the loader while the data is being processed. 
* temporalControl - The control to be used in the temporal control (instant or interval).

## 4.Map options

These options can be created in a key-value format to be used by a particular map.

