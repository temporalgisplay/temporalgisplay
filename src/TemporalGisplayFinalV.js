import { Gisplay } from './Gisplay/Gisplay';

module.exports = {


    ////////////////POINTS
    //usaaccidents GEOJSON (Dot Map)
    usaaccidents: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Alcohol Involvement',
                    externalName: 'f1',
                }
            ],
            urls: {
                dataURL: 'fileAccidents',
            }
        };
        let variableUsage = {
            thematicInformation: {
                color: {
                    internalName: 'Alcohol Involvement',
                    mappingMethod: 'colorbrewer-qualitative'
                    // mapping: [
                    //     { value: "Alcohol Involvement", visual: "red" },
                    //     { value: "No Alcohol", visual: "blue" }
                    // ]
                }
            }
        };

        let globalOptionsUSAOneBound = {
            bounds: {
                usa: { //USA
                    NE: { lng: -57.578144, lat: 71.521815 },
                    SW: { lng: -175.351582, lat: 4.205206 },
                    description: 'Mainland USA'
                }
            },
            container: 'gisplay2',
            provider: 'MB',
            showLoader: true
        };

        Gisplay.makeDotMap(parsingOptions, variableUsage, globalOptionsUSAOneBound);
    },

    usa_accidents_figures: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Alcohol Involvement',
                    externalName: 'f1',
                },
                {
                    internalName: 'Number of deaths',
                    externalName: 'f2'
                    // values: [0, 23],
                    // classBreakMethod: 'equalintervals'
                }
            ],
            /* optionalVariables: [
                {
                    internalName: 'Tem alcool',
                    externalName: 'f1',
                },
                {
                    internalName: 'Num mortos',
                    externalName: 'f2',
                }
            ], */
            urls: {
                dataURL: 'fileFigures',
            }
        };
        let variableUsage = {
            thematicInformation: {
                orientation: {
                    internalName: 'Alcohol Involvement',
                    mapping: [
                        { value: "Alcohol Involvement", visual: 0 },
                        { value: "No Alcohol", visual: 50 }
                    ]
                },
                color: {
                    internalName: 'Number of deaths',
                    mapping: ["#f03b20", "#2c7fb8", "#1c9099"],
                    classBreaksMethod: 'equalintervals'
                }
            }
        };

        let globalOptionsUSA = {
            bounds: {
                usa: { //USA
                    NE: { lng: -67.631836, lat: 48.57479 },
                    SW: { lng: -123.793945, lat: 23.563987 }
                },
                alaska: { //Alaska
                    NE: { lng: -138.427734, lat: 72.289067 },
                    SW: { lng: -174.638672, lat: 50.176898 }
                },
                hawaii: { //Hawaii
                    NE: { lng: -162.388916, lat: 17.056785 },
                    SW: { lng: -153.4021, lat: 22.917923 }
                }
            },
            layout: {
                vertical: {
                    sizes: [40, 60],
                    descendants: [
                        {
                            horizontal: {
                                sizes: [60, 40],
                                descendants: ["alaska", "hawaii"]
                            }
                        },
                        "usa"
                    ]
                }
            },
            container: 'gisplay2',
            provider: 'MB',
            // provider: 'MBGL',
            showLoader: true
        };
        Gisplay.makeFiguresMap(parsingOptions, variableUsage, globalOptionsUSA);
    },

    NYYellowColorShape: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Store flag',
                    externalName: 'store_and_fwd_flag'
                },
                {
                    internalName: 'Trip Distance',
                    externalName: 'trip_distance',
                },
                {
                    internalName: 'Latitude',
                    externalName: 'pickup_latitude',
                },
                {
                    internalName: 'Longitude',
                    externalName: 'pickup_longitude',
                },
                {
                    internalName: 'MTA Tax',
                    externalName: 'mta_tax',
                },
                {
                    internalName: 'Vendor ID',
                    externalName: 'VendorID',
                },
                {
                    internalName: 'Pickup Date',
                    externalName: 'tpep_pickup_datetime',
                }
            ],
            urls: {
                dataURL: 'csvTaxis1M'
            },
            csv: {
                numWorkers: 4,
                chunkSize: 1024 * 1024 * 10,
            }
        };

        let variableUsage = {
            spatialInformation: {
                longitude: { internalName: 'Longitude' },
                latitude: { internalName: 'Latitude' }
            },
            temporalInformation: {
                internalName: 'Pickup Date',
                granularity: 'month'
            },
            thematicInformation: {
                color: {
                    internalName: 'Trip Distance',
                    mappingMethod: 'colorbrewer-sequential',
                    numberOfClasses: 4,
                    classBreaksMethod: 'equalintervals'
                    // mapping: ["#ff0000", "#0000ff", "#0000ff", "#0000ff", "#0000ff", "#ff0000", "#ff0000"],
                },
                shape: {
                    internalName: 'Store flag',
                    mapping:
                        [
                            { value: 'Y', visual: 'filled_square' },
                            { value: 'N', visual: 'triangle' }
                        ]
                }
            }
        };

        let globalOptionsNYCity = {
            bounds: {
                ny: { //NY
                    NE: { lng: -74.30809, lat: 40.604569 },
                    SW: { lng: -73.727188, lat: 40.938414 }
                }
            },
            container: 'gisplay2',
            provider: 'MBGL',
            showLoader: true
        };

        Gisplay.makeDotMap(parsingOptions, variableUsage, globalOptionsNYCity);
    },

    NYYellowSize: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Store flag',
                    externalName: 'store_and_fwd_flag',
                },
                {
                    internalName: 'Trip Distance',
                    externalName: 'trip_distance',
                },
                {
                    internalName: 'Pickup Date',
                    externalName: 'tpep_pickup_datetime',
                },
                {
                    internalName: 'Latitude',
                    externalName: 'pickup_latitude',
                },
                {
                    internalName: 'Longitude',
                    externalName: 'pickup_longitude',
                },
                {
                    internalName: 'MTA Tax',
                    externalName: 'mta_tax',
                },
                {
                    internalName: 'Vendor ID',
                    externalName: 'VendorID',
                }
            ],
            urls: {
                dataURL: 'csvTaxis1MProp',
            }
        };
        let variableUsage = {
            spatialInformation: {
                longitude: { internalName: 'Longitude' },
                latitude: { internalName: 'Latitude' }
            },
            temporalInformation: {
                internalName: 'Pickup Date',
                granularity: 'year'
            },
            thematicInformation: {
                color: {
                    internalName: 'Store flag',
                    mapping: [
                        { value: "Y", visual: "red" },
                        { value: "N", visual: "blue" }
                    ]
                    // mapping: ["red", "blue"],
                },
                size: {
                    internalName: 'Trip Distance',
                    mapping: [15, 40, 100],
                    classBreaksMethod: 'quantiles'
                }
            }
        };

        let globalOptionsNYCity = {
            bounds: {
                ny: {
                    NE: { lng: -74.30809, lat: 40.604569 },
                    SW: { lng: -73.727188, lat: 40.938414 }
                }
            },
            container: 'gisplay2',
            provider: 'GM',
            showLoader: true
        };

        Gisplay.makeProportionalSymbolsMap(parsingOptions, variableUsage, globalOptionsNYCity);
    },

    NYTrees: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Latitude da arvore',
                    externalName: 'Latitude'
                },
                {
                    internalName: 'Longitude da arvore',
                    externalName: 'longitude'
                },
                {
                    internalName: 'Status da arvore',
                    externalName: 'status'
                },
               /*  {
                    internalName: 'Saude da arvore',
                    externalName: 'health'
                },
                {
                    internalName: 'Especie',
                    externalName: 'spc_latin'
                }, */
               /*  {
                    internalName: 'Problemas da arvore',
                    externalName: 'problems'
                }, */
                /* {
                    internalName: 'SideWalk',
                    externalName: 'sidewalk'
                }, */
            ],
            urls: {
                dataURL: 'nytrees'
            }
        };
        let variableUsage = {
            spatialInformation: {
                longitude: { internalName: 'Longitude da arvore' },
                latitude: { internalName: 'Latitude da arvore' }
            },
            thematicInformation: {
                color: {
                    internalName: 'Status da arvore',
                    mappingMethod: 'colorbrewer-qualitative'
                }
            }
        };

        let globalOptionsNYCity = {
            bounds: {
                ny: {
                    NE: { lng: -74.30809, lat: 40.604569 },
                    SW: { lng: -73.727188, lat: 40.938414 }
                }
            },
            container: 'gisplay2',
            provider: 'GM',
            showLoader: true
        };

        Gisplay.makeDotMap(parsingOptions, variableUsage, globalOptionsNYCity);
    },

    PTPulmonia: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Género',
                    externalName: 'gender',
                },
                /* {
                    internalName: 'Idade do paciente',
                    externalName: 'age',
                    values: [0, 102],
                    numberOfClasses: 5
                }, */
                {
                    internalName: 'Ano do caso de pneumonia',
                    externalName: 'year',
                },
                {
                    internalName: 'Latitude',
                    externalName: 'latitudeParish',
                },
                {
                    internalName: 'Longitude',
                    externalName: 'LongitudeParish',
                }
            ],
            optionalVariables: [
                {
                    internalName: 'Dias de admissão',
                    externalName: 'admissionDaysClass',
                },
                {
                    internalName: 'Nome Hospital',
                    externalName: 'desigHospital',
                }/* ,
                {
                    internalName: 'Ano do caso de pulmonia',
                    externalName: 'year',
                } */
            ],
            urls: {
                dataURL: 'ptPulmonia',
            },
        };
        let variableUsage = {
            temporalInformation: {
                internalName: 'Ano do caso de pneumonia',
                granularity: 'year',
            },
            spatialInformation: {
                longitude: { internalName: 'Longitude' },
                latitude: { internalName: 'Latitude' }
            },
            thematicInformation: {
                color: {
                    internalName: 'Género',
                    mappingMethod: 'colorbrewer-qualitative'
                    /*   mapping: [
                          { value: "M", visual: "blue" },
                          { value: "F", visual: "pink" }
                      ], */
                    // values: ["M", "F"]
                }
            }
            /*  shape: {
                 externalName: 'age',
                 mapping: {
                     "M": "square",
                     "F": "circlefull",
                 },
             } */
        };

        let globalOptionsPortugalLXPorto = {
            bounds: {
                pt: { //PT
                    NE: { lng: -4.954834, lat: 42.666281 },
                    SW: { lng: -10.953369, lat: 35.406961 },
                    description: 'Portugal Continental'
                },
                lx: { //Lisboa
                    NE: { lng: -8.915405, lat: 39.047956 },
                    SW: { lng: -9.560852, lat: 38.62757 },
                    description: 'Portugal Continental'
                },
                porto: { //Porto
                    NE: { lng: -8.520584, lat: 41.238046 },
                    SW: { lng: -8.736877, lat: 41.071069 },
                    description: 'Portugal Continental'
                }
            },
            layout: {
                vertical: {
                    sizes: [40, 60],
                    descendants: [
                        {
                            horizontal: {
                                sizes: [50, 50],
                                descendants: ["porto", "lx"]
                            }
                        },
                        "pt"
                    ]
                }
            },
            container: 'gisplay2',
            provider: 'MBGL',
            showLoader: true,
            timeControl: 'interval'
        };
        Gisplay.makeDotMap(parsingOptions, variableUsage, globalOptionsPortugalLXPorto);
    },

    //////////// POLYGONS
    usaaccidents_by_county: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Number of deaths',
                    externalName: 'f3'
                },
                {
                    internalName: 'State',
                    externalName: 'f1',
                },
                {
                    internalName: 'County',
                    externalName: 'f2',
                }
            ],
            urls: {
                dataURL: 'choroplethByCounty',
            }
        };
        let variableUsage = {
            thematicInformation: {
                color: {
                    internalName: 'Number of deaths',
                    mapping: ["#ece7f2", "#a6bddb", "#2b8cbe"],
                    classBreaksMethod: 'equalintervals'
                    // mappingMethod: 'colorbrewer-qualitative'
                    // mapping: [
                    //     { value: "Alcohol Involvement", visual: "red" },
                    //     { value: "No Alcohol", visual: "blue" }
                    // ]
                }
            }
        };

        let globalOptionsUSA = {
            bounds: {
                usa: { //USA
                    NE: { lng: -67.631836, lat: 48.57479 },
                    SW: { lng: -123.793945, lat: 23.563987 }
                },
                alaska: { //Alaska
                    NE: { lng: -138.427734, lat: 72.289067 },
                    SW: { lng: -174.638672, lat: 50.176898 }
                },
                hawaii: { //Hawaii
                    NE: { lng: -162.388916, lat: 17.056785 },
                    SW: { lng: -153.4021, lat: 22.917923 }
                }
            },
            layout: {
                vertical: {
                    sizes: [40, 60],
                    descendants: [
                        {
                            horizontal: {
                                sizes: [60, 40],
                                descendants: ["alaska", "hawaii"]
                            }
                        },
                        "usa"
                    ]
                }
            },
            container: 'gisplay2',
            provider: 'MB',
            showLoader: true
        };

        Gisplay.makeChoropleth(parsingOptions, variableUsage, globalOptionsUSA);
    },

    //texture
    crimes_frequent_com_areas: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    /*      internalName: 'Amount stolen',
                         externalName: 'f3',
                         values: [1, 8587],
                         classBreakMethod: 'quantiles' */
                    internalName: 'Type of robbery',
                    externalName: 'f2'
                    // values: ['THEFT', 'BATTERY', 'NARCOTICS']
                },
                {
                    internalName: 'Amount stolen',
                    externalName: 'f3',
                    // values: [35, 3395]
                },
                {
                    internalName: 'Conty',
                    externalName: 'f1',
                },
                {
                    internalName: 'Amount stolen',
                    externalName: 'f3',
                }
            ],
            /* optionalVariables: [
                {
                    internalName: 'Conty',
                    externalName: 'f1',
                },
                {
                    internalName: 'Amount stolen',
                    externalName: 'f3',
                }
            ], */
            urls: {
                dataURL: 'crimesFreqComAreas',
            }
        };
        let variableUsage = {
            thematicInformation: {
                texture: {
                    internalName: 'Type of robbery',
                    mapping: [
                        { value: "THEFT", visual: 'crossPattern' },
                        { value: "BATTERY", visual: 'plusPattern' },
                        { value: "NARCOTICS", visual: 'wavePattern' },
                    ]
                },
                color: {
                    internalName: 'Amount stolen',
                    mappingMethod: 'colorbrewer-sequential',
                    classBreaksMethod: 'quantiles'
                }
            }
        };

        let globalOptionsUSA = {
            bounds: {
                world: {
                    NE: { lng: -86.82312, lat: 42.823498 },
                    SW: { lng: -89.27124, lat: 40.771991 }
                }
            },
            container: 'gisplay2',
            provider: 'MB',
            showLoader: true
        };
        Gisplay.makeChoropleth(parsingOptions, variableUsage, globalOptionsUSA);
    },

    //WITH IDS
    IDsDistritosPT: function () { //DUMMY DATA
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Número de acidentes',
                    externalName: 'Num_Accidents'
                    // values: [10, 222]
                },
                {
                    internalName: 'Identificador do distrito',
                    externalName: 'ID_District'
                },
                {
                    internalName: 'Ano dos acidentes',
                    externalName: 'Year'
                }
            ],
            urls: {
                dataURL: 'data',
                geospatialURL: 'distritos',
                idOnDataURL: 'ID_District',
                idOnGeoSpatialURL: 'ID_1'
            },
            /*   time: {
                  internalName: 'Ano dos acidentes',
                  externalName: 'Year',
                  granularity: 'year',
              } */
        };
        let variableUsage = {
            temporalInformation: {
                internalName: 'Ano dos acidentes',
                granularity: 'year'
            },
            thematicInformation: {
                color: {
                    internalName: 'Número de acidentes',
                    mappingMethod: 'colorbrewer-sequential',
                    classBreaksMethod: 'quantiles',
                    // classBreaks: [40, 70, 222]
                    // mapping: ["#000000", "#a1b2c3"],
                }
            }
        };

        let globalOptionsPortugal = {
            bounds: {
                pt: { //PT
                    NE: { lng: -4.954834, lat: 42.666281 },
                    SW: { lng: -10.953369, lat: 35.406961 }
                },
                acores: { //Acores
                    NE: { lng: -23.543701, lat: 40.430224 },
                    SW: { lng: -32.794189, lat: 35.406961 }
                },
                madeira: { //PT
                    NE: { lng: -15.477848, lat: 33.770015 },
                    SW: { lng: -17.784977, lat: 31.830899 }
                }
            },
            layout: {
                vertical: {
                    sizes: [40, 60],
                    descendants: [
                        {
                            horizontal: {
                                sizes: [60, 40],
                                descendants: ["acores", "madeira"]
                            }
                        },
                        "pt"
                    ]
                }
            },
            container: 'gisplay2',
            provider: 'GM',
            showLoader: true
        };

        Gisplay.makeChoropleth(parsingOptions, variableUsage, globalOptionsPortugal/* globalOptionsPortugalContinental */);
    },

    //WITH IDS
    allCountriesEmploymentRate15_24: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Employment 15 to 24 yo (%)',
                    externalName: 'aged_15_24_employment_rate_percent'
                },
                {
                    internalName: 'Country',
                    externalName: 'geo',
                },
                {
                    internalName: 'Ano',
                    externalName: 'time',
                }
            ],
            urls: { //geo,time,annual_hiv_deaths_number_all_ages
                dataURL: 'employmentFileCSV',
                geospatialURL: 'worldFile2',
                idOnDataURL: 'geo',
                idOnGeoSpatialURL: 'ISO_A3'
            }
        };
        let variableUsage = { //geo,time,aged_15_24_employment_rate_percent
            temporalInformation: {
                internalName: 'Ano',
                granularity: 'year'
            },
            thematicInformation: {
                color: {
                    internalName: 'Employment 15 to 24 yo (%)',
                    classBreaksMethod: 'quantiles',
                    mappingMethod: 'colorbrewer-sequential'
                    // mapping: ["#000000", "#a1b2c3"],
                    // values: [11.3, 81.2],
                }
            }
        };

        let globalOptionsWorld = {
            bounds: {
                world: {
                    NE: { lng: 180, lat: 90 },
                    SW: { lng: -180, lat: -90 }
                }
            },
            container: 'gisplay2',
            provider: 'GM',
            showLoader: true
        };
        Gisplay.makeChoropleth(parsingOptions, variableUsage, globalOptionsWorld);
    },

    //////////// LINES
    worldRoads: function () {
        let parsingOptions = {
            variableDeclarations: [
                {
                    internalName: 'Tipo de via',
                    externalName: 'type',
                    // values: [10, 1000]
                    // values: ['Secondary Highway', 'Major Highway', 'Ferry Route', 'Beltway', 'Bypass', 'Road', 'Ferry, seasonal', 'Unknown', 'Track']
                }
            ],
            urls: {
                dataURL: 'worldLines',
            }
        };
        let variableUsage = {
            thematicInformation: {
                color: {
                    internalName: 'Tipo de via',
                    mappingMethod: 'colorbrewer-qualitative'
                    /*  mapping: [
                          { value: 'Secondary Highway', visual: 'red' },
                          { value: 'Major Highway', visual: 'pink' },
                          { value: 'Ferry Route', visual: 'blue', },
                          { value: 'Beltway', visual: 'orange' },
                          { value: 'Bypass', visual: 'navy' },
                          { value: 'Road', visual: 'tomato' },
                          { value: 'Ferry, seasonal', visual: 'green' },
                          { value: 'Unknown', visual: 'yellow' },
                          { value: 'Track', visual: 'brown' }
                      ] */

                    // values: ['Secondary Highway', 'Major Highway', 'Ferry Route', 'Beltway', 'Bypass', 'Road', 'Ferry, seasonal', 'Unknown', 'Track']
                    // mapping: ["red", "blue"],
                }
            }
        };

        let globalWorldOpts = {
            bounds: {
                world: { //
                    NE: { lng: 180, lat: 90 },
                    SW: { lng: -180, lat: -90 }
                }
            },
            container: 'gisplay2',
            provider: 'MBGL',
            showLoader: true
        };
        Gisplay.makeLinesMap(parsingOptions, variableUsage, globalWorldOpts);
    },
};

