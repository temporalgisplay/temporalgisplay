import { Choropleth } from './Maps/Choropleth';
import { DotMap } from './Maps/DotMap';
// import { ChangeMap } from './Maps/ChangeMap';
import { ProportionalSymbolsMap } from './Maps/ProportionalSymbolsMap';
import { FiguresMap } from './Maps/FiguresMap';
import { LinesMap } from './Maps/LinesMap';
import { ChangeMap } from './Maps/ChangeMap';

/**
 * Gisplay API entry point with one method for each map available.
 */
export class Gisplay {
    /**
     * Creates a new map of type Choropleth.
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @static
     * @memberof Gisplay
     */
    static makeChoropleth(parsingOptions, mappingOptions, globalOptions) {
        new Choropleth(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Creates a new Dot Map.
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @static
     * @memberof Gisplay
     */
    static makeDotMap(parsingOptions, mappingOptions, globalOptions) {
        new DotMap(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Creates a new Proportional Symbols Map. 
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @static
     * @memberof Gisplay
     */
    static makeProportionalSymbolsMap(parsingOptions, mappingOptions, globalOptions) {
        new ProportionalSymbolsMap(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Creates a new Figures Map. 
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @static
     * @memberof Gisplay
     */
    static makeFiguresMap(parsingOptions, mappingOptions, globalOptions) {
        new FiguresMap(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Creates a new Lines Map. 
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @static
     * @memberof Gisplay
     */
    static makeLinesMap(parsingOptions, mappingOptions, globalOptions) {
        new LinesMap(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Creates a new Lines Map. 
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @static
     * @memberof Gisplay
     */
    static makeChangeMap(parsingOptions, mappingOptions, globalOptions) {
        new ChangeMap(parsingOptions, mappingOptions, globalOptions);
    }
}