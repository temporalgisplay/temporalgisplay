// import { GisplayMap } from './Maps/GisplayMap';

import { URLVariable } from './VVs/URLVariable';
import { DataVariable } from './VVs/DataVariable';
import { TimeVariable } from './VVs/TimeVariable';
import { CategoricalVariable } from './VVs/CategoricalVariable';
import { ContinuousVariable } from './VVs/ContinuousVariable';

import { GisplayError } from './GisplayError';
import { GisplayDefaults } from './GisplayDefaults';
import { ColorBrewer } from './Helpers/ColorBrewer';

/**
 * This class will contain all the options available on the Gisplay API.
 * @see Diogo's thesis page 50-52
 */
export class GisplayOptions {
    /**
     * Creates an instance of GisplayOptions.
     * @param {Object} parsingOptions - The parsing options object. 
     * @param {Object} variableUsage - The variable usage options.
     * @param {Object} globalOptions - The global options object.
     * @param {GisplayMap} gisplayMap - The GisplayMap object.
     * @memberof GisplayOptions
     */
    constructor(parsingOptions, variableUsage, globalOptions, gisplayMap) {
        this.parsingOptions = parsingOptions;

        /**
         * The time variable.
         * @type {TimeVariable}
         */
        this.timeVariable = this.processTimeVariable(variableUsage, globalOptions, gisplayMap);
        /**
         * The url variable.
         * @type {URLVariable}
         */
        this.urlVariable = this.processURLOptions(parsingOptions.urls);
        /**
         * The CSV parser information.
         * @type {{chunkSize: number, numWorkers: number}}
         */
        this.csv = this.loadCSVParserOptions(parsingOptions.csv);
        /**
         * The global options.
         * @type {Object}
         */
        this.globalOptions = this.processGlobalOptions(globalOptions);
        /**
         * The geometry variables (longitude and latitude).
         * @type {Array<DataVariable>}
         */
        this.geometryVariables = this.processGeometryVariables(variableUsage);

        //Opcionais sao as q n estiverem no variableUsage

        /**
         * The map variables.
         * @type {Array<MapVariable>}
         */
        this.thematicVariables = this.processThematicVariables(variableUsage, gisplayMap);

        //@TODO OPTIONAL VARIABLES
        /**
         * The optional variables.
         * @type {Array<DataVariable>}
         */
        this.optionalVariables = this.processOptionalVariables();


        console.log(this);
    }

    /**
     * Process the time variable and return the created time variable. 
     * @param {Object} parsingOptions - The parsing options.
     * @param {{temporalInformation: Object}} variableUsage - The information about the variables.
     * @param {Object} globalOptions - The global options. 
     * @param {GisplayMap} gisplayMap - The thematic map. 
     * @returns {TimeVariable|undefined} - the time variable if given by the programmer, otherwise undefined.
     * @memberof NewGisplayOptions
     */
    processTimeVariable(variableUsage, globalOptions, gisplayMap) {
        if (!variableUsage.temporalInformation)
            return undefined;
        let internalName = variableUsage.temporalInformation.internalName;
        let granularity = variableUsage.temporalInformation.granularity;
        if (!granularity) //No granularity given
            throw new GisplayError('No granularity was given');
        else if (GisplayDefaults.getAvailableGranularities().indexOf(granularity) === -1) //The given granularity does not exist
            throw new GisplayError(`The given granularity: ${granularity} doesn't belong to the list of available granularities: ${GisplayDefaults.getAvailableGranularities()}.`);
        else if (!this._findVariableDeclaration(internalName)) //internal name not defined in the 
            throw new GisplayError(`The given internalName: ${internalName} doesn't exist in the list of variable declarations.`);

        let variable = this._findVariableDeclaration(internalName);
        let timeControl = globalOptions.timeControl;
        if (!timeControl)
            timeControl = gisplayMap.getAvailableTemporalControls()[0];
        return new TimeVariable(variable.externalName, internalName, granularity, timeControl);
    }

    /**
     * Process URL options.
     * @param {{dataURL: string, geospatialURL: string, idOnDataURL: string, idOnGeoSpatialURL: string}} urls - The urls options given by the programmer. 
     * @returns {URLVariable} - the URLVariable that holds the reference to the URLs and ids.
     * @see https://stackoverflow.com/a/15979390/
     * https://stackoverflow.com/questions/12460378/how-to-get-json-from-url-in-javascript
     * @memberof GisplayOptions
     */
    processURLOptions(urls) {
        //Needs to see if the url is a local url(file) or not
        let dataURL;
        if (!urls.dataURL)
            throw new GisplayError(`The data URL was not given, and it is mandatory.`);
        else {
            if (urls.dataURL.includes('http') || urls.dataURL.includes('https'))
                dataURL = urls.dataURL;
            else {
                dataURL = document.getElementById(urls.dataURL);
                if (dataURL && dataURL.files[0]) {
                    dataURL = dataURL.files[0];
                    this._validateFileExtension(dataURL.name);
                }
                else
                    throw new GisplayError(`No file was selected.`);
            }
        }

        let geospatialURL;
        if (urls.geospatialURL !== undefined) {
            if (urls.geospatialURL.includes('http') || urls.geospatialURL.includes('https'))
                geospatialURL = urls.geospatialURL;
            else {
                geospatialURL = document.getElementById(urls.geospatialURL);
                if (geospatialURL && geospatialURL.files[0])
                    geospatialURL = geospatialURL.files[0];
                if (geospatialURL)
                    this._validateFileExtension(geospatialURL.name);
            }
        }
        return new URLVariable(dataURL, geospatialURL, urls.idOnDataURL, urls.idOnGeoSpatialURL);
    }

    /**
     * Validate the file extension 
     * @param {string} fileName - The name of the file to validate.
     * @returns {boolean} - true if the filename is valid, otherwise will throw an Error.
     * @memberof NewGisplayOptions
     */
    _validateFileExtension(fileName) {
        if (fileName === undefined)
            throw new GisplayError('The given file has a type this is not parseable by the Temporal Gisplay API.');
        fileName = fileName.toLowerCase();
        if (fileName.endsWith('.csv') || fileName.endsWith('.json') || fileName.endsWith('.geojson'))
            return true;
        else
            throw new GisplayError('The given file has a type that isn\'t parseable by the Temporal Gisplay API.');
    }

    /**
     * Find the variable declaration in the parsing options.
     * @param {{variableDeclarations: Object, urls:Object}} parsingOptions - The parsing options.
     * @param {string} internalName - The internal name to look for in the variable declarations.
     * @returns {{internalName?:string, externalName: string}|boolean} - the variable declared in the parsing options.
     * @memberof NewGisplayOptions
     */
    _findVariableDeclaration(internalName) {
        let variables = this.parsingOptions.variableDeclarations;
        if (!variables)
            throw new GisplayError("Declaration of variables not found.");
        for (const variable of variables)
            if (variable.internalName === internalName || variable.externalName === internalName) //If the internal name is found in the declarations then return the variable
                return variable;
        return false;
    }

    /**
     * Loads parser options.
     * @param {Object} userOptions - User defined options.
     * @returns {{chunkSize: number, numWorkers: number}} - Options for the parser.
     * @memberof GisplayOptions
     */
    loadCSVParserOptions(userOptions) {
        if (!userOptions)
            userOptions = {};
        return {
            /**
             * Size of each chunk that each worker will read at a time.
             * @type {number}
             */
            chunkSize: userOptions.chunkSize, //UserDef or 10MB
            /** 
             * Number of workers that will process the data file.
             * @type {number}
             */
            numWorkers: userOptions.numWorkers, //Worker threads 
        };
    }

    /**
     * Process global options for the Gisplay API.
     * @param {{bounds: Object, layout: Object, container: string, provider: string, showLoader: boolean, mapOnClickFunction: Function, timeControl: string}} globalOptions - The global options. 
     * @returns {{bounds: Object, layout: Object, container: string, provider: string, showLoader: boolean, mapOnClickFunction: Function, timeControl: string}} - the global options.
     * @memberof NewGisplayOptions
     */
    processGlobalOptions(globalOptions) {
        //@TODO: verify errors for global opts
        return {
            bounds: globalOptions.bounds,
            layout: globalOptions.layout || this._completeLayout(globalOptions.bounds),
            container: globalOptions.container,
            provider: globalOptions.provider || 'MB',
            showLoader: globalOptions.showLoader || true,
            mapOnClickFunction: globalOptions.mapOnClickFunction,
            legendOnClickFunction: globalOptions.legendOnClickFunction,
            timeControl: globalOptions.timeControl
        };
    }

    /**
     * Complete layout when there's one bound given and no layout for the said bound.
     * @returns {Object} - The layout for the bound.
     * @private
     * @memberof GisplayOptions
     */
    _completeLayout(bounds) {
        if (Object.keys(bounds).length === 1) {
            return {
                vertical: {
                    sizes: [100],
                    descendants: [Object.keys(bounds)[0]]
                }
            };
        }
        else
            throw new GisplayError("Layout is lacking in the options object.");
    }

    /**
     * Creates the geometry variables. These are used in CSV parsing to parse the latitude and longitude.
     * @param {any} variableUsage - The usage of the declared variables.
     * @param {any} parsingOptions - The parsing options. 
     * @returns {Array<DataVariable>} - the geometry variables (lng, lat) in an array, where the first element will contiain the longitude DataVariable and the second the latitude.
     * @memberof GisplayOptions
     */
    processGeometryVariables(variableUsage) {
        let geometryVariables = [];
        let geoVars = variableUsage.spatialInformation;
        if (geoVars && Object.keys(geoVars).length === 2) {
            let longitudeInternalName = geoVars.longitude ? geoVars.longitude.internalName : undefined;
            let latitudeInternalName = geoVars.latitude ? geoVars.latitude.internalName : undefined;
            if (longitudeInternalName && latitudeInternalName) {
                let longitudeVariable = this._findVariableDeclaration(longitudeInternalName);
                let latitudeVariable = this._findVariableDeclaration(latitudeInternalName);
                if (!latitudeVariable || !longitudeVariable)
                    throw new GisplayError('Latitude or longitude weren\'t correctly given.');
                else {
                    geometryVariables.push(new DataVariable(longitudeVariable.externalName, longitudeInternalName));
                    geometryVariables.push(new DataVariable(latitudeVariable.externalName, latitudeInternalName));
                }
            }
        }

        if (geometryVariables.length < 2 && !this.getURLVariable().dataFileIsGeoJSON() && !this.getURLVariable().hasIds())
            throw new GisplayError('No longitude/latitude given but they are required.');
        return geometryVariables;
    }

    /**
     * Process the themaic variables and create the respective (categorical or continous) class object.
     * @param {any} variableUsage 
     * @param {GisplayMap} gisplayMap - The Gisplay map.
     * @returns {Array<CategoricalVariable|ContinuousVariable>}
     * @memberof NewGisplayOptions
     */
    processThematicVariables(variableUsage, gisplayMap) {
        let shaderVariablesMap = gisplayMap.getShadersInfo().getShaderVariablesMap();
        let categoricalVVs = [];
        let continuousVVs = [];

        let thematicVariables = variableUsage.thematicInformation;
        let vvs = Object.keys(thematicVariables);
        for (const visualVarName of vvs) {
            console.warn(visualVarName, thematicVariables[visualVarName]); //DELETE

            let vvOptions = thematicVariables[visualVarName];
            let variable = this._findVariableDeclaration(vvOptions.internalName);
            if (!vvOptions.internalName || !variable)
                throw new GisplayError(`Internal name for visual variable: ${visualVarName}, not given or not declared in variable declarations.`);

            let vvMapping = vvOptions.mapping;
            if (visualVarName === GisplayDefaults.COLOR()) {
                let mappingMethod = vvOptions.mappingMethod; //e.g. colorbrewer-sequential
                if (!vvMapping && !mappingMethod)
                    throw new GisplayError(`No mapping given nor mapping method. When the visual variable is color then is mandatory to provide a mapping method or the mapping.`);
                if (mappingMethod && ((typeof mappingMethod === 'string' && GisplayDefaults.getAvailableColorBrewerMethods().indexOf(mappingMethod.toLocaleLowerCase()) === -1) || typeof mappingMethod !== 'string'))
                    throw new GisplayError(`The mapping method: ${mappingMethod} is not available`);

                if (vvMapping) { //has mapping: some value
                    if (Array.isArray(vvMapping)) {
                        if (vvMapping[0].value) // Categorical >>>>>>>>>>>>
                            categoricalVVs.push(this._createCategoricalVariableColor(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap));
                        else   //                  Continuous >>>>>>>>>>>>
                            continuousVVs.push(this._createContinuousVariableColor(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap, undefined));
                    } else
                        throw new GisplayError('Given mapping object is not an array of values hence not valid');
                } else {
                    if (mappingMethod) {
                        if (mappingMethod.toLowerCase().includes(GisplayDefaults.QUALITATIVE().toLowerCase())) // Categorical >>>>>>>>>>>>
                            categoricalVVs.push(this._createCategoricalVariableColor(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap));
                        else  //                                                                                  Continuous >>>>>>>>>>>>
                            continuousVVs.push(this._createContinuousVariableColor(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap, mappingMethod.split('-')[1]));
                    } else
                        throw new GisplayError(`Wrong options provided for visual variable: ${visualVarName}.`);
                }
            }
            else {
                if (!vvMapping) //No mapping then error
                    throw new GisplayError(`The visual variable: ${visualVarName}, needs to include it's mapping in the given options.`);
                else if (!Array.isArray(vvMapping))
                    throw new GisplayError(`The mapping for the visual variable: ${visualVarName}, isn't provided in array format.`);
                else {
                    if (vvMapping[0].value)   // If mapping[0] has value key then its categorical
                        categoricalVVs.push(this._createCategoricalVariable(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName));
                    else  //                     Otherwise its continuous
                        continuousVVs.push(this._createContinuousVariable(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap));
                }
            }
            console.log("----------------------------------"); //DELETE
        }
        return [categoricalVVs, continuousVVs];
    }

    /**
     * Returns the continuous variable created using the provided parameters.
     * @param {any} vvOptions - The options for this visual variable inside the thematicInformation object.
     * @param {Array<string>} vvMapping - The mapping between the values and their visual value (e.g. [20,40, 80] or ['#121212', '#000000']).
     * @param {Map<string, {name: string, type: number, qualifier: string}>} shaderVariablesMap - The shaders for the current map.
     * @param {{externalName: string, internalName: string}} variable - The variable in the  variable declarations.
     * @param {string} visualVarName - The name of the visual variable. 
     * @param {GisplayMap} gisplayMap - The Gisplay map.
     * @returns {ContinuousVariable} - the continuous variable created using the provided parameters.
     * @memberof NewGisplayOptions
     */
    _createContinuousVariable(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap) {
        let numberOfClasses = vvOptions.numberOfClasses;
        let classBreaks = vvOptions.classBreaks;

        if (numberOfClasses !== undefined) {
            if (+numberOfClasses !== +numberOfClasses)
                throw new GisplayError('The numberOfClasses key must be a number');
            else if (numberOfClasses <= 1)
                throw new GisplayError('The number of classes must be a number and higher than 1.');
            else if (numberOfClasses !== vvMapping.length)
                throw new GisplayError('The number of classes is not equal to the number of mappings');
        }
        numberOfClasses = vvMapping.length;

        if (classBreaks !== undefined) {
            if (!Array.isArray(classBreaks))
                throw new GisplayError('classBreaks key must be an array.');
            else if (!classBreaks.every((e) => +e === +e))
                throw new GisplayError('classBreaks must be an array of numbers.');
            else if (classBreaks.length !== numberOfClasses - 1)
                throw new GisplayError('The classBreaks don\'t match the number of classes.');
        }

        let classBreaksMethod = vvOptions.classBreaksMethod;
        let params = vvOptions.params;
        if (classBreaksMethod && typeof classBreaksMethod === 'string' && GisplayDefaults.getAvailableClassBreaksMethods().indexOf(classBreaksMethod.toLowerCase()) === -1)
            throw new GisplayError(`The class break method: ${classBreaksMethod} is not available.`);

        console.error(classBreaks, classBreaksMethod);
        if (!classBreaks && !classBreaksMethod)
            throw new GisplayError(`Neither class breaks nor class breaks method were given for the variable with internal name: ${vvOptions.internalName} and visual variable: ${visualVarName}.`);

        return new ContinuousVariable(
            variable.externalName,
            vvOptions.internalName,
            visualVarName,
            shaderVariablesMap.get(visualVarName).qualifier,
            vvMapping,
            classBreaks,
            numberOfClasses,
            classBreaksMethod,
            params
        );
    }

    /**
     * Returns the continuous variable when the visual variable is color.
     * @param {any} vvOptions - The options for this visual variable inside the thematicInformation object.
     * @param {Array<string>} vvMapping - The mapping between the values and their visual value (e.g. [20,40, 80] or ['#121212', '#000000']).
     * @param {Map<string, {name: string, type: number, qualifier: string}>} shaderVariablesMap - The shaders for the current map.
     * @param {{externalName: string, internalName: string}} variable - The variable in the  variable declarations.
     * @param {string} visualVarName - The name of the visual variable. 
     * @param {GisplayMap} gisplayMap - The Gisplay map.
     * @param {string} mappingMethod - The color brewer mapping method.
     * @returns {ContinuousVariable} - the continuous variable when the visual variable is color.
     * @memberof NewGisplayOptions
     */
    _createContinuousVariableColor(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap, dataNature) {
        let numberOfClasses = vvOptions.numberOfClasses;
        let numClassesIsNumber = +numberOfClasses === +numberOfClasses;
        let classBreaks = vvOptions.classBreaks;

        if (vvMapping !== undefined) {
            if (numberOfClasses !== undefined) {
                if (!numClassesIsNumber)
                    throw new GisplayError('The number of classes must be a number');
                else if (numberOfClasses <= 1)
                    throw new GisplayError('The number of classes must be a number and higher than 1.');
                else if (numberOfClasses !== vvMapping.length)
                    throw new GisplayError('The number of classes is not equal to the number of mappings');
            }

            if (vvMapping.every((color) => color.split('#').length !== 2)) //'#a'.split('#').length = 2
                throw new GisplayError(`The mapping color given aren't in the hexadecimal format but it's mandatory to do so for continuous variables.`);
            else { //Get the colors from GisplayMap
                let rgbColors = [];
                for (let hexColor of vvMapping)
                    rgbColors.push(ColorBrewer.convertHexColorToRGB(hexColor));
                vvMapping = rgbColors;
            }
            numberOfClasses = vvMapping.length;
        } else {
            vvMapping = gisplayMap.getDefaultColors(numClassesIsNumber ? numberOfClasses : gisplayMap.defaults().color.numberOfClasses, dataNature);
            numberOfClasses = vvMapping.length;
        }

        if (classBreaks !== undefined) {
            if (!Array.isArray(classBreaks))
                throw new GisplayError('classBreaks key must be an array.');
            else if (!classBreaks.every((e) => +e === +e))
                throw new GisplayError('classBreaks must be an array of numbers.');
            else if (classBreaks.length !== numberOfClasses - 1) //Possible problem here when is color
                throw new GisplayError('The classBreaks don\'t match the number of classes.');
        }

        let classBreaksMethod = vvOptions.classBreaksMethod;
        let params = vvOptions.params;
        if (classBreaksMethod && typeof classBreaksMethod === 'string' && GisplayDefaults.getAvailableClassBreaksMethods().indexOf(classBreaksMethod.toLowerCase()) === -1)
            throw new GisplayError(`The class break method: ${classBreaksMethod} is not available.`);

        return new ContinuousVariable(
            variable.externalName,
            vvOptions.internalName,
            visualVarName,
            shaderVariablesMap.get(visualVarName).qualifier,
            vvMapping,
            classBreaks,
            numberOfClasses,
            classBreaksMethod,
            params
        );
    }

    /**
     * Returns the categorical variable created using the provided parameters.
     * @param {any} vvOptions - The options for this visual variable inside the thematicInformation object.
     * @param {Array<{value: string, visual:string}>} vvMapping - The mapping between the values and their visual value (e.g. "Y"-> "red").
     * @param {Map<string, {name: string, type: number, qualifier: string}>} shaderVariablesMap - The shaders for the current map.
     * @param {{externalName: string, internalName: string}} variable - The variable in the  variable declarations.
     * @param {string} visualVarName - The name of the visual variable. 
     * @returns {CategoricalVariable} - the categorical variable created using the provided parameters.
     * @memberof NewGisplayOptions
     */
    _createCategoricalVariable(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName) {
        let vvMappingMap = new Map();
        for (let mappingValue of vvMapping) {
            if (visualVarName === GisplayDefaults.ORIENTATION())
                vvMappingMap.set(mappingValue.value, +mappingValue.visual); //+mappingValue.visual is to MAKE sure it's a number
            else
                vvMappingMap.set(mappingValue.value, mappingValue.visual);
        }
        return new CategoricalVariable(variable.externalName, vvOptions.internalName, visualVarName, shaderVariablesMap.get(visualVarName).qualifier, vvMappingMap);
    }

    /**
     * Returns the categorical variable when the visual variable is color.
     * @param {any} vvOptions - The options for this visual variable inside the thematicInformation object.
     * @param {Array<{value: string, visual:string}>} vvMapping - The mapping between the values and their visual value (e.g. "Y"-> "red").
     * @param {Map<string, {name: string, type: number, qualifier: string}>} shaderVariablesMap - The shaders for the current map.
     * @param {{externalName: string, internalName: string}} variable - The variable in the  variable declarations.
     * @param {string} visualVarName - The name of the visual variable. 
     * @param {GisplayMap} gisplayMap - The name of the visual variable. 
     * @returns {CategoricalVariable} - the categorical variable created using the provided parameters.
     * @memberof NewGisplayOptions
     */
    _createCategoricalVariableColor(vvOptions, vvMapping, shaderVariablesMap, variable, visualVarName, gisplayMap) {
        console.log("CATEGORICAL", gisplayMap);
        let vvMappingMap = new Map();
        if (vvMapping) {
            for (let mappingValue of vvMapping) {
                let rgbColor;
                let givenColor = mappingValue.visual;
                if (givenColor.split('#').length === 2) // Hexadecimal color
                    rgbColor = ColorBrewer.convertHexColorToRGB(givenColor);
                else //                               Named color
                    rgbColor = ColorBrewer.convertNameToRGB(givenColor);
                vvMappingMap.set(mappingValue.value, rgbColor);
            }
        }
        console.log(vvMappingMap);
        return new CategoricalVariable(variable.externalName, vvOptions.internalName, visualVarName, shaderVariablesMap.get(visualVarName).qualifier, vvMappingMap, gisplayMap);
    }

    /**
     * Return the array of optional variables.  
     * @param {Array<Object>} optionalVariables - The optional variables array given by the programmer. 
     * @returns {Array<DataVariable>} - the array of optional variables.
     * @memberof GisplayOptions
     */
    processOptionalVariables() {
        let optionalVariables = [];

        let allVariables = this.parsingOptions.variableDeclarations;
        for (let variable of allVariables) {
            let externalName = variable.externalName;
            let internalName = variable.internalName || externalName;

            //Look into spatial + thematic + temporal to see if the variable is one of them
            let foundVariable = false;
            for (let geoVar of this.geometryVariables)
                if (geoVar.getExternalName() === externalName)
                    foundVariable = true;

            let catVars = this.getCategoricalVariables();
            for (let catVar of catVars)
                if (catVar.getExternalName() === externalName)
                    foundVariable = true;

            let contVars = this.getContinousVariables();
            for (let contVar of contVars)
                if (contVar.getExternalName() === externalName)
                    foundVariable = true;

            let timeVar = this.getTimeVariable();
            if (timeVar && timeVar.getExternalName() === externalName)
                foundVariable = true;

            if (!foundVariable)
                optionalVariables.push(new DataVariable(externalName, internalName));
        }
        // for (let optionalVar of optionalVariables)
        // resOptsVariables.push(new DataVariable(optionalVar.externalName, optionalVar.internalName || optionalVar.externalName));
        return optionalVariables;
    }

    /*
    #####################################################################
    ######################     UTILITY METHODS      #####################
    #####################################################################
    */
    /**
     * Returns the URLVariable instance. 
     * @returns {URLVariable} - the URLVariable instance. 
     * @memberof GisplayOptions
     */
    getURLVariable() {
        return this.urlVariable;
    }

    /**
     * Returns an array with all the categorical variables that were created with the given options.
     * @returns {Array<CategoricalVariable>} - all the categorical variables that were created with the given options.
     * @memberof GisplayOptions
     */
    getCategoricalVariables() {
        return this.thematicVariables[0];
    }

    /**
     * Returns an array with all the continuous variables that were created with the given options.
     * @returns {Array<CategoricalVariable>} - all the continuous variables that were created with the given options.
     * @memberof GisplayOptions
     */
    getContinousVariables() {
        return this.thematicVariables[1];
    }

    /**
     * Returns the TimeVariable instance. 
     * @returns {TimeVariable} - the time variable, or null if time wasn't given. 
     * @memberof GisplayOptions
     */
    getTimeVariable() {
        return this.timeVariable;
    }

    /**
     * Returns an array with extra variables used mainly for picking information (click on map).
     * @returns {Array<DataVariable>} - an array with extra variables used mainly for picking information or an empty array when the programmer didn't give any extra variable.
     * @memberof GisplayOptions
     */
    getOptionalVariables() {
        return this.optionalVariables;
    }

    /**
     * Returns the global options.
     * @returns {Object} - the global options. 
     * @memberof NewGisplayOptions
     */
    getGlobalOptions() {
        return this.globalOptions;
    }

    /**
     * Returns the CSV options object.
     * @returns {Object} - the CSV options object.
     * @memberof GisplayOptions
     */
    getCSV() {
        return this.csv;
    }

    /**
     * Returns the geometry variables.
     * @returns {Array<DataVariable>} - the geometry variables.
     * @memberof GisplayOptions
     */
    getGeometryVariables() {
        return this.geometryVariables;
    }
}   
