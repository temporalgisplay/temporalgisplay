import defaultsFile from './defaults.json';

/**
 * NOT YET IMPLEMENTED
 * Used to verify all errors that can be checked from the given options and the defaults and shaders files.
 * @export
 * @class GisplayErrorChecker
 */
export class GisplayErrorChecker {

    constructor(options) {

    }

    /**
     * Verifies if the parsing, mapping and global options exist.
     * @param {Object} parsingOptions 
     * @param {Object} mappingOptions 
     * @param {Object} globalOptions 
     * @memberOf GisplayErrorChecker
     */
    checkMainOptions(parsingOptions, mappingOptions, globalOptions) {
        if (!parsingOptions)
            this.alertAndThrowError('Parsing options not found');
        if (!mappingOptions)
            this.alertAndThrowError('Mapping options not found');
        if (!globalOptions)
            this.alertAndThrowError('Global options not found');
    }

    // ############################################# OLD METHODS #############################################
    /**
     * Used to check errors on the given userOptions.
     * @memberof GisplayOptions
     */
    checkPreErrors(userOptions) {
        if ((!userOptions.bounds && userOptions.layout)) {
            this.alertError();
            throw new Error("Invalid bounds and/or layout options");
        }
    }

    /**
     * This method is fired after the constructor is done and should check for all types of erros 
     * that are possible to catch at this stage (eg. did the user provide an attribute?)
     * @memberof GisplayOptions
     */
    checkPossibleErrors() {
        //Errors:
        //1 sum of layout sizes higher than 100
        // this.checkBounds();
        this.checkExistingGisplayId(this.container.id);
        this.checkValidLayoutRec(this.layout);
    }

    checkExistingGisplayId(gisplayId) {
        if (document.querySelectorAll('#' + gisplayId).length > 1) {
            this.alertError();
            throw new Error("Gisplay identifier given: " + gisplayId + " already exists");
        }
    }

    /**
     * Validates the given layout.
     * Now checks: size of each descendant does not exceeds 100 per cent and the id exists on the bounds object. 
     * @param {any} obj 
     * @memberof GisplayOptions
     */
    checkValidLayoutRec(obj) {
        let orientation = obj.vertical || obj.horizontal;
        this.checkSizes(orientation);
        for (let i = 0; i < orientation.sizes.length; i++)
            if (typeof orientation.descendants[i] === 'string') //Leaf found	
                this.checkExistingBound(orientation.descendants[i]);
            else
                this.checkValidLayoutRec(orientation.descendants[i]);
    }

    /**
     * Validates the sum of all sizes for this orientation.
     * @param {Object} orientation - The vertical/horizontal object. 
     * @memberof GisplayOptions
     */
    checkSizes(orientation) {
        let sum = 0;
        for (let size of orientation.sizes)
            sum += size;
        if (sum !== 100) {//sum > 100 || sum < 100)
            this.alertError();
            throw new Error("Sum of sizes exceeds 100 per cent at: " + JSON.stringify(orientation));
        }
    }

    /**
     * The id of the bound to check the existence on the bounds object.
     * @param {string} boundId - The bounds id. 
     * @memberof GisplayOptions
     */
    checkExistingBound(boundId) {
        // if (!boundId)
        if (!this.bounds[boundId]) {
            this.alertError();
            throw new Error("Bound named: " + boundId + " does not exist. Available bounds are: [" + Object.keys(this.bounds) + "]");
        }
    }

    /**
     * Send an alert to the user and throw the error given.
     * @param {string} errorString - The error to throw.
     * @memberOf GisplayErrorChecker
     */
    alertAndThrowError(errorString) {
        this.alertError();
        throw new Error(errorString);
    }

    //
    /**
     * This method fires an alert because the user provider wrong or conflicting options.
     * @memberof GisplayOptions
     */
    alertError() {
        alert("Error on Options object found. Check browser console for more details.");
    }

    // ############################################# DEPRECATED
    /**
     * @deprecated It's not used because the visual variables already hold all information of parsing Options.
     * Returns the mapping options.
     * @returns {Object} - the mapping options.
     * @memberof NewGisplayOptions
     */
    getMappingOptions() {
        return this.mappingOptions;
    }

    /**
     * @deprecated 
     * Process options for mapping of visual variables.
     * @param {Object} mappingOptions - Mapping options given by the user.
     * @returns {Array<Object>} - The resulting options after processing.
     * @property {string} legendTitle - The title of the legend
     * @property {string} visualVariable - The type of visual variable for each option.
     * @property {Array<Object|string|number>} mapping - The mapping between each class/category of the variable and it's value. 
     * @memberof NewGisplayOptions
     */
    processMappingOptions(mappingOptions) {
        let mappingOptionsVariables = [];
        for (let externalName of Object.keys(mappingOptions)) {
            let value = mappingOptions[externalName];
            let mappingInfoVariable = {
                externalName: externalName,
                legendTitle: value.legendTitle || externalName,
                visualVariable: value.visualVariable,
                mapping: value.mapping
            };
            mappingOptionsVariables.push(mappingInfoVariable);

            this.optionsMap.set(externalName, { parsing: this._findParsingVariable(externalName), mapping: mappingInfoVariable }); //For each visual variable create: externalName -> options
        }

        // this.optionsMap = map; //For each visual variable create: externalName -> options
        //TODO: verify errors for mapping
        //1. Should have as many keys as the sum of this.parsinOptions.categoricalVars + sequentialVars
        return mappingOptionsVariables;
    }

    /**
     * @deprecated 
     * Finds the variable on the parsing options.
     * @param {string} externalName - External name.
     * @returns {Object} - The parsing options for the variable with the given name.
     * @private 
     * @memberof NewGisplayOptions
     */
    _findParsingVariable(externalName) {
        let cat = this.parsingOptions.categoricalVars;
        let seq = this.parsingOptions.sequentialVars;
        for (let i = 0; i < cat.length; i++)
            if (cat[i].externalName === externalName)
                return cat[i];

        for (let i = 0; i < seq.length; i++)
            if (seq[i].externalName === externalName)
                return seq[i];
        return null; //TODO: throw new Error('Name not found on the parsing options.')
    }

    /**
     * @deprecated 
     * @param {any} externalName 
     * @returns 
     * @memberof GisplayOptions
     */
    _findMappingVariable(externalName) {
        let mappingOpts = this.mappingOptions;
        for (let i = 0; i < mappingOpts.length; i++)
            if (mappingOpts[i].externalName === externalName)
                return mappingOpts[i];
        return null; //TODO: throw new Error('Name not found on the parsing options.')
    }
}