import { GisplayMap } from './GisplayMap';
import { ColorBrewer } from '../Helpers/ColorBrewer';

import { GisplayDefaults } from '../GisplayDefaults';

/**
 * LinesMap implemenetation
 * @export
 * @class LinesMap
 * @extends {GisplayMap}
 */
export class LinesMap extends GisplayMap {

    /**
     * Creates an instance of LinesMap.
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @memberof LinesMap
     */
    constructor(parsingOptions, mappingOptions, globalOptions) {
        super(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Draw the lines for the given combination and temporal granule in the background map.
     * @param {MapVariableCombination} mvc - The map variable combination.
     * @param {TemporalGranule} tg - The temporal granule that contains the polygons to be drawn.    
     * @param {MapVariableCombination} bgmap - The background map where we want to draw.    
     * @memberof ProportionalSymbolsMap
     */
    draw(mvc, tg, bgmap) {
        // this.drawLines(mvc, tg, bgmap);
        this.initialDrawSetup(mvc, bgmap);
        this.setLines(tg); //Draw the lines 
    }

    /**
     * The method to be called to draw the picking data after a click on the background map.
     * @param {MapVariableCombination} mvc - The MapVariableCombination to use.
     * @param {TemporalGranule} tg - The temporal granule to be used.
     * @param {BGMapWrapper} bgmap - The background map where the click happened.
     * @param {Array<number>} textureViewPort - The viewport of the texture, where we want to draw the picking data. 
     * @memberof GisplayMap
     */
    drawPicking(mvc, tg, bgmap, textureViewPort) {
        // this.drawLinesToTexture(mvc, tg, bgmap, textureViewPort);
        this.initialPickingSetup(mvc, bgmap, textureViewPort);

        this.setPickingColorsAndLines(tg);
    }

    /**
     * Returns the default values for the color (number of classes and colors).
     * @returns {{color: {numberOfClasses: number, colors: Array<number>}}} - the default values for the color (number of classes and colors).
     * @override 
     * @memberof LinesMap
     */
    defaults() {
        let numberOfClasses = 3;
        return {
            color: {
                numberOfClasses: numberOfClasses,
                colors: this.getDefaultColors(numberOfClasses)
            }
        };
    }

    /**
     * Returns the name of the vertex and fragment shader files.
     * @returns {{vertexShaderFileName:string, fragmentShaderFileName:string}} - the name of the vertex and fragment shader files.
     * @memberof LinesMap
     */
    getShadersFileNames() {
        return { vertexShaderFileName: 'lines.vert', fragmentShaderFileName: 'lines.frag' };
    }

    /**
     * NOT USED YET
     * Returns the list of visual variables that are available for this map.
     * @returns {Array<string>} - the list of visual variables that are available for this map.
     * @memberof LinesMap
     */
    getAvailableVisualVariables() {
        return [GisplayDefaults.COLOR()];
    }

    /**
     * NOT USED YET
     * Returns the list of available temporal controls for this thematic map.
     * @returns {Array<string>} - the list of available temporal controls for this thematic map.
     * @memberof LinesMap
     */
    getAvailableTemporalControls() {
        return [GisplayDefaults.INSTANT(), GisplayDefaults.INTERVAL(), GisplayDefaults.ANIMATION()];
    }

    /**
     * Returns the colors for this map given the number of classes and the nature of the data (sequential,  diverging or qualitative). 
     * @param {number} numClasses - Number of classes. 
     * @param {string} dataNature - Nature of the data.
     * @returns {Array<Array<number>>} Default colors for the map given the number of classes and nature of data.
     * @override 
     * @memberof LinesMap
     */
    getDefaultColors(numClasses, dataNature) {
        return ColorBrewer.getDefautls('DotMap', numClasses, dataNature || GisplayDefaults.SEQUENTIAL());
    }
}