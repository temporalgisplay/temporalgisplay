import { BGMapWrapper } from './BGMapWrapper';

/**
 * Bing Maps as background map provider. 
 * @see https://msdn.microsoft.com/en-us/library/mt712542.aspx
 * @export
 * @class BGMapBingMaps
 * @extends {BGMapWrapper}
 */
export class BGMapBingMaps extends BGMapWrapper {

    /**
     * Creates an instance of Bing Maps.
     * @param {Object} bgmap - The Bing Maps object.
     * @memberof BGMapBingMaps
     */
    constructor(bgmapDiv, mapBounds, viewPort) {
        super(bgmapDiv, mapBounds, viewPort);
    }

    /**
     * Method that creates the background map and sets the maximum allowed bounds.
     * @param {string} mapId - The div where the map will be contained.
     * @param {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}} mapBounds - The max bounds allowed for this map.
     * @returns {Object} - The bing maps object.
     * @memberof BGMapBingMaps
     */
    createBackgroundMap(bgmapDiv, mapBounds) {
        let center = { lat: (mapBounds.SW.lat + mapBounds.NE.lat) / 2, lng: (mapBounds.SW.lng + mapBounds.NE.lng) / 2 };
        let map = new Microsoft.Maps.Map(bgmapDiv, {
            credentials: 'tbS2WTHvuzFWMarVDedF~XGeRrWSh4JVHd1yPrfsUJQ~Al0cgZrtc-S57hQveO9iMLRlZesJBacsvRCzVNsSh5E_TzY8iPE8w_LvRjP2_51i',
            center: new Microsoft.Maps.Location(center.lat, center.lng)
        });

        let locations = [new Microsoft.Maps.Location(mapBounds.SW.lat, mapBounds.SW.lng),
        new Microsoft.Maps.Location(mapBounds.NE.lat, mapBounds.NE.lng)];
        let bounds = Microsoft.Maps.LocationRect.fromLocations(locations);
        map.setView({ bounds: bounds });

        window.m = map;

        return map;
    }

    /**
     * Returns the map's containing HTML element.
     * @return {HTMLElement} - The map's HTML element container.
     * @memberof BGMapBingMaps
     */
    getContainer() {
        return this.bgMapObject.getRootElement();
    }

    /**
     * Returns the map's current zoom level.
     * @return {number} - The map's current zoom level.
     * @memberof BGMapBingMaps
     */
    getZoom() {
        return this.bgMapObject.getZoom();
    }

    /**
     * Returns the longitude(X) of the bounding box northwest corner.
     * @return {number} - Longitude of northwest corner, measured in degrees.
     * @memberof BGMapBingMaps
     */
    getCenterLng() {
        return this.bgMapObject.getCenter().longitude;
    }

    /**
     * Returns the latitude(Y) of the bounding box northwest corner.
     * @return {number} - Latitude of northwest corner, measured in degrees.
     * @memberof BGMapBingMaps
     */
    getCenterLat() {
        return this.bgMapObject.getCenter().latitude;
    }

    /**
     * Add Pan/Drag event.
     * @param {Function} func - The function to be called when the user performs drag on the map.
     * @see https://www.bing.com/api/maps/sdkrelease/mapcontrol/isdk#layerEvents+JS
     * @see https://msdn.microsoft.com/en-us/library/mt736399.aspx
     * @memberof BGMapBingMaps
     */
    addDragEvent(func) {
        Microsoft.Maps.Events.addHandler(this.getBackgroundMapProviderObject(), 'mousewheel', func);
        // this.addEventListener('move', fun);
    }

    /**
     * Add zoom event.
     * @param {Function} func - The function to be called when the user performs zoom in/out on the map.
     * @see https://www.bing.com/api/maps/sdkrelease/mapcontrol/isdk#layerEvents+JS
     * @see https://msdn.microsoft.com/en-us/library/mt736399.aspx
     * @memberof BGMapBingMaps
     */
    addZoomEvent(func) {
        Microsoft.Maps.Events.addHandler(this.getBackgroundMapProviderObject(), 'viewchangeend', func);
    }

    /**
     * Add click event.
     * @param {GisplayMap} gisplayMap - The gisplay map object.
     * @see https://www.bing.com/api/maps/sdkrelease/mapcontrol/isdk#layerEvents+JS
     * @see https://msdn.microsoft.com/en-us/library/mt736399.aspx
     * @memberof BGMapBingMaps
     */
    addClickEvent(gisplayMap) {
        Microsoft.Maps.Events.addHandler(this.getBackgroundMapProviderObject(), 'click', (e) => {
            let rect = this.bgmapDiv.getClientRects();
            let c1 = { x: rect[0].left, y: rect[0].top };
            let p1 = { x: e.pageX, y: e.pageY };
            let point = { x: p1.x - c1.x, y: p1.y - c1.y };
            gisplayMap.clickEvent(point.x, point.y, this);
        });
    }
}