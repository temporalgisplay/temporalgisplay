/**
 * This class represents a background map wrapper. Used to be a "middle-man" between the 
 * background map provider and the Gisplay API.
 * @see https://www.mapbox.com/mapbox-gl-js/api/ 
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference
 * @see https://developer.here.com/develop/javascript-api 
 * @see Diogo's thesis Page 70-72 + 59
 */
export class BGMapWrapper {

    /**
     * Creates an instance of BGMapWrapper.
     * @param {Object} bgmap - The background map object that came from the provider (e.g., Mapbox, Google Maps). 
     */
    constructor(bgmapDiv, mapBounds, viewPort) {
        /**
         * This is the background map object that comes from the background map provider(e.g., MapBox). 
         * @type {Object}
         */
        this.bgMapObject = this.createBackgroundMap(bgmapDiv, mapBounds);
        /**
         * The div element that contains this map.
         * @type {HTMLDivElement}
         */
        this.bgmapDiv = bgmapDiv;
        /**
         * The bounds (SW and NE points) of this background map.
         * @type {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}}
         */
        this.mapBounds = mapBounds;
        /**
         * Position of this map on the Canvas. Positions are (left, bottom, width, height).
         * @type {{left:number, bottom:number, width: number, height: number}}
         */
        this.viewPort = viewPort;
        /**
         * The description to add to the background map bottom corner. Used to identify the area being presented.
         * @type {string}
         */
        this.description = "Mainland USA";
        /**
         * The size of each background map tile. All existing maps use 256x256, any new background map that has a different tile size must override this variable.
         * @type {number}
         */
        this.tileSize = 256;
    }

    /**
     * Resize the background map object, when a window resize event occurs.
     * @abstract 
     * @memberof BGMapWrapper
     */
    resize() {
    }

    /**
     * Method that creates the background map and sets the maximum allowed bounds.
     * @param {string} mapId - The div where the map will be contained.
     * @param {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}} mapBounds - The max bounds allowed for this map.
     * @memberof BGMapWrapper
     */
    createBackgroundMap(mapId, mapBounds) {
        throw new Error("Not implemented");
    }

    //DELETE THIS
    /**
     * Returns the map's containing HTML element.
     * @return {HTMLElement} - The map's HTML element container.
     * @memberof BGMapWrapper
     */
    getContainer() {
        throw new Error("Not implemented.");
    }

    /**
     * Returns the width of the canvas elment.
     * @returns {number} the width of the canvas elment.
     * @memberof BGMapWrapper
     */
    getWidth() {
        return this.getContainer().offsetWidth;
    }

    /**
     * Returns the height of the canvas element.
     * @returns {number} the height of the canvas elment.
     * @memberof BGMapWrapper
     */
    getHeight() {
        return this.getContainer().offsetHeight;
    }

    /**
     * Returns the bounds for this background map.
     * @returns { number } - the bounds for this background map.
     * @memberof BGMapWrapper
     */
    getMapBounds() {
        return this.mapBounds;
    }

    /**
     * Returns the viewport values for this background map.
     * @returns {Array<number>} -  the viewport values for this background map.
     * @memberof BGMapWrapper
     */
    getViewPort() {
        return this.viewPort;
    }

    /**
     * Calculates the viewport.
     * @returns {Array<number>} -  the viewport values for this background map.
     * @memberof BGMapWrapper
     */
    calculateViewPort(canvas) {
        let canvasWidth = canvas.offsetWidth,
            canvasHeight = canvas.offsetHeight;
        return [canvasWidth * this.viewPort.left, canvasHeight * this.viewPort.bottom, canvasWidth * this.viewPort.width, canvasHeight * this.viewPort.height];
    }

    /**
     * Calculates the power of two value equal or higher than the background map div width and height.
     * @param {any} canvas 
     * @returns 
     * @memberof BGMapWrapper
     */
    calcNextPowerOfTwo() {
        let bgmapWidth = this.bgmapDiv.offsetWidth,
            bgmapHeight = this.bgmapDiv.offsetHeight;
        return [this._calcNextPowerOfTwoHelper(bgmapWidth), this._calcNextPowerOfTwoHelper(bgmapHeight)];
    }

    /**
     * Method that calculates the next power of two equal or higher than the given value.
     * @param {number} value - The width/height of the viewport. 
     * @returns {number} - The next power of two after the given value.
     * @private 
     * @memberof BGMapWrapper
     */
    _calcNextPowerOfTwoHelper(value) {
        let powerOfTwoVals = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072]; //2^1 to 2^17
        // for (let i = 0; i < powerOfTwoVals.length; i++)
        for (let powerVal of powerOfTwoVals)
            if (powerVal >= value)
                return powerVal;
    }

    /**
     * Returns the map's current zoom level.
     * @return {number} - The map's current zoom level.
     * @memberof BGMapWrapper
     */
    getZoom() {
        throw new Error("Not implemented.");
    }

    /**
     * Returns the longitude of the bounding box northwest corner.
     * @return {number} - Longitude of northwest corner, measured in degrees.
     * @memberof BGMapWrapper
     */
    getCenterLng() {
        throw new Error("Not implemented.");
    }

    /**
     * Returns the latitude of the bounding box northwest corner.
     * @return {number} - Latitude of northwest corner, measured in degrees.
     * @memberof BGMapWrapper
     */
    getCenterLat() {
        throw new Error("Not implemented.");
    }

    /**
     * Adds a listener to a specified event type.
     * @param {string} eventstr - The event type to add a listen for.
     * @param {Function} eventfunction - The function to be called when the event is fired. The listener function is called with the data object passed to  fire , extended with  target and  type properties.
     * @memberof BGMapWrapper
     */
    addEventListener(eventstr, eventfunction) {
        throw new Error("Not implemented.");
    }

    /**
     * Add drag event.
     * @param {Function} func - The function to be called when the user performs drag on the map.
     * @memberof BGMapWrapper
     */
    addDragEvent(func) {
        throw new Error("Not implemented.");
    }

    /**
     * Add zoom event.
     * @param {Function} func - The function to be called when the user performs zoom in/out on the map.
     * @memberof BGMapWrapper
     */
    addZoomEvent(func) {
        throw new Error("Not implemented.");
    }

    /**
     * Add click event. Calculate the position of the click event relative to the containing div (bgmapDiv).
     * With the calculated x,y then call GisplayMap to proceed the hit testing.
     * @param {Map} map - The function to be called when the user clicks on the map.
     * @memberof BGMapWrapper
     */
    addClickEvent(map) {
        throw new Error("Not implemented.");
    }

    /**
     * Returns the background map object. This is the Background provider object (e.g., Mapbox, GMaps, HereMaps, Bing Maps).
     * @returns {BGMapWrapper#bgMapObject} the background map object.
     * @memberof BGMapWrapper
     */
    getBackgroundMapProviderObject() {
        return this.bgMapObject;
    }

    /**
     * Returns the tile size used to calculate the projection matrix for WebGL.
     * @returns {number} - the tile size used to calculate the projection matrix for WebGL.
     * @memberof BGMapWrapper
     */
    getTileSize() {
        return this.tileSize;
    }

    /**
     * Returns the div of this background map.
     * @returns {HTMLDivElement} - the div of this background map.
     * @memberof BGMapWrapper
     */
    getDiv() {
        return this.bgmapDiv;
    }
}
