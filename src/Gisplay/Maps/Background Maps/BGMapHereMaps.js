import { BGMapWrapper } from './BGMapWrapper';

/**
 * Here Maps as background map provider. 
 * @see https://developer.here.com/develop/javascript-api
 * @see https://developer.here.com/javascript-apis/documentation/v3/maps/topics/quick-start.html
 * @export
 * @class HereMapsBGMap
 * @extends {BGMapWrapper}
 */
export class BGMapHereMaps extends BGMapWrapper {

    /**
     * Creates an instance of Here Maps.
     * @param {Object} bgmap - The Here Maps object.
     * @memberOf HereMapsBGMap
     */
    constructor(bgmapDiv, mapBounds, viewPort) {
        super(bgmapDiv, mapBounds, viewPort);
    }

    /**
     * Method that creates the background map and sets the maximum allowed bounds.
     * @param {string} mapId - The div where the map will be contained.
     * @param {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}} mapBounds - The max bounds allowed for this map.
     * @returns {Object} - The google maps object.
     * @see https://developer.here.com/api-explorer/maps-js/v3.0/maps/map-using-view-bounds
     * @memberof BGMapHereMaps
     */
    createBackgroundMap(bgmapDiv, mapBounds) {
        let platform = new H.service.Platform({
            'app_id': 'L8PMOO22XlBlfofBNLTH',
            'app_code': '_L7WMKrjcArpjlJenPuuzg'
        });
        let defaultLayers = platform.createDefaultLayers();
        let map = new H.Map(
            bgmapDiv,
            defaultLayers.normal.map,
            {
                center: { lat: (mapBounds.SW.lat + mapBounds.NE.lat) / 2, lng: (mapBounds.SW.lng + mapBounds.NE.lng) / 2 }
            }
        );
        map.setZoom(5);

        let mapEvents = new H.mapevents.MapEvents(map);//Enable events
        let behavior = new H.mapevents.Behavior(mapEvents);//Enable zoom and pan

        let nw = [mapBounds.NE.lat, mapBounds.SW.lng],
            se = [mapBounds.SW.lat, mapBounds.NE.lng];

        let bbox = new H.geo.Rect(...nw, ...se);//42.3736, -71.0751, 42.3472, -71.0408);
        // map.setViewBounds(bbox);

        //Restrict pan outside bounds
   /*      map.getViewModel().addEventListener('sync', () => {
            let center = map.getCenter();
            // console.warn(this.bgmapDiv.id, center);
            if (!bbox.containsPoint(center)) {
                if (center.lat > bbox.getTop()) {
                    center.lat = bbox.getTop();
                } else if (center.lat < bbox.getBottom()) {
                    center.lat = bbox.getBottom();
                }
                if (center.lng < bbox.getLeft()) {
                    center.lng = bbox.getLeft();
                } else if (center.lng > bbox.getRight()) {
                    center.lng = bbox.getRight();
                }
                // console.warn("2", this.bgmapDiv.id, center);
                map.setCenter(center);
            }
        }); */

        window.m = map;

        return map;
    }

    /**
     * 
     * Resize the background map object, when a window resize event occurs.
     * @see https://stackoverflow.com/a/26295344
     * @memberof BGMapHereMaps
     */
    resize() {
        this.bgMapObject.getViewPort().resize();
    }

    /**
     * Returns the map's containing HTML element.
     * @return {HTMLElement} - The map's HTML element container.
     * @memberof BGMapHereMaps
     */
    getContainer() {
        return this.bgMapObject.getElement();
    }

    /**
     * Returns the map's current zoom level.
     * @return {number} - The map's current zoom level.
     * @memberof BGMapHereMaps
     */
    getZoom() {
        return this.bgMapObject.getZoom();
    }

    /**
     * Returns the longitude(X) of the bounding box northwest corner.
     * @return {number} - Longitude of northwest corner, measured in degrees.
     * @see https://developer.here.com/api-explorer/maps-js/v3.0/infoBubbles/position-on-mouse-click
     * @memberof BGMapHereMaps
     */
    getCenterLng() {
        return ((((180 + this.bgMapObject.getCenter().lng) % 360) + 360) % 360) - 180;
    }

    /**
     * Returns the latitude(Y) of the bounding box northwest corner.
     * @return {number} - Latitude of northwest corner, measured in degrees.
     * @see https://developer.here.com/api-explorer/maps-js/v3.0/infoBubbles/position-on-mouse-click
     * @memberof BGMapHereMaps
     */
    getCenterLat() {
        return this.bgMapObject.getCenter().lat;
    }

    /**
     * Adds a listener to a specified event type.
     * @param {string} eventstr - The event type to add a listen for.
     * @param {Function} eventfunction - The function to be called when the event is fired. The listener function is called with the data object passed to  fire , extended with  target and  type properties.
     * @memberof BGMapHereMaps
     */
    addEventListener(eventstr, eventfunction) {
        this.bgMapObject.addEventListener(eventstr, eventfunction);
    }

    /**
     * Add Pan/Drag event.
     * @param {Function} func - The function to be called when the user performs drag on the map.
     * @see https://developer.here.com/documentation/maps/topics/events.html
     * @see https://developer.here.com/documentation/maps/topics_api/h-mapevents-mapevents.html#h-mapevents-mapevents__drag-event
     * @memberof BGMapHereMaps
     */
    addDragEvent(func) {
        this.addEventListener('drag', func);
    }

    /**
     * Add zoom event.
     * @param {Function} func - The function to be called when the user performs zoom in/out on the map.
     * @see https://developer.here.com/documentation/maps/topics/events.html
     * @memberof BGMapHereMaps
     */
    addZoomEvent(func) {
        this.addEventListener('mapviewchangeend', func);
    }

    /**
     * Add click event.
     * @param {GisplayMap} gisplayMap - The gisplay map object.
     * @see https://developer.here.com/documentation/maps/topics/events.html
     * @see https://developer.here.com/documentation/maps/topics_api/h-mapevents-mapevents.html#h-mapevents-mapevents__tap-event
     * @memberof BGMapHereMaps
     */
    addClickEvent(gisplayMap) {
        this.addEventListener('tap', e => {
            let rect = this.bgmapDiv.getClientRects();
            let c1 = { x: rect[0].left, y: rect[0].top };
            let p1 = { x: e.originalEvent.pageX, y: e.originalEvent.pageY };
            let point = { x: p1.x - c1.x, y: p1.y - c1.y };
            gisplayMap.clickEvent(point.x, point.y, this);
        });
    }
}