import { BGMapWrapper } from './BGMapWrapper';

/**
 * Google Maps as background map provider. 
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference
 * @export
 * @class GoogleMapsBGMap
 * @extends {BGMapWrapper}
 */
export class BGMapGoogleMaps extends BGMapWrapper {

    /**
     * Creates an instance of GoogleMapsBGMap.
     * @param {Object} bgmap - The Google Maps object.
     * @memberOf GoogleMapsBGMap
     */
    constructor(bgmapDiv, mapBounds, viewPort) {
        super(bgmapDiv, mapBounds, viewPort);
    }

    /**
     * Method that creates the background map and sets the maximum allowed bounds.
     * @param {string} mapId - The div where the map will be contained.
     * @param {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}} mapBounds - The max bounds allowed for this map.
     * @returns {Object} - The google maps object.
     * @see https://developers.google.com/maps/documentation/javascript/examples/control-disableUI
     * @memberof BGMapGoogleMaps
     */
    createBackgroundMap(bgmapDiv, mapBounds) {
        let map = new google.maps.Map(bgmapDiv, { disableDefaultUI: true });

        let sw = new google.maps.LatLng(mapBounds.SW.lat, mapBounds.SW.lng),
            ne = new google.maps.LatLng(mapBounds.NE.lat, mapBounds.NE.lng),
            center = new google.maps.LatLng((mapBounds.SW.lat + mapBounds.NE.lat) / 2, (mapBounds.SW.lng + mapBounds.NE.lng) / 2);

        let maxBounds = new google.maps.LatLngBounds();
        maxBounds.extend(sw);
        maxBounds.extend(ne);
        map.setCenter(center);
        map.fitBounds(maxBounds);

        // In case we go out of the maxBounds this will reset the initial center.
        /* map.addListener('bounds_changed', () => {
            if (maxBounds.contains(map.getCenter()))
                return;
            else
                map.setCenter(center);
        }); */

        window.m = map;

        return map;
    }

    /**
     * Returns the map's containing HTML element.
     * @return {HTMLElement} - The map's HTML element container.
     * @memberof BGMapGoogleMaps
     */
    getContainer() {
        return this.bgMapObject.getDiv();
    }

    /**
     * Returns the map's current zoom level.
     * @return {number} - The map's current zoom level.
     * @memberof BGMapGoogleMaps
     */
    getZoom() {
        return this.bgMapObject.getZoom();
    }

    /**
     * Returns the longitude(X) of the bounding box northwest corner.
     * @return {number} - Longitude of northwest corner, measured in degrees.
     * @see http://stackoverflow.com/a/6913505
     * @memberof BGMapGoogleMaps
     */
    getCenterLng() {
        return ((((180 + this.bgMapObject.getCenter().lng()) % 360) + 360) % 360) - 180;
    }

    /**
     * Returns the latitude(Y) of the bounding box northwest corner.
     * @return {number} - Latitude of northwest corner, measured in degrees.
     * @see http://stackoverflow.com/a/6913505
     * @memberof BGMapGoogleMaps
     */
    getCenterLat() {
        return this.bgMapObject.getCenter().lat();
    }

    /**
     * Adds a listener to a specified event type.
     * @param {string} eventstr - The event type to add a listen for.
     * @param {Function} eventfunction - The function to be called when the event is fired. The listener function is called with the data object passed to  fire , extended with  target and  type properties.
     * @memberof BGMapGoogleMaps
     */
    addEventListener(eventstr, eventfunction) {
        this.bgMapObject.addListener(eventstr, eventfunction);
    }

    /**
     * Add Pan/Drag event.
     * @param {Function} func - The function to be called when the user performs drag on the map.
     * @see https://developers.google.com/maps/documentation/javascript/events
     * @see https://developers.google.com/maps/documentation/javascript/reference#Marker 
     * @memberof BGMapGoogleMaps
     */
    addDragEvent(func) {
        this.addEventListener('drag', func);
    }

    /**
     * Add zoom event.
     * @param {Function} func - The function to be called when the user performs zoom in/out on the map.
     * @see https://developers.google.com/maps/documentation/javascript/events
     * @see https://developers.google.com/maps/documentation/javascript/reference#Marker 
     * @memberof BGMapGoogleMaps
     */
    addZoomEvent(func) {
        // this.addEventListener('zoom_changed', fun);
        this.addEventListener('idle', func); //This event is fired when the map becomes idle after panning or zooming.
    }

    /**
     * Add click event.
     * @param {GisplayMap} gisplayMap - The gisplay map object.
     * @see https://developers.google.com/maps/documentation/javascript/events
     * @see https://developers.google.com/maps/documentation/javascript/reference#Marker 
     * @memberof BGMapGoogleMaps
     */
    addClickEvent(gisplayMap) {
        this.addEventListener('click', e => {
            gisplayMap.clickEvent(e.pixel.x, e.pixel.y, this);
        });
    }
}