import { BGMapWrapper } from './BGMapWrapper';

/**
 * MapBox JS as background map provider.
 * @see https://www.mapbox.com/mapbox.js/api/v3.1.1/
 * @export
 * @class BGMapMapBox
 * @extends {BGMapWrapper}
 */
export class BGMapMapBox extends BGMapWrapper {

    /**
     * Creates an instance of BGMapWrapper.
     * @param {Object} bgmap - The background map object that came from the provider (e.g., Mapbox, Google Maps). 
     */
    constructor(bgmapDiv, mapBounds, viewPort) {
        super(bgmapDiv, mapBounds, viewPort);
    }

    /**
     * Method that creates the background map and sets the maximum allowed bounds.
     * @param {string} bgmapDiv - The div where the map will be contained.
     * @param {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}} mapBounds - The max bounds allowed for this map.
     * @returns {Object} - The google maps object.
     * @memberof BGMapMapBox
     */
    createBackgroundMap(bgmapDiv, mapBounds) {
        L.mapbox.accessToken = 'pk.eyJ1IjoibG9sYXNkIiwiYSI6ImNpbmxsZDJkejAwOHR2Zm0yZHVwOWV1ejEifQ.SJ6CupBlW0gPic0n-HgY6w';

        let southWest = L.latLng(mapBounds.SW.lat, mapBounds.SW.lng),
            northEast = L.latLng(mapBounds.NE.lat, mapBounds.NE.lng),
            mapbounds = L.latLngBounds(southWest, northEast);

        let map = L.mapbox.map(bgmapDiv.id, 'mapbox.streets', {
            // set that bounding box as maxBounds to restrict moving the map
            // see full maxBounds documentation:
            // http://leafletjs.com/reference.html#map-maxbounds
            maxBounds: mapbounds,
            zoom: 10,
            attributionControl: true
        });

        // zoom the map to that bounding box
        map.fitBounds(mapbounds);

        window.m = map;

        return map;
    }

    /**
     * Resize the background map object, when a window resize event occurs.
     * @memberof BGMapMapBoxGLJS
     */
    resize() {
        this.bgMapObject.invalidateSize();
    }


    /**
     * Returns the map's containing HTML element.
     * @return {HTMLElement} - The map's HTML element container.
     * @memberof BGMapMapBox
     */
    getContainer() {
        return this.bgMapObject.getContainer();
    }

    /**
     * Returns the map's current zoom level.
     * @return {number} - The map's current zoom level.
     * @memberof BGMapMapBox
     */
    getZoom() {
        return this.bgMapObject.getZoom();
    }

    /**
     * Returns the longitude of the bounding box northwest corner.
     * @return {number} - Longitude of northwest corner, measured in degrees.
     * @memberof BGMapMapBox
     */
    getCenterLng() {
        return ((((180 + this.bgMapObject.getCenter().lng) % 360) + 360) % 360) - 180;
    }

    /**
     * Returns the latitude of the bounding box northwest corner.
     * @return {number} - Latitude of northwest corner, measured in degrees.
     * @memberof BGMapMapBox
     */
    getCenterLat() {
        return this.bgMapObject.getCenter().lat;
    }

    /**
     * Adds a listener to a specified event type.
     * @param {string} eventstr - The event type to add a listen for.
     * @param {Function} eventfunction - The function to be called when the event is fired. The listener function is called with the data object passed to  fire , extended with  target and  type properties.
     * @return {void} 
     * @memberof BGMapMapBox
     */
    addEventListener(eventstr, eventfunction) {
        this.bgMapObject.on(eventstr, eventfunction);
    }

    /**
     * Add Pan/Drag event.
     * @param {Function} func - The function to be called when the user performs drag on the map.
     * @see http://leafletjs.com/reference-1.2.0.html#map-move
     * @memberof BGMapMapBox
     */
    addDragEvent(func) {
        this.addEventListener('move', func);
        /* this.addEventListener('movestart', func);
        this.addEventListener('moveend', func); */
    }

    /**
     * Add zoom event.
     * @param {Function} func - The function to be called when the user performs zoom in/out on the map.
     * @see http://leafletjs.com/reference-1.2.0.html#map-zoomend
     * @memberof BGMapMapBox
     */
    addZoomEvent(func) {
        this.addEventListener('zoomend', func);
    }

    /**
     * Add click event.
     * @param {GisplayMap} gisplayMap - The gisplay map object.
     * @see https://jsfiddle.net/5pp9pqf2/
     * @see http://leafletjs.com/reference-1.2.0.html#map-click
     * @memberof BGMapMapBox
     */
    addClickEvent(gisplayMap) {
        this.addEventListener('click', e => {
            //https://jsfiddle.net/5pp9pqf2/
            let rect = this.bgmapDiv.getClientRects();
            let c1 = { x: rect[0].left, y: rect[0].top };
            let p1 = { x: e.originalEvent.pageX, y: e.originalEvent.pageY };
            let point = { x: p1.x - c1.x, y: p1.y - c1.y };
            gisplayMap.clickEvent(point.x, point.y, this);
        });
    }
}
