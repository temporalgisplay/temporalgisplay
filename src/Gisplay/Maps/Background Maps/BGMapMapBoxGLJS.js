import { BGMapWrapper } from './BGMapWrapper';

/**
 * MapBox GL JS as background map provider.
 * @see https://www.mapbox.com/mapbox-gl-js/api/
 * @export
 * @class BGMapMapBoxGLJS
 * @extends {BGMapWrapper}
 */
export class BGMapMapBoxGLJS extends BGMapWrapper {

    /**
     * Creates an instance of BGMapMapBoxGLJS.
     * @param {HTMLDivElement} bgmapDiv 
     * @param {any} mapBounds 
     * @param {any} viewPort 
     * @memberof BGMapMapBoxGLJS
     */
    constructor(bgmapDiv, mapBounds, viewPort) {
        super(bgmapDiv, mapBounds, viewPort);
        /**
         * The tile size for this specific background map.
         * @type {number}
         */
        this.tileSize = 512;
    }

    /**
     * Method that creates the background map and sets the maximum allowed bounds.
     * @param {string} bgmapDiv - The div where the map will be contained.
     * @param {{SW: {lat:number, lng:number}, NE: {lng:number, lat:number}}} mapBounds - The max bounds allowed for this map.
     * @returns {Object} - The google maps object.
     * @see https://www.mapbox.com/mapbox-gl-js/example/fitbounds/
     * @see https://www.mapbox.com/mapbox-gl-js/example/restrict-bounds/
     * @memberof BGMapMapBoxGLJS
     */
    createBackgroundMap(bgmapDiv, mapBounds) {
        mapboxgl.accessToken = 'pk.eyJ1IjoibG9sYXNkIiwiYSI6ImNpbmxsZDJkejAwOHR2Zm0yZHVwOWV1ejEifQ.SJ6CupBlW0gPic0n-HgY6w';

        let mapbounds = [
            [mapBounds.SW.lng, mapBounds.SW.lat], // Southwest coordinates
            [mapBounds.NE.lng, mapBounds.NE.lat]  // Northeast coordinates
        ];

        let center = [(mapBounds.SW.lng + mapBounds.NE.lng) / 2,
        (mapBounds.SW.lat + mapBounds.NE.lat) / 2];

        let map = new mapboxgl.Map({
            container: bgmapDiv.id,
            zoom: 6,
            style: 'mapbox://styles/mapbox/dark-v9',
            // maxBounds: mapbounds,
            fitBounds: mapBounds,
            center: center
        });

        // this.resize(map);

        window.m = map;

        return map;
    }

    /**
     * Resize the background map object, when a window resize event occurs.
     * @memberof BGMapMapBoxGLJS
     */
    resize() {
        this.bgMapObject.resize();
    }

    /**
     * Returns the map's containing HTML element.
     * @return {HTMLElement} - The map's HTML element container.
     * @memberof BGMapMapBoxGLJS
     */
    getContainer() {
        return this.bgMapObject.getContainer();
    }

    /**
     * Returns the map's current zoom level.
     * @return {number} - The map's current zoom level.
     * @memberof BGMapMapBoxGLJS
     */
    getZoom() {
        return this.bgMapObject.getZoom();
    }

    /**
     * Returns the longitude of the bounding box northwest corner.
     * @return {number} - Longitude of northwest corner, measured in degrees.
     * @memberof BGMapMapBoxGLJS
     */
    getCenterLng() {
        return ((((180 + this.bgMapObject.getCenter().lng) % 360) + 360) % 360) - 180;
    }

    /**
     * Returns the latitude of the bounding box northwest corner.
     * @return {number} - Latitude of northwest corner, measured in degrees.
     * @memberof BGMapMapBoxGLJS
     */
    getCenterLat() {
        return this.bgMapObject.getCenter().lat;
    }

    /**
     * Adds a listener to a specified event type.
     * @param {string} eventstr - The event type to add a listen for.
     * @param {Function} eventfunction - The function to be called when the event is fired. The listener function is called with the data object passed to  fire , extended with  target and  type properties.
     * @return {void} 
     * @memberof BGMapMapBoxGLJS
     */
    addEventListener(eventstr, eventfunction) {
        this.bgMapObject.on(eventstr, eventfunction);
    }

    /**
     * Add Pan/Drag event.
     * @param {Function} func - The function to be called when the user performs drag on the map.
     * @see https://www.mapbox.com/mapbox-gl-js/api/#map.event:drag
     * @memberof BGMapMapBoxGLJS
     */
    addDragEvent(func) {
        this.addEventListener('drag', func);
    }

    /**
     * Add zoom event.
     * @param {Function} func - The function to be called when the user performs zoom in/out on the map.
     * @see https://www.mapbox.com/mapbox-gl-js/api/#map.event:zoomend
     * @memberof BGMapMapBoxGLJS
     */
    addZoomEvent(func) {
        this.addEventListener('zoomend', func);
    }

    /**
     * Add click event.
     * @param {GisplayMap} gisplayMap - The gisplay map object.
     * @see https://jsfiddle.net/5pp9pqf2/
     * @see https://www.mapbox.com/mapbox-gl-js/api/#map.event:click
     * @memberof BGMapMapBoxGLJS
     */
    addClickEvent(gisplayMap) {
        this.addEventListener('click', e => {
            //https://jsfiddle.net/5pp9pqf2/
            let rect = this.bgmapDiv.getClientRects();
            let c1 = { x: rect[0].left, y: rect[0].top };
            let p1 = { x: e.originalEvent.pageX, y: e.originalEvent.pageY };
            let point = { x: p1.x - c1.x, y: p1.y - c1.y };
            gisplayMap.clickEvent(point.x, point.y, this);
        });
    }
}
