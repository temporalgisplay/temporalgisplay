import { GisplayMap } from './GisplayMap';
import { ColorBrewer } from '../Helpers/ColorBrewer';

import { GisplayDefaults } from '../GisplayDefaults';

/**
 * FiguresMap implemenetation
 * @export
 * @class FiguresMap
 * @extends {GisplayMap}
 */
export class FiguresMap extends GisplayMap {

    /**
     * Creates an instance of DotMap.
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @memberof FiguresMap
     */
    constructor(parsingOptions, mappingOptions, globalOptions) {
        super(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Draw the figures for the given combination and temporal granule in the background map.
     * @param {MapVariableCombination} mvc - The map variable combination.
     * @param {TemporalGranule} tg - The temporal granule that contains the polygons to be drawn.    
     * @param {MapVariableCombination} bgmap - The background map where we want to draw.    
     */
    draw(mvc, tg, bgmap) {
        // this.drawFigures(mvc, tg, bgmap);
        this.initialDrawSetup(mvc, bgmap);

        this.createImageTexture(GisplayDefaults.ORIENTATION()); // LOAD TEXTURE IMAGE
        this._setFigureIndex(mvc); // FIGURE INDEX <<< @TODO METHOD uses a string 'figureIndex' how to change?
        this.setOrientation(mvc);
        this.setSizeMinMaxPixels(); //Min and max pixels default values from defaults.json

        //SIZE
        if (mvc.hasSize()) {
            this.setSizeMinMaxData(mvc); //Min and max data values
            this.setSizeAttributeMultiple(tg);
        }
        else
            this.setSizeAttributeUnique(mvc);
        this.setPoints(tg); //POSITION POINTS
    }

    /**
     * The method to be called to draw the picking data after a click on the background map.
     * @param {MapVariableCombination} mvc - The MapVariableCombination to use.
     * @param {TemporalGranule} tg - The temporal granule to be used.
     * @param {BGMapWrapper} bgmap - The background map where the click happened.
     * @param {Array<number>} textureViewPort - The viewport of the texture, where we want to draw the picking data. 
     * @memberof GisplayMap
     */
    drawPicking(mvc, tg, bgmap, textureViewPort) {
        // this.drawFiguresToTexture(mvc, tg, bgmap, textureViewPort);
        this.initialPickingSetup(mvc, bgmap, textureViewPort);

        this.createImageTexture(GisplayDefaults.ORIENTATION()); // LOAD TEXTURE IMAGE
        this._setFigureIndex(mvc); // FIGURE INDEX <<< @TODO METHOD uses a string 'figureIndex' how to change?
        this.setOrientation(mvc);
        this.setSizeMinMaxPixels(); //Min and max pixels default values from defaults.json

        //SIZE
        if (mvc.hasSize()) {
            this.setSizeMinMaxData(mvc); //Min and max data values
            this.setSizeAttributeMultiple(tg);
        }
        else
            this.setSizeAttributeUnique(mvc);

        this.setPickingColorsAndPoints(tg);
    }

    /**
     * Returns the default values for the color (number of classes and colors).
     * @returns {{color: {numberOfClasses: number, colors: Array<number>}}} - the default values for the color (number of classes and colors).
     * @override 
     * @memberof FiguresMap
     */
    defaults() {
        let numberOfClasses = 3;
        return {
            color: {
                numberOfClasses: numberOfClasses,
                colors: this.getDefaultColors(numberOfClasses)
            }
        };
    }

    /**
     * Returns the name of the vertex and fragment shader files.
     * @returns {{vertexShaderFileName:string, fragmentShaderFileName:string}} - the name of the vertex and fragment shader files.
     * @memberof FiguresMap
     */
    getShadersFileNames() {
        return { vertexShaderFileName: 'figures.vert', fragmentShaderFileName: 'figures.frag' };
    }

    /**
     * NOT USED YET
     * Returns the list of visual variables that are available for this map.
     * @returns {Array<string>} - the list of visual variables that are available for this map.
     * @memberof FiguresMap
     */
    getAvailableVisualVariables() {
        return [GisplayDefaults.COLOR(), GisplayDefaults.SHAPE(), GisplayDefaults.SIZE(), GisplayDefaults.ORIENTATION()];
    }

    /**
     * NOT USED YET
     * Returns the list of available temporal controls for this thematic map.
     * @returns {Array<string>} - the list of available temporal controls for this thematic map.
     * @memberof FiguresMap
     */
    getAvailableTemporalControls() {
        return [GisplayDefaults.INSTANT(), GisplayDefaults.INTERVAL(), GisplayDefaults.ANIMATION()];
    }

    /**
     * Returns the colors for this map given the number of classes and the nature of the data (sequential,  diverging or qualitative). 
     * @param {number} numClasses - Number of classes. 
     * @param {string} dataNature - Nature of the data.
     * @returns {Array<Array<number>>} Default colors for the map given the number of classes and nature of data.
     * @override 
     * @memberof FiguresMap
     */
    getDefaultColors(numClasses, dataNature) {
        return ColorBrewer.getDefautls('DotMap', numClasses, dataNature || GisplayDefaults.SEQUENTIAL());
    }
}