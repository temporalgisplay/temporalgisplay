import { GisplayMap } from './GisplayMap';
// import { Legend } from '../Layout/Legend';
import { ColorBrewer } from '../Helpers/ColorBrewer';

import { GisplayDefaults } from '../GisplayDefaults';

/**
 * Choropleth implementation.
 */
export class Choropleth extends GisplayMap {
    /**
     * Creates an instance of Choropleth.
     * @param {Object} parsingOptions - The parsing options.
     * @param {Object} mappingOptions - The mapping options.
     * @param {Object} globalOptions - The globabl options.
     * @memberof Choropleth
     */
    constructor(parsingOptions, mappingOptions, globalOptions) {
        super(parsingOptions, mappingOptions, globalOptions);
    }

    /**
     * Draw the polygons and borders for the given combination and temporal granule in the background map.
     * @param {MapVariableCombination} mvc - The map variable combination.
     * @param {TemporalGranule} tg - The temporal granule that contains the polygons to be drawn.    
     * @param {MapVariableCombination} bgmap - The background map where we want to draw.    
     */
    draw(mvc, tg, bgmap) {
        // console.time('drawChoro');
        this._drawPolygons(mvc, tg, bgmap);
        this._drawBorders(mvc, tg, bgmap);
        // console.timeEnd('drawChoro');
    }

    /**
     * Draw the polygons for the given combination and temporal granule in the given background map.
     * @param {MapVariableCombination} mvc - The map variable combination.
     * @param {TemporalGranule} tg - The temporal granule that contains the polygons to be drawn.    
     * @param {MapVariableCombination} bgmap - The background map where we want to draw.   
     * @private
     */
    _drawPolygons(mvc, tg, bgmap) {
        this.initialDrawSetup(mvc, bgmap);

        this.createImageTexture(GisplayDefaults.TEXTURE());
        this.setTextureIndex(mvc); // PATTERN INDEX
        this.setTextureSize();
        this.setPolygons(tg); //POSITION POLYGONS
    }

    /**
     * Draw the borders for the given combination and temporal granule in the given background map.
     * @param {MapVariableCombination} mvc - The map variable combination.
     * @param {TemporalGranule} tg - The temporal granule that contains the polygons to be drawn.    
     * @param {MapVariableCombination} bgmap - The background map where we want to draw.   
     * @private
     */
    _drawBorders(mvc, tg, bgmap) {
        this.initialDrawSetup(mvc, bgmap, true);
        this.setPolygonsBorders(tg);
    }

    /**
     * The method to be called to draw the picking data after a click on the background map.
     * @param {MapVariableCombination} mvc - The MapVariableCombination to use.
     * @param {TemporalGranule} tg - The temporal granule to be used.
     * @param {BGMapWrapper} bgmap - The background map where the click happened.
     * @param {Array<number>} textureViewPort - The viewport of the texture, where we want to draw the picking data. 
     * @memberof GisplayMap
     */
    drawPicking(mvc, tg, bgmap, textureViewPort) {
        // this.drawPolygonsToTexture(mvc, tg, bgmap, textureViewPort);
        this.initialPickingSetup(mvc, bgmap, textureViewPort);

        this.createImageTexture(GisplayDefaults.TEXTURE());
        this.setTextureIndex(mvc, false);
        this.setTextureSize();

        this.setPickingColorsAndPolygons(tg);
    }

    /**
     * Returns the color scheme and number of classes associated with the id given.
     * @returns {{colorScheme: string[], numberOfClasses: number}} - Color scheme and number of classes associated with the id given, empty object otherwise.
     * @override 
     * @memberof Choropleth
     */
    defaults() {
        let numberOfClasses = 4;
        return {
            color: {
                numberOfClasses: numberOfClasses,
                colors: this.getDefaultColors(numberOfClasses)
            }
        };
    }

    /**
     * Returns the avialable time controls for the thematic map.
     * @returns {Array<string>}
     * @memberof GisplayMap
     */
    getAvailableTemporalControls() {
        return [GisplayDefaults.INSTANT(), GisplayDefaults.ANIMATION()];
    }

    /**
     * Returns the name of the vertex and fragment shader files.
     * @returns {{vertexShaderFileName:string, fragmentShaderFileName:string}} - the name of the vertex and fragment shader files.
     * @override 
     * @memberof Choropleth
     */
    getShadersFileNames() {
        return { vertexShaderFileName: 'polygons.vert', fragmentShaderFileName: 'polygons.frag' };
    }

    /**
     * Returns the list of Visual variables that this map allows.
     * @returns {Array<string>} - the list of Visual variables that this map allows.
     * @override
     * @memberof Choropleth
     */
    getAvailableVisualVariables() {
        return [GisplayDefaults.COLOR(), GisplayDefaults.TEXTURE()];
    }

    /**
     * Returns the colors for this map given the number of classes and the nature of the data (sequential,  diverging or qualitative). 
     * @param {number} numClasses - Number of classes. 
     * @param {string} dataNature - Nature of the data.
     * @returns {Array<Array<RGB>>} Default colors for the map given the number of classes and nature of data.
     * @override 
     * @memberof Choropleth
     */
    getDefaultColors(numClasses, dataNature) {
        console.warn(numClasses, dataNature);
        return ColorBrewer.getDefautls('Choropleth', numClasses, dataNature || GisplayDefaults.SEQUENTIAL());
    }
}