import { GisplayDefaults } from '../../GisplayDefaults';

const earcut = require('../../../lib/earcut');
// import { earcut } from '../../../lib/earcut';
// import earcut from 'script-loader!../../../lib/earcut';

/**
 * GeoJSONIds Parser.
 * @export
 * @class GeoJSONIdsParser
 */
export class GeoJSONIdsParser {

    /**
     * Creates an instance of GeoJSONIdsParser.
     * @param {Blob} geoSpatialURL - The file to read and process. 
     * @param {string} geoSpatialId - The id of the geometry on the geospatial file.
     * @param {WebGLRenderingContext} webgl - The WebGLRenderingContext.
     * @memberof GeoJSONIdsParser
     */
    constructor(geoSpatialURL, geoSpatialId, webgl) {
        /**
         * The id of the geometry on the geospatial file.
         * @type {string}
         */
        this.geoSpatialId = geoSpatialId;
        /**
         * The WebGL Rendering context.
         * @type {WebGLRenderingContext}
         */
        this.webglContext = webgl;
        /**
         * The initial index for the polygon or line.
         * @type {number}
         */
        this.initialIndex = 0;
        /**
         * The color number to be used for each polygon/line/point that is read from the file.
         * @type {number}
         */
        this.RGBAColorNumber = 1;
        /** 
         * An array that holds all points for all polygons/lines. 
         * @type {Array<number>|WebGLBuffer}
         */
        this.allPoints = [];
        /** 
         * An array that holds all colors for all polygons/lines. One color for each vertex.
         * @type {Array<number>|WebGLBuffer}
         */
        this.allColors = [];

        /** 
         * The identifier of the type of primitive (Point, polygon or line).
         * @type {number}
         */
        this.geometryPrimitive;
        /**
         * The Filereader.
         * @type {FileReader}
         */
        this.reader = this.createFileReader();
        this.reader.readAsText(geoSpatialURL);
    }

    /**
     * Creates a FileReader object and adds the load and error events.
     * @returns {FileReader} - The Filereader object which will be used to read from the file.
     * @memberof GeoJSONIdsParser
     */
    createFileReader() {
        let reader = new FileReader();
        reader.onloadend = (e) => { this.processFile(e.target.result); };
        reader.onerror = (e) => { console.error(e); };
        return reader;
    }

    /**
     * Process the GeoJSON file.
     * @param {Object} fileData - The GeoJSON data that was read from the file. 
     * @memberof GeoJSONIdsParser
     */
    processFile(fileData) {
        let geojson = JSON.parse(fileData);
        console.log(geojson);
        console.time("geojsonIdsParser");
        let geometryIdsMap = new Map();
        for (let feature of geojson.features)
            geometryIdsMap.set(feature.properties[this.geoSpatialId], this.processFeature(feature));
        // this.endParser(geometryIdsMap);

        if (GisplayDefaults.hasPolygons(this.geometryPrimitive) || GisplayDefaults.hasLines(this.geometryPrimitive)) { //POLYGON OR LINES
            this.joinAllPoints();
            this.joinAllColors();
        }

        console.log(geometryIdsMap);
        this.dispatchIdGeometryEndEvent(geometryIdsMap);
        console.timeEnd("geojsonIdsParser");
    }

    /**
     * Process point or polygon.
     * @param {Object} feature - The feature to process.
     * @returns {Polygon|Array<number>} - The processed polygon or the point coordinates.
     * @memberof GeoJSONIdsParser
     */
    processFeature(feature) {
        if (this.geometryPrimitive === undefined)
            this._setGeometryPrimitive(feature.geometry.type);

        if (feature.geometry.type === 'Polygon' || feature.geometry.type === 'MultiPolygon') {
            return this.processPolygon(feature);
        } else if (feature.geometry.type === 'Point') {
            return this.processPoint(feature);
        } else if (feature.geometry.type === 'LineString' || feature.geometry.type === 'MultiLineString') {
            return this.processLine(feature);
        }
        else
            throw new Error("GeoJSON feature is not a Polygon, a Point or a Line");
    }

    /**
     * Set the type of geometry primitive for the data read. Only need to be called once. 
     * @param {string} geometryType - The type of geometry. 
     * @memberof GeoJSONParser
     */
    _setGeometryPrimitive(geometryType) {
        switch (geometryType) {
            case 'Point':
                this.geometryPrimitive = GisplayDefaults.getPrimitive().POINT;
                break;
            case 'Polygon':
            case 'MultiPolygon':
                this.geometryPrimitive = GisplayDefaults.getPrimitive().POLYGON;
                break;
            case 'LineString':
            case 'MultiLineString':
                this.geometryPrimitive = GisplayDefaults.getPrimitive().LINE;
                break;
        }
    }

    /**
     * Returns the point and it's RGBA color.
     * @param {{geometry: JSON, properties: JSON}} point - The geometry and properties of the point.
     * @returns {{spatial: {lineIndices:Array<number>}, RGBA:Array<number>}} - The point coordinates and the RGBA that identifies it.
     * @memberof GeoJSONParser
     */
    processPoint(point) {
        let RGBA = GisplayDefaults.numberToRGBA(this.RGBAColorNumber++);
        return { spatial: point.geometry.coordinates, RGBA };
    }

    /**
     * Add the polygon points to the array of all polygon points and saves the polygon borders and triangulation indices.
     * The triangles and vertices calculated by earcut triangulation.
     * @param {{geometry: JSON, properties: JSON}} polygon - The geometry and properties of the polygon.
     * @returns {Array<{triangles: Array<number>, vertices: Array<number>}>} - An array of Polygons. Each polygon has it's vertices (lng,lat) and triangles (which vertices to connect to create triangles).
     * @see http://www.macwright.org/2015/03/23/geojson-second-bite.html#polygons
     * @see https://github.com/mapbox/earcut/
     * @memberOf GeoJSONParser
     */
    processPolygon(polygon) {
        let polygonIndices = [], //Vertices given by earcut
            bordersIndices = []; //Save all triangles for the processed polygon or multipolygon
        let RGBA = GisplayDefaults.numberToRGBA(this.RGBAColorNumber++);
        if (polygon.geometry.type === "Polygon") { //@TODO: [Demos never use this if statement.]

            let outsidepolygon = polygon.geometry.coordinates[0]; //See: http://geojson.org/geojson-spec.html#polygon
            let tempVerts = new Array();

            for (let out = 0; out < outsidepolygon.length - 1; out++) {
                tempVerts.push(outsidepolygon[out][0], outsidepolygon[out][1]);
                this.allPoints.push(outsidepolygon[out][0], outsidepolygon[out][1]);
                this.allColors.push(...RGBA);
            }

            let earcutVertices = earcut(tempVerts);
            let finalVertices = earcutVertices.map((e) => e + this.initialIndex);
            for (let finalVertex of finalVertices)
                polygonIndices.push(finalVertex);

            this._createBorders(tempVerts.length / 2, bordersIndices);
            this.initialIndex += tempVerts.length / 2;
        }
        else if (polygon.geometry.type == "MultiPolygon") { //See http://geojson.org/geojson-spec.html#multipolygon
            for (const cs of polygon.geometry.coordinates) { //For each polygon
                let tempVerts = new Array();

                let outsidepolygon = cs[0];
                for (const c of outsidepolygon) {
                    tempVerts.push(c[0], c[1]);
                    this.allPoints.push(c[0], c[1]);
                    this.allColors.push(...RGBA);
                }

                let earcutVertices = earcut(tempVerts);
                let finalVertices = earcutVertices.map((e) => e + this.initialIndex);
                for (let finalVertex of finalVertices)
                    polygonIndices.push(finalVertex);

                this._createBorders(tempVerts.length / 2, bordersIndices);
                this.initialIndex += (tempVerts.length / 2); //Update the first index for the next polygon
            }
        }
        return { spatial: { polygonIndices, bordersIndices }, RGBA };
    }

    /**
     * Create the border indices for the polygon starting at it's initial index.
     * @param {number} numVertices - The number of vertices of the polygon. 
     * @param {Array<number>} bordersIndices - The array of indices. If it's a multipolygon it might contain some indices otherwise it's empty.
     * @memberof GeoJSONParser
     */
    _createBorders(numVertices, bordersIndices) {
        //Loop tempVerts and look create indices for this 
        bordersIndices.push(this.initialIndex);
        let currentIndex = this.initialIndex;
        for (let i = 1; i < numVertices; i++) {
            currentIndex++;
            bordersIndices.push(currentIndex, currentIndex);
        }
        bordersIndices.push(this.initialIndex);
    }

    /**
     * Process the line (LineString or MultiLineString).
     * @param {Object} feature - The GeoJSON Feature. 
     * @returns {Array<number>|Array<Array<number>>} - the line (array of points) or a group of lines (array of arrays of points).
     * @see http://wiki.geojson.org/GeoJSON_draft_version_6#MultiLineString
     * @memberof GeoJSONParser
     */
    processLine(feature) {
        let lineIndices = [];
        let RGBA = GisplayDefaults.numberToRGBA(this.RGBAColorNumber++);
        if (feature.geometry.type === 'LineString') {
            /**
                 {
                    "type": "LineString",
                    "coordinates": [
                        [100.0, 0.0], [101.0, 1.0]
                    ]
                }
            */

            let lngLatPoints = [];
            for (const pt of feature.geometry.coordinates) {
                lngLatPoints.push(pt[0], pt[1]); //Push lng,lat
                this.allPoints.push(pt[0], pt[1]);
                this.allColors.push(...RGBA);
            }
            this._createLines(lngLatPoints.length / 2, lineIndices);
            this.initialIndex += (lngLatPoints.length / 2); //Update the first index for the next line
        }
        else {
            /**
                {
                    "type": "MultiLineString",
                    "coordinates": [
                        [ [100.0, 0.0], [101.0, 1.0] ],
                        [ [102.0, 2.0], [103.0, 3.0] ]
                    ]
                }
             */
            for (const line of feature.geometry.coordinates) { //Loop over each line inside multiLineStr
                let lngLatPoints = [];
                for (const pt of line) { //Loop over each point of this line
                    lngLatPoints.push(pt[0], pt[1]); //Push lng/lat
                    this.allPoints.push(pt[0], pt[1]);
                    this.allColors.push(...RGBA);
                }
                // points.push(linePoints);// [[100, 0, 101, 1], [102, 2, 103, 3]]
                this._createLines(lngLatPoints.length / 2, lineIndices);
                this.initialIndex += (lngLatPoints.length / 2); //Update the first index for the next line
            }
        }
        // return lineIndices;//this.bindLineToWebGLContext(points);
        return { spatial: { lineIndices }, RGBA };//this.bindLineToWebGLContext(points);
    }

    /**
     * Create line segments between each two indices.
     * @param {number} numIndices - The number of indices to add.
     * @param {Array<number>} lineIndices - The indices for the line (If it's a multilinestring may already contain indices).
     * @memberof GeoJSONIdsParser
     */
    _createLines(numIndices, lineIndices) {
        lineIndices.push(this.initialIndex);
        let currentIndex = this.initialIndex;
        for (let i = 1; i < numIndices - 1; i++) {
            currentIndex++;
            lineIndices.push(currentIndex, currentIndex);
        }
        lineIndices.push(++currentIndex);
    }

    /**
     * Join all points of all polygons/lines into a single WebGL buffer.
     * @memberof Parser
     */
    joinAllPoints() {
        let webglContext = this.webglContext;
        const vertArray = new Float32Array(this.allPoints);
        const bufferP = webglContext.createBuffer();
        webglContext.bindBuffer(webglContext.ARRAY_BUFFER, bufferP);
        webglContext.bufferData(webglContext.ARRAY_BUFFER, vertArray, webglContext.STATIC_DRAW);
        this.allPoints = bufferP;
    }

    /**
     * Join all points of all polygons/lines into a single WebGL buffer.
     * @memberof Parser
     */
    joinAllColors() {
        let webglContext = this.webglContext;
        const vertArray = new Uint8Array(this.allColors);
        const bufferP = webglContext.createBuffer();
        webglContext.bindBuffer(webglContext.ARRAY_BUFFER, bufferP);
        webglContext.bufferData(webglContext.ARRAY_BUFFER, vertArray, webglContext.STATIC_DRAW);
        this.allColors = bufferP;
    }

    /**
     * Dispatch the event for the geometry ids.
     * @param {Map<string, Polygon|Array<number>>} geometryIdsMap - The map with ids and respective geometry. 
     * @memberof Parser
     */
    dispatchIdGeometryEndEvent(geometryIdsMap) {
        document.dispatchEvent(new CustomEvent("geometryIdsEnd", {  //Retornar pelo menos os dados processados e as classes 
            detail: {
                geometryIdsMap: geometryIdsMap,
                allPoints: this.allPoints,
                allColors: this.allColors,
                geometryPrimitive: this.geometryPrimitive
            }
        }));
    }
}