//Import for Intellisense
import { DataVariable } from '../../VVs/DataVariable';

let CSVDataWorker = require("worker-loader?name=CSVDataWorker.js!./CSVDataWorker.js");
import { Parser } from '../Parser';
import { GeoJSONIdsParser } from '../GeoJSON/GeoJSONIdsParser';
import { GisplayDefaults } from '../../GisplayDefaults';

/**
 * Fast CSV Parser. This parser was created because current parsers that work on the browser do not provide the required performance for this thesis.
 * @export
 * @class NewCSVParser
 * @extends {Parser}
 */
export class CSVParser extends Parser {
    /**
     * 
     * @param {GisplayOptions} gisplayOptions - The options class.
     * @param {WebGLRenderingContext} webgl - The WebGLRenderingContext.
     * @memberof NewCSVParser
     */
    constructor(gisplayOptions, webgl) {
        super(gisplayOptions, webgl);
        console.time("csvParser");

        /**
         * The start byte of the file.
         * @type {number=0}
         */
        this.startByte = 0;
        /**
         * Number of bytes to read at the beginning of the file.
         * @type {number=1024}
         */
        this.headerBytesSize = 1024;

        //Load Programmer Options
        let csvOpts = this.opts.getCSV();
        /**
         * The size of each chunk to be read from the file. The default is 10MiB. 
         * @type {number=1024*1024*10}
         */
        this.chunkSize = csvOpts.chunkSize || 1024 * 1024 * 10;
        /**
         * The char or string that is used to split columns of each row. Defaults is comma.
         * @type {string=','}
         */
        this.columnDelimiter = ',';
        /**
         * The row delimiter. Defaults is \n.
         * CR = \r (MAC); LF = \n (LINUX); CRLF = \r\n (WINDOWS) 
         * @see https://en.wikipedia.org/wiki/Newline
         * @see http://stackoverflow.com/a/1552782/
         * @see https://stackoverflow.com/a/39259747/
         * @type {string='\n'}
         */
        this.rowDelimiter = '\n';

        let numWorkers = Math.max(Math.floor((this.fileSize / this.chunkSize)), 1);
        numWorkers = numWorkers > 4 ? 4 : numWorkers;
        // numWorkers = 4;
        /**
         * Number of data workers that will process the file in parallel.
         * @type {number=4}
         */
        this.numDataWorkers = csvOpts.numWorkers || numWorkers;
        /**
         * The data workers that will read a portion of the CSV dataset.
         * @type {Array<Worker>}
         */
        this.dataWorkers = new Array(this.numDataWorkers);
        /**
         * Information to be stored for each worker.
         * @type {Array<any>}
         */
        this.dataWorkersInfo = new Array(this.numDataWorkers);

        //REPLIES
        /**
         * The number of worker that replied with the terminated message.
         * @type {number}
         */
        this.workerTerminatedReplies = 0;
        /**
         * Used to save the first and last rows of each worker after they're done processing their part. 
         * @type {Array<Array<string>>}
         */
        this.workerRemainingRows = [];//Array(this.numDataWorkers).fill([]);

        if (this.urlVariable.hasIds())
            new GeoJSONIdsParser(this.urlVariable.getGeoSpatialURL(), this.urlVariable.getIdOnGeoSpatialURL(), this.webglContext);

        /**
         * The Filereader.
         * @type {FileReader}
         */
        this.reader = this.createFileReader();
        this.readHeader();
    }

    /**
     * Code that will be used in the future to decide how many workers to start and the amount of bytes each will have to deal with.
     * @memberof CSVNewParser
     */
    unusedConstructorCode() {
        //Decide num data workers based on file size.
        if (this.numDataWorkers * this.chunkSize > this.fileSize) {
            for (let i = 1, found = false; i < this.numDataWorkers && !found; i++) {//Update numDataworkers
                if (this.chunkSize * i > this.fileSize) {
                    this.numDataWorkers = i;
                    found = true;
                }
            }
        }

        let chunksByWorker = Math.ceil((this.fileSize / this.numDataWorkers) / this.chunkSize);
        let totalChunks = Math.ceil(chunksByWorker * this.numDataWorkers);
        this.numChunks = totalChunks > 0 ? totalChunks : 1; //NOT USED ATM
    }

    /**
     * Creates a FileReader object and adds the load and error events to it.
     * @returns {FileReader} - The Filereader object which will be used to read from the file.
     * @memberof GeoJSONParser
     */
    createFileReader() {
        let reader = new FileReader();
        reader.onload = (e) => { this.chunkLoaded(e.target.result); };
        reader.onerror = (e) => { console.error(e); };
        return reader;
    }

    /**
     * Reads the first 1024 bytes (1KiB) (the value of this.headerBytesSize) of the file.
     * @memberof NewCSVParser
     */
    readHeader() {
        let start = Math.min(this.startByte, this.fileSize);
        let end = Math.min(this.startByte + this.headerBytesSize, this.fileSize - 1);
        this.reader.readAsText(this.dataFile.slice(start, end));
    }

    /**
     * After the header chunk was loaded then look for 
     * @param {string} textRead - The string of text that was read from the file.  
     * @memberof NewCSVParser
     */
    chunkLoaded(textRead) {
        //To detect the rowDelimiter we can calculte the number of \r, \n and \r\n
        let rowDelimiterDetected = this.detectRowDelimiter(textRead);
        if (!rowDelimiterDetected)
            this.readHeader();
        else {
            if (this.detectColumnDelimiter(textRead))
                this.processHeader(textRead);
        }
    }

    /**
     * Detects which is the row delimiter for the file in use.
     * @param {string} textRead - The string of text that was read from the file.  
     * @return {boolean} true, if the row delimiter was detected, false, otherwise.
     * @memberof NewCSVParser
     */
    detectRowDelimiter(textRead) {
        let R = textRead.split('\r').length;
        let N = textRead.split('\n').length;
        if (R === N)
            this.rowDelimiter = '\r\n';
        else if (R > N)
            this.rowDelimiter = '\r';
        else
            this.rowDelimiter = '\n';

        // logRed("R", R, "N", N);
        if (R > 1 && N > 1 && R !== N) //
            throw new Error(`Dataset row delimiter is inconsistent! There are more than 1 row delimited by \\r and more than one row delimited by \\n. 
            Inspect your file with  HxD (https://mh-nexus.de/en/hxd/) and resolve inconsistencies.`);

        let finalText = textRead.split(this.rowDelimiter);
        if (finalText.length < 2) {
            this.headerBytesSize *= 2;
            console.warn('Not enought bytes read from the file. Attempting to double the number of bytes and trying again...');
            return false;
        }
        else
            return true;
    }

    /**
     * Detects the column separator for the file in use.
     * @param {string} textRead - The string of text that was read from the file.  
     * @see https://data-gov.tw.rpi.edu/wiki/CSV_files_use_delimiters_other_than_commas
     * @memberof NewCSVParser
     */
    detectColumnDelimiter(textRead) {
        let headerText = textRead.split(this.rowDelimiter)[0];
        let numCommas = headerText.split(',').length - 1;
        let numSemiColons = headerText.split(';').length - 1;
        let numPipes = headerText.split('|').length - 1;
        if (numCommas > 0 && numSemiColons === 0 && numPipes === 0)
            this.columnDelimiter = ',';
        else if (numCommas === 0 && numSemiColons > 0 && numPipes === 0)
            this.columnDelimiter = ';';
        else if (numCommas === 0 && numSemiColons === 0 && numPipes > 0)
            this.columnDelimiter = '|';
        else
            throw new Error(`Couldn't detect the column delimiter. Something must be wrong with the dataset. Header text: ${headerText}`);
        return true;
    }

    /**
     * Process header using the programmer options and the header read from the file.
     * @param {string} textRead - The string of text that was read from the file.   
     * @memberof NewCSVParser
     */
    processHeader(textRead) {
        let header = textRead.split(this.rowDelimiter)[0];
        let columns = header.split(this.columnDelimiter);

        let numColumns = columns.length; //To be used by Workers?
        let headerIndices = [];
        let catIndices = [];
        let contIndices = [];
        let timeIndex = [];
        let optionalIndices = [];
        let geoIndices = [];

        catIndices = this.findVariablesIndices(this.categoricalVariables, columns);
        contIndices = this.findVariablesIndices(this.continousVariables, columns);
        timeIndex = this.hasTime ? columns.indexOf(this.timeVariable.getExternalName()) : -1;
        optionalIndices = this.findVariablesIndices(this.optionalVariables, columns);
        geoIndices = this.findGeometryIndices(columns);

        // console.log(catIndices, contIndices, timeIndex, optionalIndices, numColumns);
        this.verifyIndicesIntegrity(catIndices, contIndices, timeIndex, optionalIndices);
        headerIndices = [catIndices, contIndices, geoIndices, timeIndex, optionalIndices];
        this.startWorkers(headerIndices, numColumns);
    }

    /**
     * For each variable of the given variables find the index of it's internal name on the header.
     * @param {Array<DataVariable>} variables - The variables to look for it's indices.
     * @param {Array<string>} headerColumns - The header columns
     * @returns {Array<number>} - the index of each variable internal name on the header.
     * @memberof NewCSVParser
     */
    findVariablesIndices(variables, headerColumns) {
        let indices = [];
        for (const variable of variables) {
            let index = headerColumns.indexOf(variable.getExternalName());
            if (index > -1)
                indices.push(index);
        }
        return indices;
    }

    /**
     * For the CSV file finds the indices of the geometry. If the geometry should be reused then it should return the index of the identifier.
     * @param {Array<string>} columns - The header keys. 
     * @returns {number|Array<number>} - The id or the longitude and latitude indices.
     * @memberof NewCSVParser
     */
    findGeometryIndices(columns) {
        let id = this.urlVariable.getIdOnDataURL();
        // console.warn(id);
        if (id)
            return columns.indexOf(id);
        else {
            let longitudeExternalName = this.geometryVariables[0].getExternalName();
            let latitudeExternalName = this.geometryVariables[1].getExternalName();
            let lngIndex = columns.indexOf(longitudeExternalName);
            let latIndex = columns.indexOf(latitudeExternalName);
            if (latIndex === -1 || lngIndex === -1)
                throw new Error("Longitude or Latitude keys not found in the CSV header.");
            return [lngIndex, latIndex];
        }
    }

    /**
     * Verifies if there's any error in any given variable external name.
     * @param {Array<number>} catIndices - The indices of all categorical variables.
     * @param {Array<number>} contIndices - The indices of all continuous variables.
     * @param {number} timeIndex - The index of the time variable.
     * @param {Array<number>} optionalIndices - The indices of the optional variables.
     * @throws {Error} - At least one variable external name wasn't found in the CSV file header. 
     * @memberof NewCSVParser
     */
    verifyIndicesIntegrity(catIndices, contIndices, timeIndex, optionalIndices) {
        let errCt = 0;
        if (this.categoricalVariables.length !== catIndices.length)
            errCt++;
        if (this.continousVariables.length !== contIndices.length)
            errCt++;
        if (this.hasTime && timeIndex === -1)
            errCt++;
        if (this.optionalVariables.length !== optionalIndices.length)
            errCt++;

        if (errCt > 0)
            throw new Error(`At least ${errCt} variable(s) external names weren't found in the CSV file header.`);
    }

    /**
     * Creates the workers that will parse the file in chunks.
     * @param {Array<Array<number>>} headerIndices - The indices of the optional variables, if any.
     * @param {number} numColumnsPerRow - The number of columns each row must contain.
     * @see https://stackoverflow.com/a/44782052/
     * @memberof NewCSVParser
     */
    startWorkers(headerIndices, numColumnsPerRow) {
        let CSVWorkerOptions = {
            dataFile: this.dataFile,
            chunkSize: Math.max(this.headerBytesSize, this.chunkSize), //UserDef or 10MB ?
            columnDelimiter: this.columnDelimiter, //UserDef or comma
            rowDelimiter: this.rowDelimiter, //UserDef or \n 
            numColumnsPerRow: numColumnsPerRow, //Used to make sure every line has the same number of 
            workerIndex: -1, //worker index
            startByte: 0, //Start Byte for worker
            endByte: 0, //End byte for worker
            headerIndices: headerIndices, //The header indices

            categoricalVariables: this._cloneCategoricalVariables(), //this.categoricalVariables,
            continuousVariables: this.continousVariables,
            timeVariable: this.timeVariable,
            // hasIds: this.urlVariable.hasIds(),

            optionalVariables: this.optionalVariables,
            // webglContext: this.webglContext,
            classCalcRequired: this.classCalculationRequired(),
            timeGranularity: headerIndices[3] >= 0 ? this.timeVariable.getGranularity() : ''
        };

        let numDataWorkers = this.numDataWorkers;
        let bytesByDataWorker = Math.round(this.fileSize / numDataWorkers);
        //Loop by this.numDataWorkers e start each with some information
        for (let i = 0; i < this.numDataWorkers; i++) {
            CSVWorkerOptions.workerIndex = i;
            CSVWorkerOptions.startByte = i * bytesByDataWorker;
            CSVWorkerOptions.endByte = i < numDataWorkers - 1 ? (i + 1) * bytesByDataWorker : this.fileSize;
            this.dataWorkers[i] = new CSVDataWorker();

            console.log(CSVWorkerOptions);

            this.dataWorkers[i].postMessage({
                protocolMessage: GisplayDefaults.MESSAGES_CSV().TO_START,
                messageData: {
                    workerOptions: CSVWorkerOptions
                }
            });
            this.dataWorkers[i].onmessage = (e) => { this.receiveWorkersMessage(e.data); };
        }
    }

    /**
     * Clone the categorical variable to remove the gisplayMap object because it can't be cloned to send to Web Workers.
     * @returns 
     * @memberof CSVNewParser
     */
    _cloneCategoricalVariables() {
        let catVarsToSend = [];//new Array(this.categoricalVariables.length);
        for (const catVar of this.categoricalVariables) {
            catVarsToSend.push(Object.assign(Object.create(Object.getPrototypeOf(catVar)), catVar));
        }
        for (let catVar of catVarsToSend)
            catVar.gisplayMap = null;
        console.log(catVarsToSend);
        return catVarsToSend;
    }

    /**
     * Receives the message that was sent by any Worker.
     * @param {{protocolMessage: string, messageData:Object}} message - The message that was sent by the worker.
     * @memberof CSVNewParser
     */
    receiveWorkersMessage(message) {
        let protocolMessages = GisplayDefaults.MESSAGES_CSV();
        switch (message.protocolMessage) {
            case protocolMessages.END_START: { //Worker processed it's part
                if (++this.workerTerminatedReplies === this.numDataWorkers) {
                    this.workerTerminatedReplies = 0;
                    this.sendMessageToWorkers(protocolMessages.TO_LIMITS_CAT);
                }
                break;
            }
            case protocolMessages.END_LIMITS_CAT: {
                //Save the limits/categories and time granules sent by the W
                let catVarsCategories = message.messageData.categoricalVariables;
                for (const catVar of this.categoricalVariables)
                    if (catVar.classCalculationRequired())
                        catVar.addCategories(catVarsCategories.get(catVar.getInternalName()));

                let continousVariables = message.messageData.continousVariables;
                for (const contVar of this.continousVariables) {
                    let minMax = continousVariables.get(contVar.getInternalName());
                    contVar.setMinMax(minMax[0]);
                    contVar.setMinMax(minMax[1]);
                    contVar.setStep();
                }

                if (this.hasTime)
                    this.timeVariable.addTemporalGranulesToSet(message.messageData.timeVariable);

                if (++this.workerTerminatedReplies === this.numDataWorkers) {
                    //Now we have the TGs and Categories but still need to calculate the histograms for each continuous variable
                    //Send min/max of each Cont var to the Ws
                    /*   console.log("ALL REPLIED");
                      console.log('---------------------------------------');
                      console.log(this.categoricalVariables);
                      console.log(this.continousVariables);
                      console.log(this.timeVariable);
                    */
                    this.workerTerminatedReplies = 0;
                    let continuousVarsMinMax = [];
                    for (const contVar of this.continousVariables)
                        continuousVarsMinMax.push([contVar.getMin(), contVar.getMax()]);
                    this.sendMessageToWorkers(protocolMessages.TO_LIMITS_CAT_RES, continuousVarsMinMax);
                }
                break;
            }
            case protocolMessages.END_LIMITS_CAT_RES: {
                let histograms = message.messageData.histograms;
                for (let [i, contVar] of this.continousVariables.entries())
                    if (contVar.classCalculationRequired())
                        contVar.updateHistogram(histograms[i]);

                if (++this.workerTerminatedReplies === this.numDataWorkers) {
                    this.workerTerminatedReplies = 0;

                    for (let contVar of this.continousVariables)
                        contVar.createBins();

                    /*      console.log("ALL REPLIED SEE CONT VARS");
                         console.log('---------------------------------------');
                         console.log(this.continousVariables); */

                    // Generate MVCs and TGs because we already have everything we need to do so
                    //Each Cat Var generate Map of categories to index
                    //Each cont var generate classes
                    //Time Var generate indices for each granule
                    for (const catVar of this.categoricalVariables) {
                        if (catVar.classCalculationRequired())
                            catVar.updateCategoricalInformation();
                    }
                    for (const contVar of this.continousVariables)
                        if (contVar.classCalculationRequired())
                            contVar.calculateClasses();

                    if (this.hasTime) {
                        this.timeVariable.createTemporalGranulesMap(this.timeVariable.getTemporalGranulesSet());
                        this.timeVariable.sortTemporalGranules();
                    }

                    this.createMapVariableCombinations(); //@TODO: SHouldn't be called if not needed

                    //Send combinations created to the Ws
                    this.sendTO_JOIN_DATAMessage();
                }
                break;
            }
            case protocolMessages.END_JOIN_DATA: {
                //Should receive the information about each W
                //Number of elements per MVC/TG
                this.dataWorkersInfo[message.messageData.workerIndex] = message.messageData.numElemsPerTG;
                if (++this.workerTerminatedReplies === this.numDataWorkers) {
                    this.workerTerminatedReplies = 0;
                    console.log("All Ws joined their data");
                    console.log(this.dataWorkersInfo);
                    //Generate start and end RGBA for each W MVC/TG 
                    let rgbasByWorker = this._generateStartRGBAsForWorkers();   //Should send to each worker: 1. the start RGBA of each TG for each MVC 
                    this.sendMessageToWorkers(protocolMessages.TO_GENERATE_RGBA, rgbasByWorker);
                }
                break;
            }
            case protocolMessages.END_GENERATE_RGBA: {
                if (++this.workerTerminatedReplies === this.numDataWorkers) {
                    this.workerTerminatedReplies = 0;
                    console.log("All Ws Generated RGBA Values");
                    //All replied then we can start receiving data to build the final MVCs and TGs
                    this.sendMessageToWorkers(protocolMessages.TO_SEND_DATA);
                }
                break;
            }
            case protocolMessages.END_SEND_DATA: {
                let finalDataRows = message.messageData.finalDataRows;
                //Put each row into its MVC/TG ...
                // console.log(this.workerTerminatedReplies, finalDataRows);
                // this.categoricalVariables[0].values = ['N', 'Y']; /// HERE FIND SOLUTION FOR THIS <<<<<<<<<<<<<<COMMENTED 21/01/2018
                // console.warn(this.mapVarCombinations.length);
                console.warn(finalDataRows);
                // console.error("Worker==", this.workerTerminatedReplies + 1);
                for (let i = 0; i < finalDataRows.length; i++) { //MVCs
                    for (let j = 0; j < finalDataRows[i].length; j++) { //TGs
                        if (finalDataRows[i][j].RGBA.length > 0) {
                            // if (i === 1 && j === 0 && this.workerTerminatedReplies + 1 === 2
                            //     || i === 1 && j === 0 && this.workerTerminatedReplies + 1 === 3) {
                            // }
                            /*  console.warn(i, j, this.workerTerminatedReplies + 1);
                             console.log(this.printFirstRGBA(finalDataRows[i][j].RGBA), this.printLastRGBA(finalDataRows[i][j].RGBA));
                             console.log(finalDataRows[i][j].RGBA);
                             console.log(finalDataRows[i][j].numberRGBA); */
                            this.mapVarCombinations[i].temporalGranules[j].addSetOfRows(finalDataRows[i][j]);
                        }
                    }
                }

                for (const mvc of this.mapVarCombinations) //Update the current RGBA for each Map var combination
                    mvc.updateCurrentRGBA();

                // console.error("Worker==", this.workerTerminatedReplies + 1);
                // console.error("-------------------------------------------------------------------");
                if (++this.workerTerminatedReplies === this.numDataWorkers) {
                    //DONE
                    this.terminateDataWorkers();
                    console.log("ALL Ws sent their data and are terminated");
                    this.geometryPrimitive = !this.urlVariable.hasIds() ? GisplayDefaults.getPrimitive().CSVPOINT : GisplayDefaults.getPrimitive().CSVIDS;
                    this.endParser();
                }
                else
                    this.sendMessageToWorkers(protocolMessages.TO_SEND_DATA);
            }
        }
    }

    printFirstRGBA(RGBA) {
        let firstRGBA = [];
        for (let i = 0; i < 4; i++)
            firstRGBA.push(RGBA[i])
        return firstRGBA;
    }

    printLastRGBA(RGBA) {
        let lastRGBA = [];
        for (let i = RGBA.length - 4; i < RGBA.length; i++)
            lastRGBA.push(RGBA[i]);
        return lastRGBA;
    }

    /**
     * Generate the start RGBA codes for all possible combinations of MVC/TG in ascending order by worker.
     * @returns {Array<Array<number>>} -  the start RGBA codes for all possible combinations of MVC/TG in ascending order by worker.
     * @memberof CSVNewParser
     */
    _generateStartRGBAsForWorkersOLD() {
        let rgbasByWorker = GisplayDefaults.cloneNestedArray(this.dataWorkersInfo);//.slice(0); //To send with the startRGBA for each MVC/TG
        for (let mvc of this.mapVarCombinationStrToIndexMap.keys()) {
            let mvcIndex = this.mapVarCombinationStrToIndexMap.get(mvc);
            let rgba = this.mapVarCombinations[mvcIndex].getStartingRGBA(),
                rgbaNum = GisplayDefaults.RGBAToNumber(...rgba);

            let first = true,
                numElems = 0,
                numTGs = this.mapVarCombinations[mvcIndex].getTemporalGranules().length;
            for (let tgIndex = 0; tgIndex < numTGs; tgIndex++) {
                for (let [workerIndex, worker] of this.dataWorkers.entries()) {
                    numElems += this.dataWorkersInfo[workerIndex][mvcIndex][tgIndex] + 1;
                    if (first) {
                        rgbasByWorker[workerIndex][mvcIndex][tgIndex] = rgba;
                        first = false;
                    } else {
                        let newRGBANum = GisplayDefaults.RGBAToNumber(...rgba) + numElems;
                        let newRGBA = GisplayDefaults.numberToRGBA(newRGBANum);
                        rgbasByWorker[workerIndex][mvcIndex][tgIndex] = newRGBA;
                    }
                }
            }
        }
        let numWorkers = this.dataWorkers.length;
        for (let i = 0; i < numWorkers; i++)
            console.log(rgbasByWorker[i]);
        return rgbasByWorker;
    }

    _generateStartRGBAsForWorkers() {
        let rgbasByWorker = GisplayDefaults.cloneNestedArray(this.dataWorkersInfo);//.slice(0); //To send with the startRGBA for each MVC/TG
        for (let mvc of this.mapVarCombinationStrToIndexMap.keys()) {
            let mvcIndex = this.mapVarCombinationStrToIndexMap.get(mvc);
            let rgba = this.mapVarCombinations[mvcIndex].getStartingRGBA();

            let index = 0,
                numElems = GisplayDefaults.RGBAToNumber(...rgba),
                numTGs = this.mapVarCombinations[mvcIndex].getTemporalGranules().length;
            for (let [workerIndex, worker] of this.dataWorkers.entries()) {
                if (index++ === 0)
                    rgbasByWorker[workerIndex][mvcIndex] = rgba;
                else {
                    rgba = GisplayDefaults.numberToRGBA(numElems);
                    rgbasByWorker[workerIndex][mvcIndex] = rgba;
                }
                numElems += this.dataWorkersInfo[workerIndex][mvcIndex] + 1;
            }
        }
        let numWorkers = this.dataWorkers.length;
        for (let i = 0; i < numWorkers; i++)
            console.log(rgbasByWorker[i]);
        return rgbasByWorker;
    }

    /**
     * Send message with name: TO_JOIN, and this way the Ws can join the data given the MVCs and TGs created. 
     * @memberof CSVNewParser
     */
    sendTO_JOIN_DATAMessage() {
        let protocolMessages = GisplayDefaults.MESSAGES_CSV();
        let MVCs = this.mapVarCombinationStrToIndexMap;
        let sortedTGs = this.hasTime ? this.timeVariable.getSortedTemporalGranules() : [0];
        /*   console.log(MVCs);
          console.log(sortedTGs); */
        //Needs to send the cat vars, the cont vars and the time variable and the mvcs created
        this.sendMessageToWorkers(protocolMessages.TO_JOIN_DATA, {
            categoricalVariables: this._cloneCategoricalVariables(),//this.categoricalVariables,
            continuousVariables: this.continousVariables,
            timeVariable: this.timeVariable,
            mvcs: MVCs,
            numberMVCs: MVCs.size,
            numberTGs: sortedTGs.length
        });
    }

    sendMessageToWorkers(message, dataToSend) {
        let protocolMessages = GisplayDefaults.MESSAGES_CSV();
        switch (message) {
            /*  case protocolMessages.TO_REMAINING_ROWS1: {
                 this.dataWorkers[0].postMessage({
                     protocolMessage: message,
                     messageData: dataToSend
                 });
                 break;
             } */
            case protocolMessages.TO_LIMITS_CAT: {
                for (const dataWorker of this.dataWorkers)
                    dataWorker.postMessage({ protocolMessage: message });
                break;
            }
            case protocolMessages.TO_LIMITS_CAT_RES: {
                for (const dataWorker of this.dataWorkers)
                    dataWorker.postMessage({
                        protocolMessage: message,
                        messageData: dataToSend
                    });
                break;
            }
            case protocolMessages.TO_JOIN_DATA: {
                for (const dataWorker of this.dataWorkers)
                    dataWorker.postMessage({
                        protocolMessage: message,
                        messageData: dataToSend
                    });
                break;
            }
            case protocolMessages.TO_GENERATE_RGBA: {
                for (const [i, dataWorker] of this.dataWorkers.entries()) {
                    dataWorker.postMessage({
                        protocolMessage: message,
                        messageData: dataToSend[i] //Only the rgbas for this worker
                    });
                }
                break;
            }
            case protocolMessages.TO_SEND_DATA: {
                this.dataWorkers[this.workerTerminatedReplies].postMessage({
                    protocolMessage: message,
                    messageData: dataToSend
                });
                break;
            }
        }
    }

    /**
     * Send terminate event to all data workers after their job is done.
     * @memberof NewCSVParser
     */
    terminateDataWorkers() {
        for (const dataWorker of this.dataWorkers)
            dataWorker.terminate();
        delete this.dataWorkers;
    }

    /**
     * Split Transitory data, join points if needed and dispatch the parser end event.
     * @memberof Parser
     */
    endParser() {
        if (!this.urlVariable.hasIds()) {
            this.setGeometryPrimitiveTGs();
            this.setNumElementsTemporalGranules();

            console.time("joinPoints");
            if (this.geometryVariables.length > 1) {
                this.joinMapVarCombinationData();
                this.joinPickingColors();
            }
            console.timeEnd("joinPoints");
        }
        this.dispatchParseEndEvent();
    }
}