//Import for Intellisense
import { URLVariable } from '../VVs/URLVariable';
import { DataVariable } from '../VVs/DataVariable';
import { TimeVariable } from '../VVs/TimeVariable';
import { CategoricalVariable } from '../VVs/CategoricalVariable';
import { ContinuousVariable } from '../VVs/ContinuousVariable';

import { MapVariableCombination } from '../VVs/MapVariableCombination';
import { GisplayDefaults } from '../GisplayDefaults';
import { GisplayOptions } from '../GisplayOptions';
import { GisplayError } from '../GisplayError';

/**
 * General parser class.
 * @class Parser
 */
export class Parser {

    /**
     * Creates an instance of Parser.
     * @param {GisplayOptions} gisplayOptions - The options class.
     * @param {WebGLRenderingContext} webgl - The WebGLRenderingContext.
     * @memberof Parser
     */
    constructor(gisplayOptions, webgl) {
        /**
         * All the options for the Gisplay AAPI.
         * @type {GisplayOptions}
         */
        this.opts = gisplayOptions;
        /**
         * All the categorical variables that were created with the given options.
         * @type {Array<CategoricalVariable>}
         */
        this.categoricalVariables = this.opts.getCategoricalVariables();
        /**
         * All the continuous variables that were created with the given options.
         * @type {Array<ContinuousVariable>}
         */
        this.continousVariables = this.opts.getContinousVariables();
        /**
         * The time variable or null if there's no Time in the dataset.
         * @type {TimeVariable}
         */
        this.timeVariable = this.opts.getTimeVariable();
        /**
         * An array with extra variables used mainly for picking information or an empty array when the programmer didn't give any extra variable.
         * @type Array<Variable>}
         */
        this.optionalVariables = this.opts.getOptionalVariables();
        /**
         * The URLVariable.
         * @type {URLVariable} 
         */
        this.urlVariable = this.opts.getURLVariable();
        /**
         * The geometry variables (longitude and latitude).
         * @type {Array<DataVariable>}
         */
        this.geometryVariables = this.opts.getGeometryVariables();

        let URLVariable = this.urlVariable;
        /**
         * The file with the data.
         * @type {File}
         */
        this.dataFile = URLVariable.getDataFile();
        /**
         * The size of the file in bytes.
         * @type {number}
         */
        this.fileSize = URLVariable.getDataFileSize();
        /**
         * The name of the file.
         * @type {string}
         */
        this.fileName = URLVariable.getDataFileName();

        /**
         * The WebGLRenderingContext.
         * @type {WebGLRenderingContext}
         */
        this.webglContext = webgl;
        /**
         * The dataset contains time or not.
         * @type {boolean}
         */
        this.hasTime = this.timeVariable ? true : false;

        //COMBINATIONS
        /**
         * Map where each key is a combination string and it's value is the index of it's position in the visual variable combinations array.
         * @type {Map<string, number>}
         */
        this.mapVarCombinationStrToIndexMap = new Map();
        /**
         * Array of visual variable combinations.
         * @type {Array<MapVariableCombination>}
         */
        this.mapVarCombinations = [];
        if (!this.classCalculationRequired())
            this.createMapVariableCombinations(); //Call method to create VVCombinations and associated info

        //SAVE DATA FOR POLYGONS OR LINES
        /** 
         * The identifier of the type of primitive (Point, polygon or line).
         * @type {number}
         */
        this.geometryPrimitive;
        /** 
         * An array that holds all points of all polygons/lines. 
         * @type {Array<number>|WebGLBuffer}
         */
        this.allPoints = [];
        /**
         * An array that holds all colors for all polygons/lines. One color for each vertex.
         * @type {Array<number>|WebGLBuffer}
         */
        this.allColors = [];
    }

    /**
     * Returns true if there's the need to calculate the classes for any visual variable or not.
     * @returns {boolean} true, if any continuous variable requires class calculation.
     * @memberof GisplayOptions
     */
    classCalculationRequired() {
        for (let catVariable of this.categoricalVariables)
            if (catVariable.classCalculationRequired()) //If at least one cont or time 
                return true;
        for (let contVariable of this.continousVariables)
            if (contVariable.classCalculationRequired()) //If at least one cont or time 
                return true;
        if (this.timeVariable)
            return true;
        return false;
    }

    /**
     * Verify if the row has errors or not. This method is called by the respective Parser for each row or just for one row.
     * The programmer may decide to never call this method if he is sure the given options are all correct.
     * Basically will look if all given options (visual variables, Geometry, time and extra variables) are according to it's specification in the Parsing options.
     * @param {Array<Object>} row - Any row given to look for existing errors.
     * @memberof Parser
     */
    hasErrors(row) {
        let [vvs, geo, time, optionals] = [...row];
        let geometryExists = geo !== undefined;
        let timeExists = time !== undefined && time.lenght === 1;

        if (!geometryExists || (this.hasTime && !timeExists)
            || !this.continousVariables && vvs[0].length > 0
            || this.optionalVariables.length !== optionals.length) {
            console.log(row);
            throw new Error("Found errors parsing file.");
        }
        this.errorsAlreadyChecked = true;
    }

    /**
     * Create combinations of map variables. 
     * This method must only be executed when the classes for all continuous variables are calculated, otherwise it won't work.
     * @see https://stackoverflow.com/a/33352604 - Create array with index 0..n-1
     * @memberof Parser
     */
    createMapVariableCombinations() {
        let categoricalVariables = this.categoricalVariables;
        let continuousVariables = this.continousVariables;

        let indices = [];
        for (let catVar of categoricalVariables) {
            let numCategories = catVar.getVisualVariableMapping().size;
            indices.push([...Array(numCategories).keys()]); //.map(x => ++x) https://stackoverflow.com/a/33352604
        }
        for (let contVar of continuousVariables) {
            let numIntervals = contVar.getClassIntervals().length;
            indices.push([...Array(numIntervals).keys()]);
        }

        //Create visual variable combination strings
        let vvCombinationStrs = this.generateVVCombinationStrings(indices);
        let startingRGBAs = this.calculateStartingRGBAs(vvCombinationStrs.length);
        // console.warn(startingRGBAs);
        for (let [index, vvCombStr] of vvCombinationStrs.entries()) {
            // console.warn(index, vvCombStr);
            this.mapVarCombinations.push(
                new MapVariableCombination(
                    vvCombStr,
                    [...categoricalVariables, ...continuousVariables],
                    startingRGBAs[index],
                    this.timeVariable ? Array(this.timeVariable.granuleIndex).keys() : [],
                    this.webglContext,
                    this.anyQualifierIsAttribute()
                ));
        }
        // console.log(this.VVCombinations);
        this.mapVarCombinationStrToIndexMap = this.createMVCombinationStrToIndexMap(vvCombinationStrs); // Create the combination string -> index map
    }

    /**
     * Generates the visual variable combination strings.
     * If there's only one visual variable then return it's indices as an array of strings, otherwise, if there's more than one 
     * visual variable, then call the recursive method.
     * @param {Array<number>|Array<Array<number>>} indices 
     * @returns {Array<string>|Array<Array<string>>}
     * @memberof Parser
     */
    generateVVCombinationStrings(indices) {
        let vvCombinationStrs = [];
        if (indices.length === 1)
            for (let indice of indices[0])
                vvCombinationStrs.push('' + indice);
        else
            vvCombinationStrs = this.generateVVCombinationStringsRec(indices);
        return vvCombinationStrs;
    }

    /**
     * Recursive method that generates all possible combinations of values, given the arrays of values possible values for each visual variable.
     * These values can be indeces (0..n) of classes or categories and/or strings of categories.
     * @param {Array<Array<string|number>>} visVarsValues - Array with visual variable values 
     * @returns {Array<string>} - the resulting combinations of the given values.
     * @see https://stackoverflow.com/q/4331092
     * @memberof Parser
     */
    generateVVCombinationStringsRec(visVarsValues) {
        if (visVarsValues.lenght === 0)
            return [];
        else if (visVarsValues.length === 1)
            return visVarsValues[0];
        else {
            let result = [];
            let rec = this.generateVVCombinationStringsRec(visVarsValues.slice(1));
            for (let i = 0; i < rec.length; i++)
                for (let j = 0; j < visVarsValues[0].length; j++)
                    result.push('' + visVarsValues[0][j] + rec[i]);
            return result;
        }
    }

    /**
     * Calculates the first RGBA color for each visual variable combination.
     * This first color can be used to quickly identify the containing visual variable combination of the clicked element (Picking).
     * @param {number} nCombs - Number of combinations of visual variables. 
     * @returns {Array<Uint8Array>} - the color (RGBA) that starts 
     * @memberof Parser
     */
    calculateStartingRGBAs(nCombs) {
        let colorPerCombination = Math.round(Math.pow(256, 4) / nCombs); //4MM / number combinations
        let startingPoints = [];
        let [r, g, b, a] = [0, 0, 0, 1];
        startingPoints.push([r, g, b, a]);
        for (let i = 1; i < nCombs; i++)
            startingPoints.push(GisplayDefaults.numberToRGBA(i * colorPerCombination));
        return startingPoints;
    }

    /**
     * Creates a Map with as many keys as map variable combinations. 
     * Each combination will have an index generated sequentially.
     * @param {Array<string>} combinations - The calculated combinations.
     * @returns {Map<string, number>} - a map with each key being the map variable combination and the value is the respective index, generated sequentially.
     * @memberof Parser
     */
    createMVCombinationStrToIndexMap(combinations) {
        let associationMap = new Map();
        let index = 0;
        for (let combination of combinations)
            associationMap.set(combination, index++);
        return associationMap;
    }

    /**
     * Retuns the index of the given visual variable combination string.
     * @param {string} combinationString - The name of the visual variable combination.
     * @returns {number} - the index of the given visual variable combination string. 
     * @memberof Parser
     */
    findCombinationIndex(combinationString) {
        return this.mapVarCombinationStrToIndexMap.get(combinationString);
    }

    /**
     * This is the event called when the data is completly processed and should be send back to the Map (type) to be drawn and used accordingly.
     * @see https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events
     * @memberof Parser
     */
    dispatchParseEndEvent() {
        console.log("ParserEnd event");
        document.dispatchEvent(new CustomEvent("parserEnd", {  //Retornar pelo menos os dados processados e as classes 
            detail: {
                mapVariableCombinations: this.mapVarCombinations,
                mapVariableCombinationToIndexMap: this.mapVarCombinationStrToIndexMap,
                categoricalVariables: this.categoricalVariables,
                continousVariables: this.continousVariables,
                allPoints: this.allPoints,
                allColors: this.allColors
            }
        }));
    }

    /**
     * Returns true if any of the continuous variables has 'attribute' qualifier on the shader.
     * @returns {boolean} - true if any of the continuous variables has 'attribute' qualifier on the shader, false, otherwise.
     * @memberof Parser
     */
    anyQualifierIsAttribute() {
        return this.continousVariables.filter((cv) => cv.qualifierIsAttribute()).length > 0;
    }

    //DELETE THIS
    /**
     * Create transitory sub arrays
     * @param {number} numElements - Number of elements on the dataset. 
     * @memberof Parser
     */
    createTransitoryDataArray(numElements) {
        let numSubArrays = Math.ceil(numElements / (100 * 1000));
        for (let i = 0; i < numSubArrays; i++)
            this.transitoryArray[i] = [];
        window.ta = this.transitoryArray; //TODO: DELETE
    }

    /**
     * Add one row to the transitory data array.
     * @param {Array<Object>} row - The row with visual variables, geometry, time (if exists) and extra variables (if any).
     * @memberof Parser
     */
    addTransitoryData(row) {
        if (this.countTempRows++ % (this.maxArrayLength) === 0 && this.countTempRows !== 1)
            this.currentTransitoryArrayIndex++;
        if (!this.transitoryArray[this.currentTransitoryArrayIndex]) //Create the sub-array if it does not exist
            this.transitoryArray[this.currentTransitoryArrayIndex] = [];
        this.transitoryArray[this.currentTransitoryArrayIndex].push(row);
    }

    /**
     * Split each row that was read from the file to it's respective visual variable combination.
     * @memberof Parser
     */
    splitTransitoryData_OLD() {
        console.time("splitTransitoryData");
        //For each cont var that needs class cal then calculate
        for (let contVar of this.continousVariables)
            if (contVar.classCalculationRequired())
                contVar.calculateClasses();

        //Calculate and create VVCombinations and indexMap
        this.createMapVariableCombinations();

        //for takes 2.5 seconds for 1M rows (with points )
        console.time("for");
        //Split data to it's respective VVCombination
        for (let subArray of this.transitoryArray) {
            for (let row of subArray) {
                let vvCombStr = row[0][0];
                let index = 0;
                for (let contVar of this.continousVariables) {
                    if (contVar.classCalculationRequired())
                        vvCombStr += contVar.findClassIntervalIndex(row[0][1][index]);
                    index++;
                }
                // console.log(row);
                row[0] = row[0][1];
                // console.log(row); // <<<<<<<<<<<<<<<<<<<<<<<<<<<
                this.mapVarCombinations[this.findCombinationIndex(vvCombStr)].addDataRow(row);
                // break; // <<<<<<<<<<<<<<<<<<<<<<<<<<<
            }
        }
        console.timeEnd("for");

        console.timeEnd("splitTransitoryData");
        // console.log(this);
        // delete this.transitoryArray;
    }

    /**
     * Split each row that was read from the file to it's respective visual variable combination.
     * @memberof Parser
     */
    splitTransitoryData() {
        throw new GisplayError('Not yet implemented method to split transitory data array for this parser');
    }

    /**
     * For each temporal granularity inside of each visual variable combination, join all data points in a single WebGLBuffer.
     * @memberof Parser
     */
    joinMapVarCombinationData() {
        for (const mvc of this.mapVarCombinations)
            mvc.joinTemporalGranuleData(this.webglContext, this.geometryPrimitive);
    }

    /**
     * If the primitive to draw are points then join all RGBA identifiers into one WebGLBuffer.
     * @memberof Parser
     */
    joinPickingColors() {
        for (const mvc of this.mapVarCombinations)
            mvc.joinPickingColors();
    }

    /**
     * For each temporal granule inside each map variable combination, update it's numElements variable.
     * @memberof Parser
     */
    setNumElementsTemporalGranules() {
        for (const mvc of this.mapVarCombinations)
            for (const tg of mvc.getTemporalGranules())
                tg.setNumElements();
    }

    /**
     * Join all points of all polygons/lines into a single WebGL buffer.
     * @memberof Parser
     */
    joinAllPoints() {
        let webglContext = this.webglContext;
        const vertArray = new Float32Array(this.allPoints);
        const bufferP = webglContext.createBuffer();
        webglContext.bindBuffer(webglContext.ARRAY_BUFFER, bufferP);
        webglContext.bufferData(webglContext.ARRAY_BUFFER, vertArray, webglContext.STATIC_DRAW);
        this.allPoints = bufferP;
    }

    /**
     * Join all points of all polygons/lines into a single WebGL buffer.
     * @memberof Parser
     */
    joinAllColors() {
        let webglContext = this.webglContext;
        const vertArray = new Uint8Array(this.allColors);
        const bufferP = webglContext.createBuffer();
        webglContext.bindBuffer(webglContext.ARRAY_BUFFER, bufferP);
        webglContext.bufferData(webglContext.ARRAY_BUFFER, vertArray, webglContext.STATIC_DRAW);
        this.allColors = bufferP;
    }

    /**
     * Set the geometry primitive of each Temporal Granule.
     * @memberof Parser
     */
    setGeometryPrimitiveTGs() {
        // console.error(this.geometryPrimitive);
        for (const mvc of this.mapVarCombinations)
            for (const tg of mvc.getTemporalGranules())
                tg.setGeometryPrimitive(this.geometryPrimitive);
    }

    /**
     * Split Transitory data, join points if needed and dispatch the parser end event.
     * @memberof Parser
     */
    endParser(parserName) {
        // console.error(this.transitoryArray);
        throw new GisplayError(`Parser has no endParser method...`);
        /*     switch (parserName) {
                case 'csv': {
                    if (this.urlVariable.hasIds()) {
                        console.error("HASURLS");
                        console.log(this.mapVarCombinations);
                        this.dispatchParseEndEvent();
                    }
                    else {
                        this.setGeometryPrimitiveTGs();
    
                        this.setNumElementsTemporalGranules();
                        console.time("joinPoints");
                        // this.geometryPrimitive = 0;
                        if (this.geometryVariables.length > 1) {
                            this.joinMapVarCombinationData();
                            this.joinPickingColors();
                        }
                        console.timeEnd("joinPoints");
                        this.dispatchParseEndEvent(); //TODO: Move to the end of the Map?
                    }
                    break;
                }
                case 'geojson': {
                    if (this.classCalculationRequired())
                        this.splitTransitoryData();
    
                    this.setGeometryPrimitiveTGs(); //Set the type of Geo for each TG (same as this.geoPrimitive)
                   
                    if (GisplayDefaults.hasPolygons(this.geometryPrimitive) || GisplayDefaults.hasLines(this.geometryPrimitive)){
                        this.joinAllPoints();
                        this.joinAllColors();
                    } else{
                        this.joinPickingColors();
                    }
    
                    this.setNumElementsTemporalGranules();
    
                    console.time("joinDATA");
                    this.joinMapVarCombinationData();
                    console.timeEnd("joinDATA");
                    this.dispatchParseEndEvent();
                    break;
                }
            } */
    }
}