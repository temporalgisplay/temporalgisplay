
/**
 * Represents the url variable that is given by the programmer in the parsing options.
 * @export
 * @class URLVariable
 */
export class URLVariable {
    /**
     * Creates an instance of URLVariable.
     * @param {File|string} dataURL - URL of the data to read.  
     * @param {File|string} geoSpatialURL - URL of the geometry and identifiers to read.
     * @param {string} idOnDataURL - The id of the geometry in the data URL.
     * @param {string} idOnGeoSpatialURL - The id of the geometry in the geospatial URL.
     * @memberof URLVariable
     */
    constructor(dataURL, geoSpatialURL, idOnDataURL, idOnGeoSpatialURL) {
        /**
         * The data url file handler.
         * @type {File}
         */
        this.dataURL = dataURL;
        /**
         * The geospatial url file handler.
         * @type {File}
         */
        this.geoSpatialURL = geoSpatialURL;
        /**
         * The id of the geometry on the data url.
         * @type {string}
         */
        this.idOnDataURL = idOnDataURL;
        /**
         * The id of the geometry on the geospatial url. 
         * @type {string}
         */
        this.idOnGeoSpatialURL = idOnGeoSpatialURL;
    }

    /**
     * Returns the data URL.
     * @returns {File} - The data file (URL) or a URL from the web.
     * @memberof URLVariable
     */
    getDataFile() {
        return this.dataURL;
    }

    /**
     * Returns the name of the data file.
     * @returns {string} - the name of the data file.
     * @memberof URLVariable
     */
    getDataFileName() {
        return this.dataURL.name;
    }

    /**
     * Returns the size in bytes of the data file.
     * @returns {number} - the size in bytes of the data file.
     * @memberof URLVariable
     */
    getDataFileSize() {
        return this.dataURL.size;
    }

    /**
     * Returns the geoSpatial URL.
     * @returns {File|string} - The geospatial file (URL) or a URL from the web.
     * @memberof URLVariable
     */
    getGeoSpatialURL() {
        return this.geoSpatialURL;
    }

    /**
     * Returns the name of the geospatial file.
     * @returns {string} - the name of the geospatial file.
     * @memberof URLVariable
     */
    getGeospatialName() {
        return this.geoSpatialURL.name;
    }

    /**
     * Returns the size in bytes of the geospatial file.
     * @returns {number} - the size in bytes of the geospatial file.
     * @memberof URLVariable
     */
    getGeospatialSize() {
        return this.geoSpatialURL.size;
    }

    /**
     * Returns the id of the geometry in the data URL.
     * @returns {string} - the id of the geometry in the data URL.
     * @memberof URLVariable
     */
    getIdOnDataURL() {
        return this.idOnDataURL;
    }

    /**
     * Returns the id of the geometry in the geospatial URL.
     * @returns {string} - the id of the geometry in the geospatial URL.
     * @memberof URLVariable
     */
    getIdOnGeoSpatialURL() {
        return this.idOnGeoSpatialURL;
    }

    /**
     * Returns true if the data file is a geojson file. 
     * @returns {boolean} - true, if the data file is a geojson file, false, otherwise.
     * @memberof URLVariable
     */
    dataFileIsGeoJSON() {
        return this._isGeoJSON(this.getDataFileName());
    }

    /**
     * Verifies if the given name is a GeoJSON file.
     * @param {string} name - The name of the file. 
     * @returns {boolean} - true, if it's a geojson file, false, otherwise.
     * @private
     * @memberof URLVariable
     */
    _isGeoJSON(name) {
        return name.endsWith('.json') || name.endsWith('.geojson');
    }

    /**
     * Returns true, if both ids exist, false otherwise.
     * @returns {boolean} - true, if both ids exist, false, otherwise.
     * @memberof URLVariable
     */
    hasIds() {
        return this.idOnDataURL !== undefined && this.idOnGeoSpatialURL !== undefined;
    }
}