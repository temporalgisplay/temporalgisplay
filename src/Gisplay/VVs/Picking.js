import { GisplayDefaults } from '../GisplayDefaults';

export class WebGLPicking {

    /**
     * Creates an instance of WebGLPicking.
     * @param {WebGLRenderingContext} webglContext - The WebGL context.
     * @memberof WebGLPicking
     */
    constructor(webglContext) {
        /**
         * @type {WebGLRenderingContext}
         */
        this.webglContext = webglContext;

        this.continuousData = [];
        this.optionalInfo = [];

        this.transitoryPickingColors = [];
        this.finalPickingColors = [];
        this.searchColors = [];
    }

    /**
     * Add the picking information.
     * @param {Array<Object>} dataRow - The row of data that was given by the Parser.
     * @param {number} geometryPrimitive - The identifier of the geometry primitive.
     * @memberof TemporalGranule
     */
    addRow(row, hasPoints) { //GEOJSON
        // console.log(row);
        if (hasPoints)
            this.transitoryPickingColors.push(...row[1].RGBA);
        this.searchColors.push(GisplayDefaults.RGBAToNumber(...row[1].RGBA));

        // this.continuousData.push(row[0]); //@TODO: If it does not exist no need to do this 
        // this.optionalInfo.push(row[2]); //@TODO: If it does not exist no need to do this 
        for (let i = 0; i < row[0].length; i++) {
            if (!Array.isArray(this.continuousData[i]))
                this.continuousData[i] = [];
            this.continuousData[i].push(row[0][i]);
        }
        for (let i = 0; i < row[2].length; i++) {
            if (!Array.isArray(this.optionalInfo[i]))
                this.optionalInfo[i] = [];
            this.optionalInfo[i].push(row[2][i]);
        }
    }

    /**
     * Add a set of rows to this temporal granule.     
     * @param {{continuousData: Array<Array<number>>, spatialData: Array<number>,optionalData: Array<Array<number|string>>, RGBA: Array<number>, numberRGBA: Array<number>}} dataRows - 
     * @memberof WebGLPicking
     */
    addSetOfRows(dataRows) {//CSV
        if (this.searchColors.length > 0 && dataRows.numberRGBA.length > 0) { //@TODO: DELETE IF
            if (!(this.searchColors[this.searchColors.length - 1] < dataRows.numberRGBA[0])) {
                let currRGBA = [];
                for (let i = this.transitoryPickingColors.length - 4; i < this.transitoryPickingColors.length; i++)
                    currRGBA.push(this.transitoryPickingColors[i]);
                let givenRGBA = [];
                for (let i = 0; i < 4; i++)
                    givenRGBA.push(dataRows.RGBA[i]);
                console.error(currRGBA, givenRGBA);
            }
            console.assert(this.searchColors[this.searchColors.length - 1] < dataRows.numberRGBA[0])
        }

        for (const rgba of dataRows.RGBA)
            this.transitoryPickingColors.push(rgba);

        for (const NumRGBA of dataRows.numberRGBA)
            this.searchColors.push(NumRGBA);
        // this.searchColors.push(...dataRows.numberRGBA);

        for (let i = 0; i < dataRows.continuousData.length; i++) {
            if (!Array.isArray(this.continuousData[i]))
                this.continuousData[i] = [];
            for (const contData of dataRows.continuousData[i])
                this.continuousData[i].push(contData);
            // this.continuousData[i].push(...dataRows.continuousData[i]);
        }
        for (let i = 0; i < dataRows.optionalData.length; i++) {
            if (!Array.isArray(this.optionalInfo[i]))
                this.optionalInfo[i] = [];
            for (const optData of dataRows.optionalData[i])
                this.optionalInfo[i].push(optData);
            // this.optionalInfo[i].push(...dataRows.optionalData[i]);
        }
    }

    getPickingColors() {
        return this.finalPickingColors;
    }

    getSearchColors() {
        return this.searchColors;
    }

    /**
     * Find the index of the given RGBA number in the list of searchable colors.
     * @param {number} numberRGBA - The RGBA number representation.
     * @param {any} hasIds - Boolean that represents the presence or absence or ids.
     * @returns {number} - -1 if the numberRGBA is not found.
     * @memberof WebGLPicking
     */
    findColorIndex(numberRGBA, hasIds) {
        if (!hasIds) { //If not ids then it can use the binary search
            /* let numPickingColors = this.searchColors.length;
            if (numPickingColors >= 2) {
                let firstColor = this.searchColors[0];
                let lastColor = this.searchColors[numPickingColors - 1];
                if (numberRGBA === firstColor)
                    return 0;
                else if (numberRGBA === lastColor)
                    return numPickingColors - 1;
                else if (numberRGBA > firstColor && numberRGBA < lastColor) {
                    console.warn("VAI PARA PESQUISA BINARIA", "first=", firstColor
                        , "numRGBA=", numberRGBA,
                        "lastColor", lastColor,
                        "indice=", numberRGBA - firstColor
                    )
                    // return numberRGBA - firstColor;
                    return this.binarySearchColorIndex(numberRGBA);
                }
                else
                    return -1;
            }
            else if (numPickingColors === 1 && numberRGBA === this.searchColors[0])
                return 0; */
            return this.binarySearchColorIndex(numberRGBA);
        } else { //Since it contains ids then the colors are not in a sequential order
            for (let i = 0; i < this.searchColors.length; i++)
                if (numberRGBA === this.searchColors[i])
                    return i;
        }
        return -1;
    }

    binarySearchColorIndex(numberRGBA) {
        let index = 0,
            min = 0,
            max = this.searchColors.length - 1;

        while (min <= max) {
            index = Math.floor((min + max) / 2);
            if (this.searchColors[index] === numberRGBA)
                return min;
            else if (this.searchColors[index] < numberRGBA)
                min = index + 1;
            else
                max = index - 1;
        }
    }

    getOptionalInfo() {
        return this.optionalInfo;
    }

    getIndexOptionalData(index) {
        let optionalInfo = [];

        for (const [i, optVar] of this.optionalInfo.entries())
            optionalInfo[i] = this.optionalInfo[i][index];
        return optionalInfo;
    }

    getIndexContinuousData(index) {
        let contInfo = [];
        let numContVars = this.continuousData.length;
        // for (let i = 0; i < numContVars - 1; i++)
        //     contInfo[i] = this.continuousData[i][index];
        for (const [i, contVar] of this.continuousData.entries())
            contInfo[i] = this.continuousData[i][index];
        return contInfo;
    }

    joinPickingColorsPoints() {
        console.error("JOIN PICKING COLORS", this.transitoryPickingColors, this.transitoryPickingColors.length);
        let gl = this.webglContext;
        this.finalPickingColors = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.finalPickingColors);
        gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(this.transitoryPickingColors), gl.STATIC_DRAW);
        this.transitoryPickingColors = undefined;
    }

    /**
     * Replaces the RGBA with the RGBA from the GeoJSONIdsParser.
     * @param {number} rowIndex - The index where to replace.
     * @param {Array<number>} RGBA - The RGBA identifier.
     * @param {Array<number>} hasPoints - Boolean that represents the presence or absence of points.
     * @memberof WebGLPicking
     */
    replaceRowRGBAValue(rowIndex, RGBA, hasPoints) {
        this.searchColors[rowIndex] = GisplayDefaults.RGBAToNumber(...RGBA);

        if (hasPoints) // Only if its point that it matters
            for (let i = 0; i < 4; i++) // Replace color RGBA itself?
                this.transitoryPickingColors[rowIndex * 4 + i] = RGBA[i];
    }

    ///////////////////////////////////////////////////////////DELETEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    //@TODO DELETE
    joinPickingColorsPointsCSV() {
        console.error("JOIN PICKING COLORS", this.transitoryPickingColors, this.transitoryPickingColors.length);
        let allColors = [];
        // if(Array.isArray(this.pickingColors[0]))
        for (let RGBA of this.transitoryPickingColors)
            allColors.push(...RGBA);

        let gl = this.webglContext;
        this.finalPickingColors = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.finalPickingColors);
        gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(allColors), gl.STATIC_DRAW);
        // this.finalPickingColors = pickingBuffer;
    }

    //@TODO DELETE
    //http://prntscr.com/i3ucmf
    //https://stackoverflow.com/a/15753839/
    joinPickingColorsPolygonsOLD(polygons) { //generate color for each vertex (each indice generated)
        console.warn("JOIN PICKING COLORS POLYGONS");
        let finalColors = [];
        let numTotalVertices = 0;
        for (const [i, polygon] of polygons.entries()) {
            let numVertices = polygon.polygonIndices.length; //Numero vertices do poligono ()
            numTotalVertices += numVertices; //just count
            let RGBA = this.transitoryPickingColors[i]; //[ [RGBA], [RGBA]]
            // console.log(polygon.polygonIndices, RGBA);
            let [r, g, b, a] = [...RGBA];

            //GENERATE COLORS FOR POLYGONS "RANDOM"
            /*   if (!window.i)
                  window.i = 200;
              [r, g, b, a] = [200, 200, 200, window.i++]; */

            for (let j = 0; j < numVertices; j++)
                finalColors.push(r, g, b, a);
        }
        console.assert(numTotalVertices === finalColors.length / 4);
        console.log(finalColors);
        console.log("NumTotalIndices = ", numTotalVertices, "Num Colors = ", finalColors.length);

        this.transitoryPickingColors = undefined;

        let gl = this.webglContext;
        this.finalPickingColors = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.finalPickingColors);
        gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(finalColors), gl.STATIC_DRAW);

        // console.log(gl.isBuffer(this.finalPickingColors));

        // this.finalPickingColors = pickingBuffer;
        // throw new Error("WUT");
    }

    //@TODO DELETE
    joinPickingColorsPolygons(polygons) { //generate color for each vertex (each indice generated)

    }

    //@TODO DELETE
    joinPickingColorsLines(lines) { //generate color for each vertex (each indice generated)
        console.log("JOIN PICKING COLORS LINES", this.transitoryPickingColors, lines);
        let finalColors = [];
        let numTotalIndices = 0;
        for (const [i, line] of lines.entries()) {
            let numVertices = line.length;
            numTotalIndices += numVertices;
            let RGBA = this.transitoryPickingColors[i];
            let [r, g, b, a] = [...RGBA];
            for (let j = 0; j < numVertices; j++)
                finalColors.push(r, g, b, a);
        }
        console.log(finalColors);
        console.assert(numTotalIndices === finalColors.length / 4);
        console.log("NumTotalIndices = ", numTotalIndices, "Num Colors = ", finalColors.length / 4);


        this.transitoryPickingColors = undefined;

        let gl = this.webglContext;
        this.finalPickingColors = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.finalPickingColors);
        gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(finalColors), gl.STATIC_DRAW);
        // this.finalPickingColors = pickingBuffer;
        // throw new Error("WUT");
    }
}