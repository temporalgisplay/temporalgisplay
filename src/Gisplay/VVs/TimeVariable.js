import { DataVariable } from './DataVariable';
import { GisplayDefaults } from '../GisplayDefaults';

/**
 * Represents the time information.
 * @export
 * @class TimeVariable
 */
export class TimeVariable extends DataVariable {

    /**
     * Creates an instance of TimeVariable.
     * @param {string} externalName - The external name of the time variable.
     * @param {string} internalName - The internal name of the time variable.
     * @param {string} granularity - The granularity of the time variable.
     * @param {string} timeControl - The time control for the time variable.
     * @memberof TimeVariable
     */
    constructor(externalName, internalName, granularity, timeControl) {
        super(externalName, internalName);
        /**
         * The time granularity.
         * @type {string}
         */
        this.granularity = granularity;
        /**
         * The granules for this time variable that were processed from the file.
         * @type {Set<string|number>}
         */
        this.granules = new Set();
        /**
         * A map with each temporal granule and it's index.
         * @type {Map<number|string, number>}
         */
        this.temporalGranulesMap = new Map();
        /**
         * The current granule index. This is used to give each granule it's specific index.
         * @type {number}
         */
        this.granuleIndex = 0;
        /**
         * The temporal granules sorted by Date. If its a number (year or day or value etc) then sorted by number.
         * @type {Array<number|string>}
         */
        this.sortedTemporalGranulesArr = [];
        /** 
         * The map that contains the string that represents the date and the respective date (e.g. 2016_1_1 => 1 January 2016 OR 2016 => 2016)
         * If the date is a number (e.g. year, monthOfYear, value etc) then the key and the value are equal (2016 => 2016).
         * A value here will be a key in the temporalGranulesMap.
         * @type {Map<string|number, string|number>}
         */
        this.strToTemporalGranuleMap = new Map();
        /** 
         * The map with the string temporal granules and their respective index inm the sorted temporal granules array.
         * E.g. Map (3)  {"1 January 2016" => 0, "2 January 2016" => 1, "3 January 2016" => 2, "4 January 2016" => 3 }.
         * Used to quickly search for the index of the start and end point of the time control.
         * A value here is an index in the sortedTemporalGranulesArr.
         * @type {Map<string|number, number>}
         */
        this.sortedTGsToIndexMap = new Map();
        /**
         * The time control to use.
         * @type {string}
         */
        this.timeControl = timeControl;
    }

    /**
     * The granularity
     * @returns {string} - The granularity. 
     * @memberof TimeVariable
     */
    getGranularity() {
        return this.granularity;
    }

    /**
     * Returns the type of time control that should be used.
     * @returns 
     * @memberof TimeVariable
     */
    getTypeOfTimeControl() {
        return this.timeControl ? this.timeControl : GisplayDefaults.INSTANT();
    }

    /**
     * Available granularities for the time value.
     * @returns {Array<string>} - the set of available granularities.  
     * @memberof TimeVariable
     */
    _getAvailableGranularities() {
        return ['year', 'month', 'day', 'value', //Continuous
            'monthOfYear', 'dayOfYear', 'dayOfMonth', 'hourOfDay', 'minuteOfHour' // Cyclic
        ];
    }

    //GRANULES ETC
    /**
     * Add all values to the temporal granules map.
     * @param {Array<number>} values - The array of values to add to the granules map.
     * @memberof TimeVariable
     */
    createTemporalGranulesMap(values) {
        for (let val of values)
            this.temporalGranulesMap.set(val, this.granuleIndex++);
    }

    /**
     * Add temporal granule to the temporal granules set and return it's index if it does not exist.
     * @param {number} temporalGranule - The temporal granule to be added. 
     * @returns {number} - the index of the given temporal granule.
     * @memberof TimeVariable
     */
    addTemporalGranule(temporalGranule) {
        if (this.temporalGranulesMap.has(temporalGranule))
            return this.temporalGranulesMap.get(temporalGranule);
        else {
            this.temporalGranulesMap.set(temporalGranule, this.granuleIndex);
            return this.granuleIndex++;
        }
    }

    /**
     * Add the given temporal granule to the set of existing temporal granules.
     * @param {string|number} temporalGranule - The temporal granule to add to the set of temporal granules.
     */
    addTemporalGranuleToSet(temporalGranule) {
        this.granules.add(temporalGranule);
    }

    /**
     * Add temporal granules to the set of temporal granules.
     * @param {Set<string>} temporalGranules - The temporal granules. 
     * @memberof TimeVariable
     */
    addTemporalGranulesToSet(temporalGranules) {
        for (const temporalGranule of temporalGranules.keys())
            this.addTemporalGranuleToSet(temporalGranule);
    }

    /**
     * Returns the Set of temporal granules.
     * @returns {Set<string>} - the Set of temporal granules. 
     * @memberof TimeVariable
     */
    getTemporalGranulesSet() {
        return this.granules;
    }

    /**
     * Returns the temporal granules that exist on the dataset. E.g. 2016, 2017 etc
     * @returns {Map<number, number>} - the temporal granules that exist on the dataset.
     * @memberof TimeVariable
     */
    getTemporalGranules() {
        return this.temporalGranulesMap;//Array.from(this.temporalGranulesMap.keys());
    }

    /**
     * Returns true if the temporal granule values were given, false, otherwise.
     * @returns {booelan} - true if the temporal granule values were given, false, otherwise.
     * @memberof TimeVariable
     */
    classCalculationRequired() {
        return true;
    }

    /**
     * Sort the temporal granules creating an array with those sorted elements.
     * If the elements are dates then order by date, if they are numbers order by number.
     * @returns {Array<string|number>} - the sorted temporal granules, sorted by date/number.
     * @memberof TimeVariable
     */
    sortTemporalGranules() {
        let availableGranularites = this._getAvailableGranularities();
        let tgs = this.getTemporalGranules().keys();
        let granularitiesArray = []; // 
        let strToTGMap = new Map(); // 26_1_0 => 1 January 2016
        for (const tg of tgs) {
            let strGranularity;
            switch (this.granularity) {
                case availableGranularites[1]: //Month
                    strGranularity = this._getMonth(tg);
                    break;
                case availableGranularites[2]: //Day
                    strGranularity = this._getDay(tg);
                    break;
                default:
                    strGranularity = tg;
                    break;
            }
            granularitiesArray.push(strGranularity);
            strToTGMap.set(strGranularity, tg);
        }
        this.strToTemporalGranuleMap = strToTGMap;
        this.sortedTemporalGranulesArr = granularitiesArray.sort((a, b) => { return new Date(a) - new Date(b); });
        for (let [i, sortedTG] of this.sortedTemporalGranulesArr.entries())
            this.sortedTGsToIndexMap.set(sortedTG, i);
        /*   console.warn(this.sortedTemporalGranulesArr);
          console.warn(this.strToTemporalGranuleMap);
          console.warn(this.sortedTGsToIndexMap); */
        this._sortTemporalGranulesMap();
        return this.sortedTemporalGranulesArr;
    }

    /**
     * Sort the temporal granules strings (read from file) to the indices of the sorted
     * temporal granules.
     * @private
     * @memberof TimeVariable
     */
    _sortTemporalGranulesMap() {
        let sortedIndexMap = new Map();
        let sortedTGsToIndexMap = this.sortedTGsToIndexMap; //January 2017 -> 0, February 2017 -> 1
        let strToTGMap = this.strToTemporalGranuleMap; //January 2017 -> 2017_1, February 2017 -> 2017_2
        for (let key of sortedTGsToIndexMap.keys())
            sortedIndexMap.set(strToTGMap.get(key), sortedTGsToIndexMap.get(key)); //2017_1 -> 0
        this.temporalGranulesMap = sortedIndexMap;
    }

    /**
     * Returns the locale month string. 
     * In portugal will return 'janeiro', 'fevereiro' etc... 
     * @param {string} dateString - The date string read from the file.
     * @returns {string} - the locale month string.
     * @private
     * @see https://stackoverflow.com/a/18648314/
     * @memberof TimeVariable
     */
    _getLocaleMonth(dateString) {
        let date = new Date(dateString.replace(/_/g, '-'));
        return date.toLocaleString("en-us", { month: "long" });
    }

    /**
     * Returns the month name plus the year.
     * @param {string} dateString 
     * @returns {string} - the month name plus the year.
     * @memberof TimeVariable
     */
    _getMonth(dateString) {
        let date = new Date(dateString.replace(/_/g, '-'));
        let monthName = this._getLocaleMonth(dateString);
        let year = date.getFullYear();
        return monthName + " " + year;
    }

    /**
     * Get the date including the day (day month year).
     * @param {string} dateString - The date string read from the file.
     * @returns {string} - the date including the day.
     * @see https://stackoverflow.com/a/18648314/
     * @private
     * @memberof TimeVariable
     */
    _getDay(dateString) {
        let date = new Date(dateString.replace(/_/g, '-'));
        let monthName = this._getLocaleMonth(dateString);
        let day = date.getDate();
        let year = date.getFullYear();
        return day + " " + monthName + " " + year;
    }

    /**
     * Returns the index of the temporal granule given by the string. 
     * @param {string|number} temporalgranuleStr - The temporal granule string representation.
     * @returns {number} - the index of the temporal granule given by the string.
     * @memberof TimeVariable
     */
    getGranuleIndexFromStr(temporalgranuleStr) {
        let key = this.strToTemporalGranuleMap.get(temporalgranuleStr);
        return this.temporalGranulesMap.get(key);
    }

    /**
     * Returns the index of the temporal granule read from the file.
     * @param {string} str - The given string 
     * @returns {number} - the index of the temporal granule read from the file.
     * @memberof TimeVariable
     */
    getTGMapIndex(str) {
        return this.temporalGranulesMap.get(str);
    }

    /**
     * Returns the sorted temporal granules. 
     * @returns {Array<string|number>} - the sorted temporal granules.  
     * @memberof TimeVariable
     */
    getSortedTemporalGranules() {
        return this.sortedTemporalGranulesArr;
    }

    /**
     * Returns the indices of the temporal granules between the start and end temporal granules string representations.
     * @param {any} startTGStr - The string representation of the range start temporal granule. 
     * @param {any} endTGStr - The string representation of the range end temporal granule. 
     * @returns {Array<number>} - the indices of the temporal granules between the given start and end temporal granules.
     * @memberof TimeVariable
     */
    getRangeIndices(startTGStr, endTGStr) {
        let [sortedStartIndex, sortedEndIndex] = [this.sortedTGsToIndexMap.get(startTGStr), this.sortedTGsToIndexMap.get(endTGStr)];
        let sortedTGsArrSliced = this.sortedTemporalGranulesArr.slice(sortedStartIndex, sortedEndIndex + 1);
        let rangeIndices = [];
        for (let sortedTG of sortedTGsArrSliced)
            rangeIndices.push(this.getGranuleIndexFromStr(sortedTG));
        return rangeIndices;
    }

    /**
     * Returns the index of the given temporal granule string/number. 
     * The index is the value associated with  the given key.
     * @param {string|number} sortedTemporalGranule - The temporal granule string/number.
     * @returns {number} - the index of the given temporal granule string/number. 
     * @memberof TimeVariable
     */
    getSortedIndexFromStr(sortedTemporalGranule) {
        return this.sortedTGsToIndexMap.get(sortedTemporalGranule);
    }

    /**
     * Returns the index of the next temporal granule on the "timeline".
     * @param {string|number} currentTG - The current temporal granule.
     * @returns {number} - the index of the next temporal granule on the "timeline".
     * @memberof TimeVariable
     */
    findNextTGIndex(currentTG) {
        let indexSortedTG = this.sortedTGsToIndexMap.get(currentTG);
        let nextSortedTG = this.sortedTemporalGranulesArr[indexSortedTG + 1];
        return this.getSortedIndexFromStr(nextSortedTG);
    }

    /**
     * Returns the index of the previous temporal granule on the "timeline".
     * @param {string|number} currentTG - The current temporal granule.
     * @returns {number} - the index of the previous temporal granule on the "timeline".
     * @memberof TimeVariable
     */
    findPreviousTGIndex(currentTG) {
        let indexSortedTG = this.sortedTGsToIndexMap.get(currentTG);
        let previousTG = this.sortedTemporalGranulesArr[indexSortedTG - 1];
        return this.getSortedIndexFromStr(previousTG);
    }
}