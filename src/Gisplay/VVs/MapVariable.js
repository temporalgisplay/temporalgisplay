import { DataVariable } from './DataVariable';


/**
 * Represents a Map Variable.
 * @see https://eagereyes.org/basics/data-continuous-vs-categorical
 * @see http://leg.ufpr.br/~silvia/CE055/node8.html
 * @export
 * @class MapVariable
 */
export class MapVariable extends DataVariable {

    /**
     * Creates an instance of MapVariable.
     * @param {string} externalName - The external name for this variable.
     * @param {string} internalName - The internal name for this variable.
     * @param {string} typeOfVisualVariable - The type of visual variable (e.g. color, shape, size, orientation or texture).
     * @param {string} shaderVariableQualifier - The shader variable qualifier. 
     * @param {Array<string|number>|Map<string, string|number>} visualVariableMapping - The mapping for the given type of visual variable.
     * @memberof MapVariable
     */
    constructor(externalName, internalName, typeOfVisualVariable, shaderVariableQualifier, visualVariableMapping) {
        super(externalName, internalName);
        /**
         * The type of visual variable mapping (e.g. color, shape, size, orientation or texture).
         * @type {string}
         */
        this.typeOfVisualVariable = typeOfVisualVariable;
        /**
         * The access qualifier for the shader variable.
         * @type {string}
         */
        this.shaderQualifier = shaderVariableQualifier;
        /**
         * The shader qualifier is of type attribute or not.
         * @type {boolean} 
         */
        this.qualifierIsAttributeBool = shaderVariableQualifier === 'attribute';
        /**
         * The mapping between the values and the type of visual variable availabe values.
         * E.g. 'THEFT' => 'triangle'
         * @type {Map<string, string>|Array<string|number>}
         */
        this.visualVariableMapping = visualVariableMapping;
        /**
         * The mapping between each class/category index and it's visual variable usable value.
         * Possible keys: 0 .... n-1
         * Possible values: 1) Array of numbers for color
         *                  2) Index number for shapes and textures
         *                  3) Number for size and orientation
         * E.g. 'THEFT' => 'triangle' => 8 (8 is the index of the triangle in the shapes map)
         *      'NO THEFT' => 'square' => 4
         * Then we would have Map( 0 -> 8, 1 -> 4)
         * @type {Map<number,Array<number>|number>}
         */
        this.indexToUsableValueMap = new Map();
    }

    /**
     * Returns the type of visual variable (e.g. color, shape, size, orientation or texture) that this map variable will be mapped to.
     * @returns {string} - the type of visual variable mapping (e.g. color, shape, size, orientation or texture).
     * @memberof MapVariable
     */
    getTypeOfVisualVariable() {
        return this.typeOfVisualVariable;
    }

    /**
     * Returns the mapping for the type of visual variable. 
     * It can be a Object<Key, Value> if it's categorical, an array of strings or array of numbers. 
     * @returns {Map<string, string>|Array<string|number>} - the mappings for this visual variable.
     * @memberof MapVariable
     */
    getVisualVariableMapping() {
        return this.visualVariableMapping;
        //Array number (Cont no size e orientation)
        //Array string (Cont para color, size e orientation + Cat para color)
        //Object K->V (Cat para shape, texture e orientation)
    }

    /**
     * This map variable requires it's classes to be calculated or not. Categorical variables always return false.
     * @returns {boolean} - true, if we need to calculate the classes, false, otherwise.
     * @memberof MapVariable
     */
    classCalculationRequired() {
        return false;
    }

    /**
     * Returns the mapping between each class/category and it's visual variable usable value.
     * @returns {Map<number,Array<number>|number>} - the mapping between each class/category and it's visual variable usable value.
     * @memberof MapVariable
     */
    getIndexToUSableValueMap() {
        return this.indexToUsableValueMap;
    }

    /**
     * The value associated with the given index.
     * E.g. [255,0,0] for color or 57 for size/orientation or 3 for shape/texture.
     * @param {number} index - The index of the visual variable.
     * @returns {Array<number>|number>}
     * @memberof MapVariable
     */
    findUsableValue(index) {
        return this.indexToUsableValueMap.get(index);
    }

    /**
     * The access qualifier for the shader variable 
     * @returns {string} - the access qualifier for the shader variable 
     * @memberOf MapVariable
     */
    getShaderVariableQualifier() {
        return this.shaderQualifier;
    }

    /**
     * Returns true if the shader qualifier is equal to attribute, false, otherwise.
     * @returns {boolean} - true if the shader qualifier is equal to attribute, false, otherwise.
     * @memberOf MapVariable
     */
    qualifierIsAttribute() {
        return this.qualifierIsAttributeBool;
    }
}