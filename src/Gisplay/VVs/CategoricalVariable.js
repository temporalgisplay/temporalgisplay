import { MapVariable } from './MapVariable';
import { GisplayDefaults } from '../GisplayDefaults';
import { GisplayError } from '../GisplayError';
// import { GisplayMap } from '../Maps/GisplayMap';

/**
 * Represents a categorical map variable.
 * Categorical data, is for those aspects of your data where you make a distinction between different groups, and where you typically can list a small number of categories. This includes product type, gender, age group, etc.
 * @see https://eagereyes.org/basics/data-continuous-vs-categorical
 * @export
 * @class CategoricalVariable
 */
export class CategoricalVariable extends MapVariable {

    /**
     * Creates an instance of CategoricalVariable.
     * @param {string} externalName - The external name for this variable.
     * @param {string} internalName - The internal name for this variable.
     * @param {string} typeOfVisualVariable - The name of visual variable (e.g. color, shape, size, orientation or texture).
     * @param {string} shaderVariableQualifier - The shader variable qualifier. 
     * @param {Map<string, string|number>} visualVariableMapping - The mapping for the given visual variable.
     * @param {GisplayMap} gisplayMap - The GisplayMap. It's used when we need to get the colors for the variable.
     * @memberof CategoricalVariable
     */
    constructor(externalName, internalName, typeOfVisualVariable, shaderVariableQualifier, visualVariableMapping, gisplayMap) {
        super(externalName, internalName, typeOfVisualVariable, shaderVariableQualifier, visualVariableMapping);
        /**
         * The categories for this categorical variable.
         * @type {Set<string>}
         */
        this.categories = new Set();
        if (visualVariableMapping && visualVariableMapping.size > 0) { //The mapping was given by the programmer (Map<"ABC"->"red", "DEF"->"blue" etc> )
            /**
             * Map containing the values for this categorical map variable and an integer to represent each of these values.
             * @type {Map<string, number>} 
             */
            this.valueToIndexMap = this._createCategoricalVarMap(visualVariableMapping.keys());
            this._setIndexToUsableValueMap();
        }

        /**  
         * Boolean that stores true if class calcutation is required, false, otherwise.
         * @type {boolean}
         */
        this.classCalculationRequiredBool = this.visualVariableMapping.size === 0;
        /**
         * The GisplayMap object.
         * @type {GisplayMap}
         */
        this.gisplayMap = gisplayMap;
        console.error(this);
    }

    /**
     * For each category in this categorical variable, create one index which will be used as substitute.
     * This replacement will save up a lot of space. 
     * @param {Array<string>} values - The categories that this visual variable holds. 
     * @returns {Map<string, number>} - the map which contains a sequential integer to represent each category.
     * @memberof CategoricalVariable
     */
    _createCategoricalVarMap(values) {
        let map = new Map();
        let index = 0;
        for (let value of values)
            map.set(value, index++);
        return map;
    }

    /**
     * Returns the value to index map.
     * @returns {Map<string, number>} - the map containing the values for this categorical visual variable and an integer to represent each of these values.
     * @memberof CategoricalVariable
     */
    getValueIndexMapping() {
        return this.valueToIndexMap;
    }

    /**
     * Returns the index of the given category.
     * @param {string} value - The category to find. 
     * @returns {number} - the index of the given category.
     * @memberof CategoricalVariable
     */
    findIndex(value) {
        return this.getValueIndexMapping().get(value);
    }

    /**
     * Set each index (of each category) to it's respective value (color, or number in the shape/texture image).
     * E.g. 0 -> [255,0,0] etc or 0 -> 14 (index in image).
     * @private
     * @memberof CategoricalVariable
     */
    _setIndexToUsableValueMap() {
        let typeOfVisualVariable = this.getTypeOfVisualVariable();
        let valueToIndexMapKeys = this.valueToIndexMap.keys();
        console.warn(valueToIndexMapKeys);
        console.warn(this.getVisualVariableMapping());

        switch (typeOfVisualVariable) {
            case GisplayDefaults.COLOR():
                for (let mapkey of valueToIndexMapKeys) {
                    let index = this.valueToIndexMap.get(mapkey); // 0, 1..
                    let color = this.visualVariableMapping.get(mapkey); // [255, 0, 0], [122, 220, 85] ...
                    this.indexToUsableValueMap.set(index, color);
                }
                break;
            case GisplayDefaults.SHAPE():
                for (let mapkey of valueToIndexMapKeys) { // "Y", "N"
                    let index = this.valueToIndexMap.get(mapkey); // 0, 1..
                    let shapeName = this.visualVariableMapping.get(mapkey); // "terrain", "bricks", "triangle"
                    let shapeIndex = GisplayDefaults.findShapeIndex(shapeName); // 6, 7, 12, 13
                    // console.log(index, shapeName, textureIndex);
                    this.indexToUsableValueMap.set(index, shapeIndex);
                }
                break;
            case GisplayDefaults.TEXTURE():
                for (let mapkey of valueToIndexMapKeys) { // "Y", "N"
                    let index = this.valueToIndexMap.get(mapkey); // 0, 1..
                    let patternName = this.visualVariableMapping.get(mapkey); // "terrain", "bricks", "triangle"
                    let patternIndex = GisplayDefaults.findPatternIndex(patternName); // 6, 7, 12, 13
                    console.log(index, patternName, patternIndex);
                    this.indexToUsableValueMap.set(index, patternIndex);
                }
                break;
            case GisplayDefaults.SIZE():
                throw new GisplayError("Visual variable: size, can't be used with a categorical variable.");
            case GisplayDefaults.ORIENTATION():
                console.log(valueToIndexMapKeys, this.valueToIndexMap, this.visualVariableMapping);
                for (let mapkey of valueToIndexMapKeys) { // "Y", "N"
                    let index = this.valueToIndexMap.get(mapkey); // 0, 1..
                    let orientationValue = this.visualVariableMapping.get(mapkey); // 0, 28, 90 (degrees)
                    console.log(index, orientationValue);
                    this.indexToUsableValueMap.set(index, orientationValue);
                }
            // throw new Error("Index to orientation value not yet implemented in Categorical Variable. Should look like COLOR + SHAPE/TEXTURE combined");
        }
        console.log("CAT VV_TO_INDEX_MAP =========================", this.indexToUsableValueMap);
    }

    /**
     * Add a categorie to the set of categories in this categorical variable.
     * @param {string} category - The category to add to this categorical variable. 
     */
    addCategory(category) {
        this.categories.add(category);
    }

    /**
     * Add the given categories to the set of categories of this Categorical variable.
     * @param {Set<string>} categories - The categories given. 
     */
    addCategories(categories) {
        for (const category of categories.keys())
            this.addCategory(category);
    }

    /**
     * Returns the set of categories for this Categorical variable.
     * @returns {Set<string>} - the set of categories for this Categorical variable.
     * @memberof CategoricalVariable
     */
    getCategories() {
        return this.categories;
    }

    /**
     * Update the information when all categories are on the Set. 
     * Used when the categories weren't given by the programmer.
     * @memberof CategoricalVariable
     */
    updateCategoricalInformation() {
        this.valueToIndexMap = this._createCategoricalVarMap(this.categories.keys());

        let categories = Array.from(this.categories);
        console.error(this.valueToIndexMap.size);
        if(this.valueToIndexMap.size < 2)
            throw new GisplayError(`Only one category (${Array.from(this.valueToIndexMap.keys())}) in the dataset. `);
        let colors = this.gisplayMap.getDefaultColors(this.valueToIndexMap.size, GisplayDefaults.QUALITATIVE());
        for (let [i, category] of categories.entries()) 
            this.visualVariableMapping.set(category, colors[i]);
        
        this._setIndexToUsableValueMap();
    }

    /**
     * This map variable requires it's classes to be calculated or not. Categorical variables always return false.
     * @returns {boolean} - true, if we need to calculate the classes, false, otherwise.
     * @memberof CategoricalVariable
     */
    classCalculationRequired() {
        return this.classCalculationRequiredBool; // the map has no K->V pairs
    }
}