/**
 * Represents a variable. This variable has an external and an internal Name.
 * External name is the name of the variable on the dataset.
 * Internal name is the name the programmer gave to this variable.
 * @export
 * @class DataVariable
 */
export class DataVariable {

    /**
     * Creates an instance of a Variable.
     * @param {string} externalName - The external name for this variable.
     * @param {string} internalName - The internal name for this variable.
     * @memberof DataVariable
     */
    constructor(externalName, internalName) {
        /**
         * It's the external name of this variable
         * @type {string}
         */
        this.externalName = externalName;
        /**
         * The internal name for this variable.
         * @type {string}
         */
        this.internalName = internalName;
    }

    /**
     * Returns the external name for this visual variable.
     * @returns {string} - the external name of the visual variable.
     * @memberof DataVariable
     */
    getExternalName() {
        return this.externalName;
    }

    /**
     * Returns the internal name for this visual variable.
     * @returns {string} - the internal name of the visual variable.
     * @memberof DataVariable
     */
    getInternalName() {
        return this.internalName;
    }
}