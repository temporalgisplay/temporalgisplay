import { MapVariable } from './MapVariable';
import { GisplayDefaults } from '../GisplayDefaults';
import { GisplayError } from '../GisplayError';

/**
 * Represents a continuous variable.
 * Continous data, is data where the values can change continuously, and you cannot count the number of different values (e.g. weight, price, profits, counts, etc). 
 * @see https://eagereyes.org/basics/data-continuous-vs-categorical
 * @export
 * @class ContinuousVariable
 */
export class ContinuousVariable extends MapVariable {
    /**
     * Creates an instance of ContinuousVariable.
     * @param {string} externalName - The external name for this variable.
     * @param {string} internalName - The internal name for this variable.
     * @param {string} typeOfVisualVariable - The type of visual variable (e.g. color, shape, size, orientation or texture).
     * @param {string} shaderVariableQualifier - The shader variable qualifier. 
     * @param {Array<string|number>} visualVariableMapping - The mapping for the given type of visual variable.
     * @param {Array<number>} classBreaks - The classes for this map variable.
     * @param {string} numberOfClasses - The number of classes for this continuous variable.
     * @param {string} classBreakMethod - The class breaks method to be used.
     * @param {Array<number|string>} classBreakMethodParams - The parameters for the class break method.
     * @memberof ContinuousVariable
     */
    constructor(externalName, internalName, typeOfVisualVariable, shaderVariableQualifier, visualVariableMapping, classBreaks, numberOfClasses, classBreakMethod, classBreakMethodParams) {
        super(externalName, internalName, typeOfVisualVariable, shaderVariableQualifier, visualVariableMapping);
        /**
         * The class intervals for the map variable.
         * @type {Array<number>}
         */
        this.classBreaks = classBreaks;
        /**
         * The number of classes for the map variable.
         * @type {number}
         */
        this.numberOfClasses = visualVariableMapping ? visualVariableMapping.length : numberOfClasses; //TODO: Send this on the constructor
        /**
         * The method to use to calculate classes for the map variable.
         * @type {string}
         */
        this.classBreakMethod = classBreakMethod;
        /**
         * Returns the array of params for the class break method.
         *  @type{Array<number|string>} 
         */
        this.classBreakMethodParams = classBreakMethodParams;

        /**
         * Number of bins. This value is hardcoded to 100. But it should work even if changed to another integer value.
         * @type {number}
         */
        this.numBins = 100;
        /**
         * The minimum value.
         * @type {number}
         */
        this.min = Number.MAX_SAFE_INTEGER - 1;
        /**
         * The maximum value.
         * @type {number}
         */
        this.max = Number.MIN_SAFE_INTEGER + 1;

        this.newMin = Number.MAX_SAFE_INTEGER;
        this.newMax = Number.MIN_SAFE_INTEGER;

        /**
         * Amount between any two bins.
         * @type {number}
         */
        this.step = -1;
        /**
         * The bin limits.
         * @type {Array<number>}
         */
        this.bins = new Array(100).fill(-1);
        /**
         * Histogram with number of values by bin interval. 
         * @type {Array<number>}
         */
        this.histogram = new Array(100).fill(0); //Array with 100 elements each filled with the value 0
        /**
         * The class intervals for the map variable.
         * @type {Array<Array<number>>}
         */
        this.classIntervals;

        if (this.classBreaks) {
            this.min = this.classBreaks[0];
            this.max = this.classBreaks[this.classBreaks.length - 1];
            this.step = (this.getMax() - this.getMin()) / (this.numBins);

            this.bins = this.buildBinIntervals();
            this.classIntervals = this.calculateClassIntervals(this.classBreaks);
        }
        if (this.getVisualVariableMapping())
            this._setIndexToUsableValueMap();

        /**  
         * Boolean that stores true if class calcutation is required, false, otherwise.
         * @type {boolean}
         */
        this.classCalculationRequiredBool = this.classBreaks === undefined;
        console.log(this);
    }

    /**
     * Returns the method to be used to calculate the classes for this map variable.
     * @returns {string} - the method to be used to calculate the classes for this variable.
     * @memberof ContinuousVariable
     */
    getClassBreakMethod() {
        return this.classBreakMethod;
    }

    /**
     * Returns the class intervals for this map variable.
     * @returns {Array<number>} - the class intervals for the visual variable.
     * @memberof ContinuousVariable
     */
    getClassBreaks() {
        return this.classBreaks;
    }

    /**
     * Sets the class breaks to the calculated class breaks.
     * @param {Array<number>} calculatedClassBreaks - The array of class intervals for the visual variable.
     * @memberof ContinuousVariable
     */
    setClassBreaks(calculatedClassBreaks) {
        this.classBreaks = calculatedClassBreaks;
    }

    /**
     * Returns true, if the class breaks weren't given by the programmer for the map variable, false, otherwise. 
     * @returns {boolean} - true, if the class breaks weren't given by the programmer for the visual variable, false, otherwise.  
     * @memberof ContinuousVariable
     */
    classCalculationRequired() {
        return this.classCalculationRequiredBool;
    }

    /**
     * Calculates class intervals for this variable.
     * @param {Array<number>} classBreaks - The class breaks given by the programmer. 
     * @returns {Array<Array<number>>} - the start and end for each class of this variable.
     * @memberof ContinuousVariable
     */
    calculateClassIntervals(classBreaks) {
        console.error("classBreaks = ", classBreaks, this)
        let res = [];

        /*     
            [min, a[, [a, b[, [b,c[, [c,max] -> caso em que o min dos dados é !== do a
            min - todos os valore menores que a
            [a,b,c] ->  [min, a[, [a, b[, [b,c[, [c,max] 
            Se min for == a então n cabe 
            >= a && < a 

            1º intervalos todos os inferiores a "a"
            o ultimo intervalo é todos os >= a c 
        */

        if (this.classBreaks !== undefined) {
            res.push([this.min, classBreaks[0]]);
            for (let i = 0; i < classBreaks.length - 1; i++)
                res.push([classBreaks[i], classBreaks[i + 1]]);
            res.push([classBreaks[classBreaks.length - 1], this.max]);
        } else {
            // res.push([this.min, classBreaks[0]]);
            for (let i = 0; i < classBreaks.length - 1; i++)
                res.push([classBreaks[i], classBreaks[i + 1]]);
            // res.push([classBreaks[classBreaks.length - 1], this.max]);
        }
        return res;
        // return null;
    }

    /**
     * Returns the class intervals for this class.
     * @returns {Array<Array<number>>} - the class intervals for this class. 
     * @memberof ContinuousVariable
     */
    getClassIntervals() {
        return this.classIntervals;
    }

    /**
     * Find the class index of the given value.
     * It's inside a class if it's value is equal or higher than the starting point and less than the end point.
     * @param {number} value - The value of the element.
     * @returns {number} - The  index of the class.
     * @memberof Parser
     */
    findClassIntervalIndex(value) {
        let classIntervals = this.getClassIntervals();
        for (let [index, classInterval] of classIntervals.entries())
            if (value >= classInterval[0] && value < classInterval[1])
                return index;
        if (value < classIntervals[0][0])
            return 0;
        return classIntervals.length - 1;
    }

    /**
     * Set the min and max values if the given value changes them.
     * @param {number} value - The given vaue. 
     * @memberof ContinuousVariable
     */
    setMinMax(value) {
        if (value < this.min)
            this.min = value;
        if (value > this.max)
            this.max = value;
        /* if (value < this.newMin)
            this.newMin = value;
        if (value > this.newMax)
            this.newMax = value; */
    }

    /*
    #####################################################################
    ######################     CLASS CALCULATION      ###################
    #####################################################################
    */
    /**
     * Returns the minimum value for the visual variable.
     * @returns {number} - the minimum value for the visual variable.
     * @memberof ContinuousVariable
     */
    getMin() {
        return this.min;
    }

    /**
     * Returns the maximum value for the visual variable.
     * @returns {number} -  the maximum value for the visual variable.
     * @memberof ContinuousVariable
     */
    getMax() {
        return this.max;
    }

    /**
     * Update the step vaue.
     * @memberof ContinuousVariable
     */
    setStep() {
        this.step = (this.getMax() - this.getMin()) / (this.numBins);
    }

    /**
     * Returns the step between each bin value.
     * @returns {number} - the step between each bin value.
     * @memberof ContinuousVariable
     */
    getStep() {
        return this.step;
    }

    /**
     * Returns the histogram for this continuous variable.
     * @returns {Array<number>} - the histogram for this continuous variable.
     * @memberof ContinuousVariable
     */
    getHistogram() {
        return this.histogram;
    }

    /**
     * Updates the histogram of this continuous variable with the histogram values
     * @param {Array<number>} histogram - The given histogram
     * @memberof ContinuousVariable
     */
    updateHistogram(histogram) {
        for (let i = 0; i < histogram.length; i++)
            this.histogram[i] += histogram[i];
    }

    /**
     * Return the number of classes.
     * @returns {numer} - the number of classes.
     * @memberof ContinuousVariable
     */
    getNumberOfClasses() {
        return this.numberOfClasses;
    }

    /**
     * Create 100 bins with equal intervals between the minimum and maximum value.
     * @param {number} min - The minimum for this continuous variable. 
     * @param {number} max - The maximum for this continuous variable.
     * @returns {Array<number>} - The bins that will be used to calculate the classes.
     * @memberof LegendClasses
     */
    buildBinIntervals() {
        // let step = (max - min) / this.numBins;
        if (this.step === -1)
            this.setStep();
        let resBins = [];
        let min = this.min;
        let max = this.max;
        resBins.push(min);
        for (let i = min + this.step; i < max; i += this.step)
            resBins.push(i);
        if (Math.round(resBins[resBins.length - 1]) === max || max - resBins[resBins.length - 1] < 0.0001) //min = 0, max = 199.2 Math.round(199.19999999999976) = 199 <-- ERROR
            resBins[resBins.length - 1] = max;
        else
            resBins.push(max);
        return resBins;
    }

    /**
     * Create the bins array with 101? values that represent the intervals.
     * @memberof ContinuousVariable
     */
    createBins() {
        this.bins = this.buildBinIntervals();
    }

    /**
     * Add the value to the respective bin index.
     * This method works at O(1) instead of O(n), since it only needs to calculate the index and add one to it's bin.
     * The alternative was to loop through all the bins and find where this value would fit.
     * @param {number} value - The value to insert into one of the bins. 
     * @see http://prntscr.com/gdn1wu - Reason for if statement
     * @memberof ContinuousVariable
     */
    addValueToBins(value) {
        let binIndex = Math.floor((value - this.min) / this.step);
        if (value >= this.max) //The case it is equal to the max value then change index to the previous.
            binIndex = this.numBins - 1;
        else if (value <= this.min)
            binIndex = 0;
        this.histogram[binIndex]++;
        // console.log("value", value, "binIndex", binIndex);
    }

    /**
     * Calculates the classes for the visual variable using the method specified by the programmer.
     * @memberof ContinuousVariable
     */
    calculateClasses() {
        //Usando os bins faz calculo de classes e alterar guardar nos classIntervals
        switch (this.getClassBreakMethod()) {
            case 'quantiles':
                this.classIntervals = this.calculateClassIntervals(this.quantiles());
                break;
            case 'equalintervals':
                // console.log('equalintervals');
                this.classIntervals = this.calculateClassIntervals(this.equalIntervals());
                break;
            default:
                throw new GisplayError(`Continuous Variable: ${this.getInternalName()}. Given method for class calculation does not exist. `);
        }

        if (this.classIntervals.length !== this.numberOfClasses)
            throw new GisplayError("The number of calculated class intervals is different from the number of classes");
    }

    /**
     * Calculate the intervals for each class using the quantiles method.
     * @param {Array<number>} histogram - Histogram with quantity of values by bin interval. 
     * @param {number} binValues - The bin values between the minimum and the maximum.
     * @param {number} numRows - Number of rows read from the file.
     * @param {number} numClasses - Number of classes to calculate.
     * @returns {Array<number>} -  The resulting classes using the quantiles method.
     * @see https://en.wikipedia.org/wiki/Quantile
     * @memberof ContinuousVariable
     */
    quantiles(numberOfClasses = this.getNumberOfClasses()) {
        let numRows = 0;
        for (let i = 0, length = this.histogram.length; i < length; i++)
            numRows += this.histogram[i];

        let numValsPerClass = numRows / numberOfClasses;
        // console.log(numRows, this.getNumberOfClasses(), numValsPerClass);
        let classes = [];
        classes.push(this.bins[0]);
        let sum = 0;
        for (let i = 0; i < this.histogram.length - 1; i++) {
            sum += this.histogram[i];
            if (sum >= numValsPerClass) {
                classes.push(this.bins[i + 1]);
                sum = 0;
            }
        }
        classes.push(this.bins[this.bins.length - 1]);
        // console.log(classes);
        // console.log(this);
        if (classes.length < numberOfClasses)
            console.warn(`Quantiles method couldn't create ${numberOfClasses} classes, instead it created ${classes.length - 1} classes. [${classes}]`);
        else if (classes.length - 1 > numberOfClasses)
            throw new GisplayError(`Something went wrong with quantiles method. Number of classes calculated is: ${classes.length - 1}, the intended values was ${this.getNumberOfClasses()}`);
        return classes;
    }

    /**
     * Taking the calculated bins, this method will calculate the intervals for each class using the equal intervals method.
     * @param {number} binValues - The bin values between the minimum and the maximum.
     * @param {number} numClasses - Number of classes to calculate.
     * @returns {Array<number>} - The resulting classes using the equal intervals method.
     * @see http://wiki.gis.com/wiki/index.php/Equal_Interval_classification
     * @memberof ContinuousVariable
     */
    equalIntervals(numberOfClasses = this.getNumberOfClasses()) {
        let interval = Math.round((this.getMax() - this.getMin()) / numberOfClasses); //To give a integer (this.bins.length - 1) / this.numClasses;
        // console.log("EquInt interval=", interval);
        // console.log(this.getMin(), this.getMax(), numberOfClasses);
        let classes = [];
        classes.push(this.bins[0]);
        let currentValue = this.getMin(); // Current value
        for (let i = 0; i < numberOfClasses - 1; i++) {
            currentValue += interval;
            classes.push(this._findBin(currentValue));
        }
        classes.push(this.bins[this.bins.length - 1]);

        // console.log("equalIntervals = ", classes);
        return classes;
    }

    /**
     * Find the bin that is higher or equal than the value and the distance to next bin value is smaller than to the first.
     * @param {number} value - The value used to find the correct bin. 
     * @returns {number} - the bin that is higher or equal than the value and the distance to next bin value is smaller than to the first. 
     * @private 
     * @memberof ContinuousVariable
     */
    _findBin(value) {
        for (let i = 0; i < this.bins.length - 1; i++) {
            let val1 = this.bins[i];
            let val2 = this.bins[i + 1];
            if (value >= val1 && value < val2) {
                let distToVal1 = value - val1;
                let distToVal2 = val2 - value;
                if (distToVal1 <= distToVal2)
                    return val1;
                return val2;
            }
        }
    }

    /**
     * Set each index (of each class) to it's respective value (color array, size or orientation value).
     * E.g. 0 -> [255, 0, 0], 1 -> [0, 128, 0] etc
     * @private
     * @memberof ContinuousVariable
     */
    _setIndexToUsableValueMap() {
        let typeOfVisualVariable = this.getTypeOfVisualVariable();
        switch (typeOfVisualVariable) {
            case GisplayDefaults.COLOR():
            case GisplayDefaults.SIZE():
            case GisplayDefaults.ORIENTATION():
                for (let [index, vvMapping] of this.getVisualVariableMapping().entries())
                    this.indexToUsableValueMap.set(index, vvMapping);
                break;
            case GisplayDefaults.SHAPE():
            case GisplayDefaults.TEXTURE():
                throw new GisplayError(`${typeOfVisualVariable} shouldn't be used with a continuous variable.`);
        }
        // console.log("CONT VV_TO_INDEX_MAP =========================", this.indexToUsableValueMap);
    }
}