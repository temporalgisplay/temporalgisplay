//Import for intellisense
import { CategoricalVariable } from '../VVs/CategoricalVariable';
import { ContinuousVariable } from '../VVs/ContinuousVariable';
import { GisplayDefaults } from '../GisplayDefaults';
import { Gisplay } from '../Gisplay';

/**
 * Represents a Legend.
 * @export
 * @class Legend
 */
export class Legend {

    /**
     * Creates an instance of Legend.
     * @param {CategoricalVariable|ContinuousVariable} mapVariable - The map variable used for this legend.  
     * @param {HTMLDivElement} parentLegendsDiv - The div element that is parent of all legends.  
     * @param {HTMLImageElement} shapesImage - The image with the shapes.
     * @memberof Legend
     */
    constructor(mapVariable, parentLegendsDiv, shapesImage) {
        /**
         * The map variable that will be used for this legend. Can be used to get information about the map variable (external and internal name,etc).
         * @type {CategoricalVariable|ContinuousVariable}
         */
        this.mapVariable = mapVariable;
        /**
         * The parent div element.
         * @type {HTMLDivElement}
         */
        this.parentLegendsDiv = parentLegendsDiv;
        /**
         * The id of the div that will contain this Legend
         * @type {string}
         */
        this.legendId = parentLegendsDiv.id + '_' + mapVariable.getExternalName();
        /**
         * The title of this Legend.
         * @type {string}
         */
        this.legendTitle = this.mapVariable.getInternalName();
        /**
         * The event to be fired each time the legend state changes. The legend states changes when the user clicks in one of it's elements.
         * @type {Event}
         */
        this.legendChangedEvent = new CustomEvent('LegendChangedEvent');
        /**
         * The default  background color for the legend div.
         * @type {string}
         */
        this.defaultBackgroundColor = 'rgba(128, 129, 185, .3)';
        /**
         * The visibility icons divs for this Legend. Used to quickly access the visibility state of all Legend elements.
         * @type {Array<HTMLDivElement>}
         */
        this.visibilityIconDivs = [];

        // console.log(mapVariable);
        switch (mapVariable.getTypeOfVisualVariable()) {
            case GisplayDefaults.COLOR(): {
                if (this._isCategoricalMapVariable())
                    this.verticalColorLegend();
                else {
                    let min = this.mapVariable.getMin(),
                        max = this.mapVariable.getMax();
                    let classes = this.mapVariable.getClassIntervals();
                    let percentages = classes.map((v) => { return ((v[1] - v[0]) / (max - min)) * 100; }); //Get the percentage for each range 
                    console.log(percentages);
                    window.mv = this.mapVariable;
                    if (percentages.every((p) => p > 15)) //Was able to fit the percentages
                        // this.verticalColorLegend();
                        this.horizontalColorLegend(classes, percentages);
                    else
                        this.verticalColorLegend();
                }
            }
                break;
            case GisplayDefaults.SHAPE():
                this.shapeLegend(shapesImage, GisplayDefaults.getShapeImageSize());
                break;
            case GisplayDefaults.SIZE():
                this.sizeLegend(shapesImage, GisplayDefaults.getShapeImageSize());
                break;
            case GisplayDefaults.TEXTURE():
                this.shapeLegend(shapesImage, GisplayDefaults.getPatternImageSize());
                break;
            case GisplayDefaults.ORIENTATION():
                this.orientationLegend(shapesImage, GisplayDefaults.getFigureImageSize());
                break;
        }

        /**
         * The minimized legend. Contains only the title of the legend and the maximize button.
         * @type {HTMLDivElement}
         */
        this.minimizedLegend = this._createMinimizedLegend();

        // console.error("CREATE LEGEND", mapVariable.getTypeOfVisualVariable());
    }

    /**
     * Returns the visibility icons base64 data.
     * @static
     * @returns {{hide:string, view:string}} - the visibility icons base64 data.
     * @see https://www.base64-image.de
     * @see https://goo.gl/pXprnd - Visible
     * @see https://goo.gl/FRMRRb - Hide
     * @memberof Legend
     */
    static visibilityIcons() {
        return {
            hide: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAwSSURBVHja7Zx7dBTVHcc/s0kwRiAYhIqAeEAEQmkrVqmPHiBBSBATkAoFa5Ejta2vVpCHgOQHIk1AoAoeD9KiRYI8pBJRESRUEJFHfRykgAo+QAilKkFQQkIy/SOzk8nuzs6d3dngWfe3J9l7797HfL47M/fOvb+7kLCEJSxhCUtYwhKWsIT9IE071wfQMCZNGE4Zr0h14CfJ5/rQGgS/G6W0AJ7k3sDPfOf64BoQH34d/GncC2DBh9IfnAD18L/goR+YAAH4veWT4Dxx3AsE4e8PlStuBVDDj1sBVPHjVAB1/LgUwA1+HArgDj/uBHCLH2cCuMePKwEiwY8jASLDP0cCSFNSSaERjUgx/k5xSL5sePwGFEBakUkmXckkk+Yhs5zmEIc4xEEOcZAdUh57/JgLIMncQB496MKFLovW8C4bKWWLfBc7/BgKIE3IIY+bXIMHWiXbKGUj26XKe/yYCCCtGEg+vWnkabUfkiufeo3v+ZygdGE8w0mx+fg7PmQPe9lHOZWWVxUZtOVS2hr/23BeUNlOzOJXXuN7KoD0YAL5Ic6pb3iNHexlD5+LblP4CLstNWlkkkU2PWlmyZPqPb5nl4D0ZQK9g5IPU0IJb0hlhLUm0Z0ssrmeNI5zvez1Gt8TAaQ/j9A9IPEDSijhHdtv3F0LjWjG13LWe/yoBZCLeJzh9ZJ0XqFQ3nIs2ZhLzFdjyimnnIMpOyf9V6FVz/CjFECG8QQXWRLOspxC2R2+1LSuep6ezzU2bX/BTm2tvkq+bgj8KASQNjzFAEvCaRbxmHwWFr1bzUjy6KBQfRXrWZK5ckjQUlYgPhpP05g1zLBeIjEWQDTuYiZNzYQanmKaHHMQbDq3u5yG369N67LUKkLwty8byAbgLYbJoQYRQDJYSZYlYS+jZGu4EoXpFQ/xp8BuDIAzHOYwlbTnUpJCFv6Iwf6LKtTJL5/Rzkj5ihHySswFkPas5QozWkUR0+VMuBJTh+pPBjz+VLFZK9Hf5Iu6J8AFKWXttA56BzpyC5cGSNBNKu2ufbmdxWZOndlMDD1k9kgA6cEa8yBgJ3fKB+Hy69rU6Uy0JlCirdDXhnvSW5G052buIds8trOZqUOq7W99MoonON8svpFB8k2MBJBBFJtNVTCZvwavt1utqMnpJeRZEjb5xk7ZqdbWI52q72YE6cDzMjz8nV9+zHIyzeh75IpCZ+paAPkzs82b2Jfkh7/uQdrzEl3N6B7Gy8vqrQHMuuD0z/WmBWucOz5JYz4jzegn9PN4Rkh8zOV+M7qfXKcGZrSo3GnenuDv3B3xgFix35c7eNp8DDtGf3nHMwEkmaXcaka3ku80fbUgpayUXxqRakbLE5HBuxv2SF9W0diInKS/bHGuX6FflmSet+C/QLbz7F3ZfBP/uJbbMPgg68nif0akCWvlBucWHM+AgG//McY5P+BMvUefbwQr6Ck7vMOXy8nRDhe8GKZMR9ZzmRE5Ra7TWeAgQAD+GJnjfNjTflrzb/88gza84HkP8Zuxm9bATBkfplwrNtJZVYKwl0AA/mgVfKgpMqdZHvUSH8iiNQAPyk/ClCyjN/uMSGOnCyGMAJJMcT38uSoHPjWbfkbwpYKHPcUn2T+G8BH2q5Cj6hLYCiDJFDPELb6u6UVG8Az3aRFOh9jd+iYfYpmRmj31Zm8ksBFAkurhj1HDh6lDucoIzpOD3uIDMJ6K2oAuDrUoShBSgCB8pWsfwBz3Hz9vRgzwkYPMNoLdp13phQQhBJAkihlqRh9Ux5fL6VYb0v7y0HHv8QGYg/HsWTPKsTYFCYIECMKfjbJpg4zAWX1hjPCRr1ltBG+bc75jjY4SBAggSSyx4I91gw/6QCOwSX1h0x0+AIuM9/STfRRqdZCgngCSxBKLQ/FYecwVwMX8ojaklcQQHzZgTL7p1yrVHFYCiwAB+OPc4QMD/LXprgVw9chTw9tG8DrF2sNIYAogSTxXD3+WWwhz+LnbbQfoeqJ7m/F+tSgu7dlL4LPgDzPzj48AH1oZ7wdijF8nQFpSR+VWbCTwhcSfGQE+XGK8l8UYn2RT4uoWzrnDS+ADSWKxBX9ChPgRCRDZKk9a3XxEhpsDDCWBT5JYbFndmyBFbqoMKcCR2OLD6NMYbjNac7US9hL4WGTBL4gcX1LNyahjiiWiWeMrr33T010f51EeNyONmeRjPXVT2z0lLWIBKjhtBJVOyyiXOA3PI831gFsGM8+MbOFWnxQzwpQgizWRS4CxMqe1jTV+Ybq5OqF8uZn4y8zpmrfIlVM+8EwCQwC9TWzxocp/t0FzJUAAfo6c8neD3kjgX5t1ECD69f1q/4iDZBcCyC3B+HUDIS8kUBLAC/cG8yI7M/ErF/jL65/8tcG6oXDUEmh+ATrYP6Z6492h32gEPneBX/ftbyVXTvo/sT4MRSmBtssIXHAyL3QOb/AXpHCTEVyriD+IZeai2VZy6vADH4ejkmDKdoyNifpvYocPR3uZ3oOrFfGX2+EHT4hEdxYsNd5z5KLAj7xzbdL9805fZb6pgD/Qgv92IH6oKbFoJCg23pO1oQG1eoYvaQw2gi8PqXbMPZAVFvx+gfihJ0UjlkD28W5tSB9SL91Lv74HaGkEV0ePbzctHvlZ4D8HLF2hl/gzmuvjjOCBVg4OUZJfDz8nFL79wkikEiyldnRuOsJ469ZYOdF0zpv8+7DOUJLPShN/Gzl2fkNhVoflNv5huq5t5GannRtGqe7cw4mmk0af9h5frmCX4Uj/TsHV4RbeJI8XLPj97N2mwi6PRyaBpbyn+IXpFdv8845an4JS+5zq+E7L41F1it7ir0iqWG5Ou65zwF+piu/oIhMgQakoz8B4iw97ZpvL7se4K0y7v2OVuVlnuxM+Nu6pFnvjg14HyDeEakN+r5ffKG9ofF3zFTDBiFSSa+ePLlqvQorML3U7fZ1dJtW8xKz3gqPkyvsNiS9pPGtx1Bgpz9rkS2WxJd92+skJ59qVvLelmMHmhNfFbJawa3Le4k9vyxYL1mxb/BZstOTbwI0q+C48ReVa1pguz1Vk2bkehcJfkFJ2m1aWtmXst+7gxcdvKTJHfvAMo6QmZM5OvEp7M7qUO1Rdpt24ynbiNdMBbadco4oPso0eQDkLk+dNVvbpn9pPn0mdM1QNY+08FWQwCy0bNOcyRn2vkjtn6Yt5lVq/jM3SUxm/DXXQZ1nlmztlu0M7jchiDNYL7YQ2rCDk07+0Zj4DzajudlHXrbt8ExYxiP3cKxvU8KEwvWJXwA6AbTzDvpSPJwWtIUkG/cmnH03qJX9MnuwjyETjDxRadq6Uc6f80x1RJDtGmnM8+EoMd+uTa3jOssmizr7lAPv5WDuhX0JrWtOaVkG35ePMyJh3f4gtGdKFhVxvSXibYaI8SRaFACFFcbjzS7I2Ui8w3BzV7Qzzz3s0lLeRpDKOSZbdyTUUMSWSbVOeCKDW8UkqoxgZtMXSzr7kRWaE2oUmGfyR+/iRJamM26VUsV7vBXDX70/rWjOCfDqGafkjSniJraE6PGnHA4ziAkvSWZ6kQK3Pj4kAkQ17CtMrrtKu1juRQQYXkkwZRziilXHE9/7DH9q0dCVjuTVgu/dm7g2/aynGAth0fH1IZ5/8J0pt61ppzwBuIbDjLeNBWRpRhV4JYIMvFBgp61nH6xKRyySAJHEdAxhg2RDlt4PM4W/icmTpsQB2J798ao4XAarZyTrW8Z5UKNfclCvoSl9yQi6172IWyyLbKOuhAGH28b1OqIclnTI+tbyO4SPJ8kqlA53oTCc6m+5WgVbN6zwur3mDHpUAYYc9zXiWnBA/ghGd7aCY5er7AWMqgMI+vvO5gT704Wcut0sHWw27WE1xtDNKHgrgyquzOVn0IVtpy3x9O8t7bGITWyLxO46hAJH1+3IZfehMS1rSkha0tPmJnUoO8JHxete/gh9bc/s06NFsj6TTgpa0pDlnOGm+jorjat85FcDrmd7vg7mZEYpDfDdzgnGJr757PE7xVdcF4hZfSYB4xlfZPR7X+M67x+Mc38k/oCv/im98p7XBOfGOH95FJoVvjF9/ilv88L8fUMWqeMd3+knNkZTQmOVufYMSlrCEJSxhCUtYwhKWsIR97+3/QGhHE2dximMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTctMTEtMDFUMTI6MDA6MDkrMDE6MDDTOj2LAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE3LTExLTAxVDEyOjAwOjA5KzAxOjAwomeFNwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=',
            view: `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfhCggRBQGJXF9dAAAJH0lEQVR42u2ca3BV1RXHf+Qh5MFLEBp5pEBuC6EFHNECSsKMMAMMBBRUcKYztgMMUSqtA3yoNcDQWqrTmcrTqanGqgMOMoqMrR+CA2TKK61Ea4GSAGUEAoUSIAkJQnL74d67s8/JzTn7nLNP0sf+n2/3rrX2/v/PY6+9z9oHDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz+L9Ctk9sbSIRh9CKbLLLIJovbNNJIA4008k+qOUXT/5oAvZnM94gQIUJPV+so56immmPs5wta/5sF6EsBhRQyjhSfEa5RwV72UUVL2ELoxT08QwUtRDUdVynlEVLD6KruK6A3j7KAqY6dvc1VGmigngYaSSObnmSTTU/6Osa+yA62cfA/V4B8VrGA7kn/q+Uo1dRQTTVnO7yge5FHhAh5jOQ+7kpqc4rf8AY39coQHJPYRWuSi/csb7OIiI+IGUyhhD00Jol6mdX062rKbZhFRZJOVvI8wzRET6eQrVxpF7+RDeR2NXUYw752XTtOieI5zyCPb3Zw09hlmMk71NtaamYdmV1Hvi+buGPp0B3e4wEXrz48xet8wl+5Gvdq5TJVfMwW5rrQyWQJJ9vdZPO7gnwKS7hs6UgTWxjh6JPLc5Rz23HAa2I3ixjo2PI8jti8ysnvXPrDOWTpQD3rGODoMYqPPIz7LbzFUMd4Uyi3eNymxHe65RkLuW5p/F3udbTP4be2W0XlaOYVl7xgLqctHnsZFD75LN6wNHqUhx3t01iTZCD7ktcoYTGzuJ8HmcNS1lJqoxPLAH/imKn0oISbkv0VZodLfywnpObqKHZJT+9mj4VQKwdY6TBCjGUNVTYR3nd5MA5lp8X+VaVRxRceo0lq6IDrKDyaUzYq31JqZzyfWvw+Y4iLx9M0SPYHw0mSlkqTm1Z+SZqL/WxuSJ3azwRPrc3gc8n7IhNd7L/NUUsmMlSpFQ9YbenONFf7Ikmum3zfR4sprLDEcMsvuvOq1MdzjNZHPoWtUug/OY7SMXxXytnOMd53y9O5JuKcdxlrAOZID9yrPKSH/l28L9H/iAxXj/6cEfaHyEli0Z08CnhcQcqRUuZXqdD2BP4lXTWzdNDfJdH/ncKiRLo0OzhED9u/mSxgh3hk3WKhgpxtj9JtCj3O5ythf4sinfRfUvL5hXTxW89+OsVcsA1ylQoRR0uP08UK9kM4rkcCmX4ry5V8BonE5Kbt3h9lySESx3alqLPF4/ACWQr2/aTZgm8JrPSLFb1KhY/1yT/Dlj7H8nfVzABeFF4/U7LvLc1WfEngj36+yPn3W36fbpkLnKGMZUz0NJNPpybufYN7wpfAH32kGZ+c9oySBrM7vNTBWp8bnhQxNih6+JbAL/1cKeltQ7p07x/nQV/kAbpRGY/SqDAcBpDAL314TnjJd/azEn3VjifHVBFJ/Wx6lsA/fcQSxQHptywuiovf/9mPoRvn47FKPXh5kiAI/T5isWul9OtTHrMIZ2yOx7rkafVHWYIg9GWq8nw/kUaf8fnos2KaaGOSJz8lCYLRh9fjvl9aYiaS3jIN9CGduni8Fzx6ukoQlD78Me79mvTbcBFzmRYB4JMkrWiQIDh9xFJWifTbRBHVbUFDFYkVyd0+fDuUQAd9uJRkulIkkl5d728SU63PfHknlUAP/XTxalSeg/enuV1iFAzLxDjgD0kk2CFl6St8d2ywiHG/5feFVLJdecrjjnniVKXrkqCIW+KHQ/T2GTZHxAia7jjjiXgrLQHqRVZIp3wH6JEgVcz35oQqwHIS6wJ+USzVMexKZCc6JEis9SwNVYD1qK8kKdPXI8Ff4t5rQxXgrXgrH+qlr0OC3T4mKt6RmHBt1k0/uASJicrpEOlniHX/VfrpB5VgrvAcG5oAc0Qb48KgH0yCTPHqdE1oApTFWzgbFv1gEiSeAlUh0U8VtWIbw6MfRIJFwsv/20AnzBfxp4ZJ378EA8Xri09DoJ/G3+PR6zykwb7o+5egTPjM0C5AsYi9Jnz6fiUYIh6En2uu2WpbXL1IdmfQ9yvBrzTMLJNhk4irOmEPTN8uwRGlyps+4v18C9O10V8senHCtTQnhuU66NslOKFUnLxM2F9jpBb6BXwtYqqUwXUTU6bA9O0SnOc7Ch7bhP1J+gemP0IqyV2vYJ8mJkxa6APMkooR65jsap/Bn4X9qYDFSgUS/d0KD9YssTodW4rT8TYCeEhUdkdp4glX+8HUCvsbAao3F0sX/9/o5Wo/yFJGvVXnSDSac5bQPVzsJ0hXTQsv+ljBy5Ke/FGuuFSiA8y01K+v1kc+hqFS5U2ULxjlYj/eIlkNT3rYn5RGsRj3o0Q5Rp6LRzqvSM/9lnDWpfpxUOpUIz9wsc+xFdVXMlVBhFTmi6Q3dvzBNQfJtbTUxGNh0Ad7NWaUnS4lqd35vcU+ynk2M62DGyKDOZS12x30a5c7OZVi8cYwNlh7Wo/wvm1uNm9KKVET63mZZgf7p/l5uzr+axzmArXUUksqOeSQw6AkVUM1rOIDx948zEbL4sib/IhGz5w8YhB7LefoNHMd7TP4aZIaMbfjEs+6PDrv5V2Lx3WFsktNSKHEtvennCmOHv3ZkHQHYPKjjrUuU54BrLPtITvE8M6iH0O+bddOlCPMc7xfMyiiVLxETX6cZSNTXc78CLZYdi5EucySztsxJGM+Z20ETrLE5U1wCpN4ga18yGG+4jbN/IMD7GQjKxWWOh/gvXZb9Ta57C0KFZmsi78BbjvqeYeZiqmP+kM4QoklE4kd+xjTdeQTyE16f19hK4W+3+HKGMbzokZQPip0lMPrQj9W2zZSJtKlPZQwxVeFYIRFvN3uJosSpZVdHsukOoTO7fOZ/JAfd5Czf81RTsS3z9dwo4MIqeTGt89HuC/pVgu4xXZe5piuTuv/hMZEFvI433C0qaNefELhDlniEwp3O94yLZSznQ+4rr3P2pHKI5RKU+igRwsVPKNYJe4RYX5EJZVxFDKFyfTxGaGVKvaxj/3UhdXJzviMTgpjKCCfCBEGK7RYTzXVVHOYivAv987+kFIGI4gwQHxGKYt08RmlBm5whmrfFWAGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYd49+1hS5Bbj9eKwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNy0xMC0wOFQxNzowNTowMSswMjowMAP/KtMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTctMTAtMDhUMTc6MDU6MDErMDI6MDByopJvAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==`
        };
    }

    /**
     * Returns the minimize and maximize icons base64 data.
     * @type {{minimize: string, maximize: string}}
     * @see https://www.base64-image.de
     * @see https://goo.gl/6Zh5Qo - Down arrow icon
     * @see https://goo.gl/Yxs8do - Up arrow icon
     * @memberof Legend
     */
    static minimizeMaximizeIcons() {
        return {
            maximize: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfhCwINLztn2tkHAAAEeUlEQVR42u2cWWxUVRjHf5PwKIKKC4soIi4gIGsLBQTLLgiKoKhEXMAlRg0xhhjDEmOMMYYYY4zrm+GNFxNifPLBVFQWFUVAEEXFBSgUKLRu1wehttP/aacz98439/L9+tTp3HPO79/pzD3nfKfgOI7jOI7jOI7jOI7jOI7jOI7jOI7jZJ+cYd/dWM5o6qljg3UMFoxkF9Hpr/c4x3o45WYU9S36EREf0d16SOVkDEfa6EdE1HGu9bDKxViOttOPiPj47IigKqAfEbGJHtbDS5pxNAT1IyI+paf1EJNkPMc61I+I+Cy7EUzgeKf6ERGbOc96qEkwUer/IyPYwvnWw42bGzghRE8xi7dkBNu4wHrIcTI5oD8NyPGGjODz7ERwI41C8CRTT/88x+sygi/oZT30OKjlpJBrpLbVc3K8JiP4kguth18q0wL6U/Kel+NVGcF2LrJWKIXpnBJSJ5gsn/2KjOBrLrbWKJaZAf1JwStelhHs4BJrlWKYTZOQOc7EDq9aJyP4Jn0R3CT1jzGh0ytfkhHspLe1UleYS7PUH1/Q1S/KCHbRx1qrUOZJ/QbGFdzCCzKC3fS1ViuE+fwh9au71MrzMoJv6Wet1xm3Sv2jjO1yS8/JCPZwqbViR9zGn2LQRxhTVGvPygj20t9aM8RCqV/P6KJbXCsj+I7LrFUVt/OX1B9VUqurZQT7uNxaN5/FUv8wI0pu+RkZwfcMsFZuzV1S/xDXx9L60zKCH7jCWvsMS/hbDPAgw2PrYaWMYD8DrdUB7gnoD4u1l6dkBD9ypbX+Uqn/O9fF3tOTMoKfGGSpf5/U/40hifS2QkbwM1dZ6T8gF7V/ZXBiPT4hIzjA1Rb6y6X+L1ybaK+PyQiS7lXwUED/msR7flRGkOTrTvCI1C/XS1H3ntQ7j0D/Dsr5ZvSwjCCJzx6B/iss98fRg4EIhibdsX4ftrghWSYjiPsGLA/9SWx1S3q/jCCuGYhA34tZTkrulTdiccxBBfpu3Hpaqmci9YyMuyM9H6uEhYklgQhKW4jJQ8/IK2Vp6m65GlHsSqRAr8lU0uLknTKCYtaiBaukfqUtT98RiKCq1IbXSP1K3KBYJNeku7IfJdAr85W6RaWX5QvdkRTovZlK3qRcEIigppjG9O5cpW9T3yK35jqrShDo/dk0FCrMkxF0VJci0Dv0O1JSrXOz3J4PVSYJdI3GVymq15ojI2hfmyZZJ/XTVq6mS3TaVidKdJ1WGgsWZ8kI/q9PlehKvbSWrM6QZXr/VSgLQrWaaS5anh6IYIbS19W6W1NeuT9Vluo2MTNfX9drZ+HgQq2sVW9idmv9N6V+Vo6uTJERNDPnjP7bUj9Lh5f0gY1m5kJoofuTDOkDTJIRNDII+fLI4gFGfWhrPeLBrB5hrRGnFne2DyDLh5jbH9ysgw/bPJD1Y+zVeUd3V8JA9rd8+/5Z8I8MqjjQ4vsB3XJAdx6nmgY28q716MpCD1ZQQwMbeYfIejCO4ziO4ziO4ziO4ziO4ziO4ziO4zhOOfgXXjmCZ2kdUpcAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTctMTEtMDJUMTM6NDc6NTkrMDE6MDA9gTZjAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE3LTExLTAyVDEzOjQ3OjU5KzAxOjAwTNyO3wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=",
            minimize: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfhCwINKzMNbZQxAAACJElEQVR42u2Qu0oDQRhGPxvBRt9BvPWSUrCx9R1s7AxeY7xrNGq8pPQFFAQhT6APIGhpYe/tERRE12IlELI72SSzO/PPfCft92/mHIAQQgghhBBCCCGEEEIIIYQQQgghxDumUcMDrjBu+iFamcINHnGNiVbDMoL/3zfmTL9aG5W61S/yqmEOP/VpgADzpl+uhWqD0xcG46elhqkbCapNTrPx44umsfQE1QijQvx8JmIeYMG0RcecR/pMxh/04smhBNH6NfXRGD4cSXAW6XGP/laHbiSI0x9Iciw/wWk3+vITdK2vSrBo2q4lJzr05SbQpg8Ao+ISaNUPE7wLSlDRrS8rQSr6chIcp6UvI0Gq+qoES6bNAQBHaevbnSATfXsTHGalb2eCTPXtS1DOWt+uBEb07UlwYEofAEZiEiz7oW8+gXF9swn2bdA3l8Aa/TDBW8YJSjbpZ5/AOn1VghXt/7Rno352CazVzybBrs366SewXj/dBDsS9NNLIEYfAIa1J9iWpK9KUOjoa+L09SbYkqivL4FYfT0JROt3n2BTun53CZzQDxO8dpBgwxV9VYJVP/TbT7Dumn57CZzUT55gzVX9ZAmc1geAIWWCouv6qgRFP/RVCTzRbyeBo/pJEzisnySB4/qtEnigr0rgiX5cAo/0wwQvPuuHCZ7r+nfm9HsMJuhDHjl84haXCAy+gxBCCCGEEEIIIYQQQgghhBBCCCHEbf4A97XzTKLLCrAAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTctMTEtMDJUMTM6NDM6NTErMDE6MDAHhdh+AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE3LTExLTAyVDEzOjQzOjUxKzAxOjAwdthgwgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII="
        };
    }

    /**
     * Returns the main legend div.
     * @returns {HTMLDivElement} - the main legend div.
     * @memberof Legend
     */
    getLegendDiv() {
        return this.legendDiv;
    }

    /**
     * Create a horizontal legend for a map variable that uses color.
     * @see https://css-tricks.com/things-ive-learned-css-grid-layout/#article-header-id-1
     * @see https://stackoverflow.com/a/29346018/ -> Text overflow
     * @memberof Legend
     */
    verticalColorLegend() {
        let isCategorical = this._isCategoricalMapVariable();
        let classCatToColorMap = this.mapVariable.getVisualVariableMapping();
        /*  let classesCategories;
         if (isCategorical)
             classesCategories = this.mapVariable.getCategories();
         else
             classesCategories = this.mapVariable.getClassIntervals();
 
         console.log(colorsMapVariable, this.mapVariable, classesCategories); */

        // console.log(colorsMapVariable); // [255, 0, 0], [0, 0, 255]
        // console.log(classesCategories); // [0-1.922], [1.922, 196.4]
        // --------------------------------- CATEGORIES
        /*  colorsMapVariable = [[166, 206, 227],
         [31, 120, 180],
         [178, 223, 138],
         [51, 160, 44],
         [251, 154, 153],
         [227, 26, 28],
         [253, 191, 111],
         [255, 127, 0],
         [202, 178, 214],
         [106, 61, 154],
         [255, 255, 153],
         [177, 89, 40]]; */
        //  isCategorical = true;
        // colorsMapVariable = [[127, 201, 127], [190, 174, 212], [253, 192, 134], [255, 255, 153], [56, 108, 176], [240, 2, 127], [191, 91, 23], [102, 102, 102], [102, 102, 102], [102, 102, 102], [102, 102, 102], [102, 102, 102]];
        // classesCategories = ["Category1", "Category2", "Category3", "Category4", "Category5", "Category6", "Category7", "Category8", "Category9", "Category10", "Category11", "Category12"];
        // --------------------------------- CONTINUOUS
        // colorsMapVariable = [[255, 0, 0], [0, 255, 0], [0, 0, 255], [147, 0, 0], [147, 222, 0], [147, 222, 148], [147, 112, 234], [242, 112, 234]];
        // classesCategories = [[0, 1.942], [1.942, 18.29], [18.29, 194.54], [18.29, 194.54], [18.29, 194.54], [18.29, 194.54], [18.29, 194.54], [18.29, 194.54]];

        let legendDiv = this._createVerticalLegendDiv(Array.isArray(classCatToColorMap) ? classCatToColorMap.length : classCatToColorMap.size);
        legendDiv.appendChild(this._createTitle());
        this._addMouseEvents(legendDiv);

        // for (let i = 0; i < classesCategories.length/* numColors */; i++) {
        let i = 0;
        if (Array.isArray(classCatToColorMap)) {
            for (let color of classCatToColorMap) {
                let rowDiv = this._createLegendRow();
                // console.log(this.mapVariable.getClassIntervals()[i]);

                let visibilityIconDiv = this._createVisibilityIcon(i++);
                let colorDiv = document.createElement('div');
                colorDiv.style.backgroundColor = `rgb(${color.toString()})`;
                let classCategoryStringSpan = this._createClassOrCategorySpan(isCategorical, this.mapVariable.getClassIntervals()[i - 1]);

                rowDiv.appendChild(visibilityIconDiv);
                rowDiv.appendChild(colorDiv);
                rowDiv.appendChild(classCategoryStringSpan);

                legendDiv.appendChild(rowDiv);
            }
        } else {
            for (let [classCat, color] of classCatToColorMap) {
                // console.warn(classCat, color, this.mapVariable);
                let rowDiv = this._createLegendRow();

                let visibilityIconDiv = this._createVisibilityIcon(i++);
                let colorDiv = document.createElement('div');
                colorDiv.style.backgroundColor = `rgb(${color.toString()})`;
                let classCategoryStringSpan = this._createClassOrCategorySpan(isCategorical, classCat);

                rowDiv.appendChild(visibilityIconDiv);
                rowDiv.appendChild(colorDiv);
                rowDiv.appendChild(classCategoryStringSpan);

                legendDiv.appendChild(rowDiv);
            }
        }

        this.legendDiv = legendDiv;
        this.parentLegendsDiv.appendChild(legendDiv);
    }

    /**
     * Creates a vertical color legend.
     * @param {Array<Array<number>>} classes - The classes intervals.
     * @param {Array<number>} percentages - The percentages for each legend column.
     * @see https://stackoverflow.com/a/1144788 - Replace all commas in a string with space.
     * @memberof Legend
     */
    horizontalColorLegend(classes, percentages) {
        let colorsMapVariable = this.mapVariable.getVisualVariableMapping();
        let numColors = colorsMapVariable.length;
        let legendDiv = this._createHorizontalLegendDiv();
        legendDiv.appendChild(this._createTitle());
        this._addMouseEvents(legendDiv);

        let fullDiv = document.createElement('div');
        fullDiv.style.display = 'grid';
        percentages = percentages.map((p) => p + '%').toString().replace(/,/g, ' '); //Percentages to string
        fullDiv.style['grid-template-columns'] = percentages;

        //for over all colors
        for (let i = 0; i < numColors; i++) {
            let columnDiv = this._createLegendColumn(i);

            let visibilityIconDiv = this._createVisibilityIcon(i);
            let colorDiv = document.createElement('div');
            colorDiv.style.backgroundColor = `rgb(${colorsMapVariable[i].toString()})`;
            columnDiv.appendChild(visibilityIconDiv);
            columnDiv.appendChild(colorDiv);
            if (i === 0) {
                let numberSpan = document.createElement('div');
                numberSpan.innerHTML = classes[i][0].toFixed(2);
                columnDiv.appendChild(numberSpan);
            } else if (i === numColors - 1) {
                let numberSpan2 = document.createElement('div');
                numberSpan2.style.textAlign = 'right';
                numberSpan2.style.overflow = 'hidden';
                numberSpan2.style.display = 'inline-block';
                numberSpan2.style.textOverflow = 'ellipsis';
                numberSpan2.style.whiteSpace = 'nowrap';
                numberSpan2.innerHTML = classes[i][1].toFixed(2);
                numberSpan2.title = classes[i][1].toFixed(2);
                columnDiv.appendChild(numberSpan2);
            }

            fullDiv.appendChild(columnDiv);
        }

        legendDiv.appendChild(fullDiv);
        this.legendDiv = legendDiv;
        this.parentLegendsDiv.appendChild(legendDiv);
    }

    /**
     * Create the shape legend. 
     * Uses CSS image sprites to deal with image splitting
     * @param {HTMLImageElement} shapesImage - The image with the shapes.
     * @param {number} imageSize - The size of the shapes image.
     * @see https://css-tricks.com/css-sprites/ -- CSS Sprites
     * @see https://tableless.com.br/css-sprites/
     * @see https://stackoverflow.com/a/20933311 --Filter image
     * @see https://stackoverflow.com/a/17219553/ --Zoom
     * @see https://stackoverflow.com/a/23703655 --Center image vertically and horizontally
     * @memberof Legend
     */
    shapeLegend(shapesImage, imageSize = 128) {
        // console.log(imageSize);
        let spritePositions = this._createSpritePositions(imageSize);
        let categories = Array.from(this.mapVariable.getVisualVariableMapping().keys());//this.mapVariable.getValues();
        let numCategories = categories.length;
        let legendDiv = this._createVerticalLegendDiv(numCategories);
        legendDiv.appendChild(this._createTitle());
        this._addMouseEvents(legendDiv);

        for (let i = 0; i < numCategories; i++) {
            let rowDiv = this._createLegendRow();

            let visibilityIconDiv = this._createVisibilityIcon(i);
            let imgDiv = this._createImageDiv(shapesImage, imageSize, spritePositions[i]);
            let categoryStringSpan = this._createClassOrCategorySpan(true, categories[i]);

            rowDiv.appendChild(visibilityIconDiv);
            rowDiv.appendChild(imgDiv);
            rowDiv.appendChild(categoryStringSpan);

            legendDiv.appendChild(rowDiv);
        }

        this.legendDiv = legendDiv;
        this.parentLegendsDiv.appendChild(legendDiv);
    }

    /**
     * Create the legend for a variable that has size visual variable.
     * Uses CSS image sprites to deal with image splitting.
     * @param {HTMLImageElement} shapesImage - The image with the shapes.
     * @param {number} imageSize - The size of the image.
     * @memberof Legend
     */
    sizeLegend(shapesImage, imageSize = 128) {
        let spritePosition = this._convertShapeIndexToSprite(GisplayDefaults.getDefaultShapeIndex());
        let sizes = this.mapVariable.getVisualVariableMapping(); // [15, 40, 90] pixeis
        let classes = this.mapVariable.getClassIntervals();
        let numClasses = classes.length;
        let template_rows = '25px ';
        for (const size of sizes)
            template_rows += ' ' + (size < 20 ? 20 : size) + 'px '; //If < 20px then use 20px otherwise use the given value

        let legendDiv = this._createVerticalLegendDiv(numClasses, template_rows, '300px');
        this._addMouseEvents(legendDiv);
        legendDiv.appendChild(this._createTitle());

        for (let i = 0; i < numClasses; i++) {
            let rowDiv = this._createLegendRow('0.4fr 1fr 1fr');

            let visibilityIconDiv = this._createVisibilityIcon(i);
            let zoom = (sizes[i] / imageSize);
            let imgDiv = this._createImageDiv(shapesImage, imageSize, spritePosition, zoom);
            let classStringSpan = this._createClassOrCategorySpan(false, classes[i]);

            rowDiv.appendChild(visibilityIconDiv);
            rowDiv.appendChild(imgDiv);
            rowDiv.appendChild(classStringSpan);

            legendDiv.appendChild(rowDiv);
        }

        this.legendDiv = legendDiv;
        this.parentLegendsDiv.appendChild(legendDiv);
    }

    /**
     * Create the legend for a variable that has orientation visual variable.
     * @param {any} figuresImage - The image that contains the figures.
     * @param {number} [imageSize=128] - The size in pixels of each figure inside the figures image.
     * @memberof Legend
     */
    orientationLegend(figuresImage, imageSize = 128) {
        let spritePosition = this._convertShapeIndexToSprite(GisplayDefaults.getDefaultFigureIndex(), imageSize);
        console.log(spritePosition);
        let isCategorical = this._isCategoricalMapVariable();
        let orientationValues = [];

        let classesCategories;
        if (isCategorical) {
            let vvMapping = this.mapVariable.getVisualVariableMapping();
            classesCategories = Array.from(vvMapping.keys());
            // classesCategories = this.mapVariable.getValues();
            // let vvMapping = this.mapVariable.getVisualVariableMapping();
            for (let category of classesCategories)
                orientationValues.push(vvMapping.get(category));
        }
        else {
            //@TODO fix here 
            classesCategories = this.mapVariable.getClassIntervals();
            orientationValues = this.mapVariable.getVisualVariableMapping();
        }

        let numOrientations = orientationValues.length;
        let legendDiv = this._createVerticalLegendDiv(numOrientations);
        legendDiv.appendChild(this._createTitle());
        this._addMouseEvents(legendDiv);

        console.log(orientationValues, classesCategories, numOrientations);

        for (let i = 0; i < classesCategories.length/* numColors */; i++) {
            let rowDiv = this._createLegendRow();

            let visibilityIconDiv = this._createVisibilityIcon(i);
            let orientationFigShape = this._createImageDivRotated(figuresImage, imageSize, spritePosition, orientationValues[i]);
            let classCategoryStringSpan = this._createClassOrCategorySpan(isCategorical, classesCategories[i]);

            rowDiv.appendChild(visibilityIconDiv);
            rowDiv.appendChild(orientationFigShape);
            rowDiv.appendChild(classCategoryStringSpan);

            legendDiv.appendChild(rowDiv);
        }

        this.legendDiv = legendDiv;
        this.parentLegendsDiv.appendChild(legendDiv);
    }

    /**
     * Creates a column legend. Used by horizontal color legend.
     * @returns {HTMLDivElement} - the column legend div.
     * @private
     * @memberof Legend
     */
    _createLegendColumn() {
        let columnDiv = document.createElement('div'); //The div that will contain the visibility layer, the VV and the number or category
        columnDiv.style.display = 'grid';
        columnDiv.style['grid-template-rows'] = '1fr 1fr 1fr';
        return columnDiv;
    }

    /**
     * Add mouse enter and mouse leave events to the given legend div.
     * @param {HTMLDivElement} legendDiv - The legend div.
     * @private
     * @memberof Legend
     */
    _addMouseEvents(legendDiv) {
        legendDiv.onmouseenter = (e) => {
            legendDiv.style.outline = '1px solid';
            legendDiv.style.backgroundColor = 'white';
        };
        legendDiv.onmouseleave = (e) => {
            legendDiv.style.outline = 'none';
            legendDiv.style.backgroundColor = this.defaultBackgroundColor;
            // legendDiv.style.backgroundColor = 'white';
        };
    }

    /**
     * Create the vertical legend div that contains a title and multiple categories.
     * @param {number} numCategories - The number of categories. 
     * @param {string} template_rows - The template rows for the grid-template-rows style. 
     * @param {number} maxWidth - The max with for the vertical legend div.
     * @returns {HTMLDivElement} - the legend div.
     * @private
     * @memberof Legend
     */
    _createVerticalLegendDiv(numCategories, template_rows, maxWidth) {
        let legendDiv = document.createElement('div');
        legendDiv.id = this.legendId;
        legendDiv.style.display = 'grid';
        legendDiv.style['grid-template-rows'] = template_rows ? template_rows : '25px ' + '1fr '.repeat(numCategories) + ' 4px'; // 4px is for spacing after last element (should be changed @TODO)
        legendDiv.style['grid-row-gap'] = numCategories <= 3 ? '5px' : numCategories > 3 && numCategories < 6 ? '3px' : '1px';
        legendDiv.style.minWidth = '200px';
        legendDiv.style.maxWidth = maxWidth ? maxWidth : '300px';
        legendDiv.style.backgroundColor = this.defaultBackgroundColor;
        return legendDiv;
    }

    /**
     * Creates the horizontal legend div.
     * @returns {HTMLDivElement} - the horizontal legend div.
     * @private
     * @memberof Legend
     */
    _createHorizontalLegendDiv() {
        let legendDiv = document.createElement('div');
        legendDiv.id = this.legendId;
        legendDiv.style.display = 'grid';
        legendDiv.style['grid-template-rows'] = '25px ' + '1fr';
        legendDiv.style.minWidth = '250px';//
        legendDiv.style.maxWidth = '300px';//
        legendDiv.style.minHeight = '100px';
        legendDiv.style.backgroundColor = this.defaultBackgroundColor;
        return legendDiv;
    }

    /**
     * Creates a div with the title of the Legend and the minimize/maximize icon. 
     * Creates the minimize icon if it's the main legend, otherwise uses the maximize icon. 
     * @param {boolean} [useMinimizeIcon=true] - If the icon to use is the minimize icon.
     * @returns {HTMLDivElement} - a div with the title of the Legend and the minimize/maximize icon. 
     * @see https://stackoverflow.com/a/29346018/ - Text overflow
     * @private
     * @memberof Legend
     */
    _createTitle(useMinimizeIcon = true) {
        let titleDiv = document.createElement('div');
        titleDiv.style.display = 'grid';
        titleDiv.style["grid-template-columns"] = '90% 10%';

        let textDiv = document.createElement('div');
        textDiv.id = this.legendId + '_title';
        textDiv.style.display = 'inline-grid';
        textDiv.style.alignItems = 'center'; // Center vertically
        textDiv.style.textAlign = 'center'; //  Center horizontally
        textDiv.innerHTML = this.legendTitle;
        //Text overflow
        textDiv.style.overflow = 'hidden';
        textDiv.style.display = 'inline-block';
        textDiv.style.textOverflow = 'ellipsis';
        textDiv.style.whiteSpace = 'nowrap';
        textDiv.title = this.legendTitle;

        titleDiv.appendChild(textDiv);
        titleDiv.appendChild(useMinimizeIcon ? this._createMinimizeIcon() : this._createMaximizeIcon());

        return titleDiv;
    }

    /**
     * Creates a row for the vertical legend.
     * @param {string} template_columns - The template columns for the grid-template-columns style. 
     * @returns {HTMLDivElement} - the div with element that will contain the class or category information.
     * @private
     * @memberof Legend
     */
    _createLegendRow(template_columns) {
        let rowDiv = document.createElement('div'); //The div that will contain the visibility layer, the VV and the number or category
        rowDiv.style.display = 'grid';
        rowDiv.style['grid-template-columns'] = template_columns ? template_columns : '1fr 1fr 2.5fr';//'1fr 1fr 1fr';
        rowDiv.style['grid-column-gap'] = '5px'; //?
        return rowDiv;
    }

    /**
     * Creates the visibility icon.
     * @param {number} index - The index of the legend row.
     * @returns {HTMLDivElement} - the visibility icon div.
     * @private
     * @memberof Legend
     */
    _createVisibilityIcon(index) {
        let visibilityIconDiv = document.createElement('div');
        visibilityIconDiv.style.position = 'relative';
        visibilityIconDiv.style.cursor = 'pointer';

        let imageDiv = document.createElement('div');
        imageDiv.style.width = '128px';
        imageDiv.style.height = '128px';
        imageDiv.style.backgroundImage = `url(${Legend.visibilityIcons().view})`;
        imageDiv.style.backgroundPosition = '0px 0px';
        imageDiv.style.zoom = '.2';
        //Center image vert and horiz
        imageDiv.style.position = 'relative';
        imageDiv.style.left = '50%';
        imageDiv.style.top = '50%';
        imageDiv.style.transform = 'translate(-50%, -50%)';
        //Status and click information
        imageDiv.active = true;
        imageDiv.index = index; //Save the index to later send in the event

        this.visibilityIconDivs.push(imageDiv); //Add this visibility icon to the list of visibility icons

        visibilityIconDiv.onclick = () => {
            this._changeVisibilityIcon(imageDiv);
        };

        visibilityIconDiv.appendChild(imageDiv);
        return visibilityIconDiv;
    }

    /**
     * Creates a div and adds the image (from img) at sprite position to it.
     * @param {HTMLImageElement} img - The image with figures, shapes or patterns. 
     * @param {number} imageSize - The size of the image
     * @param {Array<string>} spritePosition - The sprite positions of the given image.
     * @param {zoom} zoom - The zoom value, if given.
     * @returns {HTMLDivElement} - the div with the image at sprite position.
     * @see https://stackoverflow.com/a/20933311 - Filter image color
     * @see https://stackoverflow.com/a/17219553/ - Zoom
     * @see https://stackoverflow.com/a/7688121/5869289 - Background position and image
     * @private
     * @memberof Legend
     */
    _createImageDiv(img, imageSize, spritePosition, zoom) {
        //Image Main Div and Div itself
        let imgDiv = document.createElement('div');
        imgDiv.style.position = 'relative';
        let image = document.createElement('div');
        image.style.width = imageSize + 'px';
        image.style.height = imageSize + 'px';
        image.style.backgroundImage = `url(${img.src})`;
        image.style.backgroundPosition = spritePosition[0] + ' ' + spritePosition[1];
        image.style.filter = 'invert(100%)';
        image.style.zoom = zoom ? zoom : imageSize > 32 ? '.3' : '1';
        //Center image vert and horiz
        image.style.position = 'relative';
        image.style.left = '50%';
        image.style.top = '50%';
        image.style.transform = 'translate(-50%, -50%)';
        imgDiv.appendChild(image);
        return imgDiv;
    }

    /**
     * Creates the span for the category or class.
     * @param {boolean} isCategory - Is category or  not.
     * @param {string|Array<number>} classOrCategory - The  given category or class (start and end values).
     * @returns {HTMLSpanElement} - a span with the category if is a category, otherwise a span with the class start and end points.
     * @private
     * @memberof Legend
     */
    _createClassOrCategorySpan(isCategory, classOrCategory) {
        let stringToShow = isCategory ? classOrCategory : (classOrCategory[0].toFixed(2) + ' - ' + classOrCategory[1].toFixed(2));
        let categoryString = document.createElement('span');
        categoryString.style.alignItems = 'center'; //Center vertically
        // categoryString.style.textAlign = 'center';//Center horizontally
        categoryString.innerHTML = stringToShow;

        //Overflow
        categoryString.style.overflow = 'hidden';
        categoryString.style.display = 'grid';//'inline-block';
        categoryString.style.textOverflow = 'ellipsis';
        categoryString.style.whiteSpace = 'nowrap';
        categoryString.title = stringToShow; // If overflow then appears on hover
        return categoryString;
    }

    /**
     * Convert all shapes, patterns or figures indices to their respective sprite position.
     * @param {number} pixelsPerShape - The number of pixels each shape, pattern or figure has.
     * @returns {Array<Array<string>>} - all shapes, patterns or figures indices to their respective sprite position.
     * @private
     * @memberof Legend
     */
    _createSpritePositions(pixelsPerShape) {
        let spritePositions = [];
        console.log(this.mapVariable);
        let categories = this.mapVariable.getVisualVariableMapping();//this.mapVariable.getValues(); // ["Y", "N"]
        let valueToIndexMap = this.mapVariable.getValueIndexMapping();// Map(2) {"Y" => 0, "N" => 1}
        let indexToShapeMap = this.mapVariable.getIndexToUSableValueMap(); // {0 => 6, 1 => 9}
        for (const [category, mapping] of categories) {
            let categoryIndex = valueToIndexMap.get(category);
            let shapeIndex = indexToShapeMap.get(categoryIndex);
            let shapeImageSprite = this._convertShapeIndexToSprite(shapeIndex, pixelsPerShape);
            spritePositions.push(shapeImageSprite);
            // console.log(categoryIndex, shapeIndex, shapeImageSprite);
        }
        return spritePositions;
    }

    /**
     * Converts the given index to it's sprite location.
     * @param {number} index - Index of the shape, pattern or figure in the image.
     * @param {number} [pixelsPerShape=128] - Number of pixels for the width and height of each image.
     * @param {number} [nCols=4] - Number of columns in the image.
     * @param {number} [nRows=4] - Number of rows in the image
     * @returns {Array<string>} - The CSS sprite (x and y) for the given index.
     * @see https://css-tricks.com/css-sprites/
     * @see https://tableless.com.br/css-sprites/
     * @private
     * @memberof Legend
     */
    _convertShapeIndexToSprite(index, pixelsPerShape = 128, nCols = 4, nRows = 4) {
        let column = index % nCols;
        let row = Math.floor(index / nRows);
        let xFinal = - (column * pixelsPerShape) + 'px';
        let yFinal = - (row * pixelsPerShape) + 'px';
        return [xFinal, yFinal];
    }

    /**
     * Creates a div with the given image rotated by orientation degrees.
     * @param {HTMLDivElement} img - The image with figures.
     * @param {number} imageSize - The size of the image.
     * @param {Array<string>} spritePosition - The sprite position inside the given image.
     * @param {number} orientation - The orientation of the image sprite.
     * @returns {HTMLDivElement} - a div with the given image rotated by orientation degrees.
     * @see https://stackoverflow.com/a/8939295/ - Rotate image around center
     * @private
     * @memberof Legend
     */
    _createImageDivRotated(img, imageSize, spritePosition, orientation) {
        console.warn(orientation);
        let imgDiv = document.createElement('div');
        imgDiv.style.position = 'relative';
        //<span class="share-link" style="position: relative; width: 128px;height: 128px; zoom: .3; filter: invert(100%); background: url(http://127.0.0.1:8080/dd5d4e1929581121a20b319d4dd7c269.png) -128px 0px;"
        let image = document.createElement('span'); //Span to hold the image
        image.style.width = imageSize + 'px';
        image.style.height = imageSize + 'px';
        image.style.backgroundImage = `url(${img.src})`;
        image.style.backgroundPosition = spritePosition[0] + ' ' + spritePosition[1];
        image.style.filter = 'invert(100%)';
        image.style.zoom = imageSize > 32 ? '.3' : '1';
        //Center image vert and horiz
        image.style.position = 'relative';
        image.style.left = '50%';
        image.style.top = '50%';
        image.style.transform = `translate(-50%, -50%) rotate(${orientation}deg)`;

        //To rotate around itself
        image.style.display = 'block';

        imgDiv.appendChild(image);

        return imgDiv;
    }

    /**
     * Change the visibility icon.
     * @param {HTMLDivElement} imageDiv - The div that contains the icon.
     * @private
     * @memberof Legend
     */
    _changeVisibilityIcon(imageDiv) {
        let icons = Legend.visibilityIcons();
        imageDiv.style.backgroundImage = `url(${imageDiv.active ? icons.hide : icons.view})`; //Change icon
        imageDiv.active = !imageDiv.active; //Change status
        this.dispatchLegendChangedEvent(imageDiv.index);
    }

    /**
     * Verifies if the map variable is categorical or continuos.
     * @returns {boolean} - true if the map variable is an instance of CategoricalVariable, false, otherwise.
     * @private 
     * @memberof Legend
     */
    _isCategoricalMapVariable() {
        return this.mapVariable instanceof CategoricalVariable;
    }

    /**
     * Creates the minimized Legend.
     * @returns {HTMLDivElement} - the minimized Legend. 
     * @private
     * @see https://codepen.io/anon/pen/Xzmvvz
     * @memberof Legend
     */
    _createMinimizedLegend() {
        let minimizedLegendDiv = document.createElement('div');
        minimizedLegendDiv.id = this.legendId + "_minimized";
        minimizedLegendDiv.style.display = 'none';
        minimizedLegendDiv.style.height = '25px';
        // minimizedLegendDiv.style.marginBottom = '5px'; //Space between elements

        minimizedLegendDiv.appendChild(this._createTitle(false));
        this._addMouseEvents(minimizedLegendDiv);
        this.parentLegendsDiv.appendChild(minimizedLegendDiv);
        return minimizedLegendDiv;
    }

    /**
     * Returns the minimize icon in an HTMLImageElement.
     * @returns {HTMLImageElement} - the minimize icon in an HTMLImageElement.
     * @private
     * @memberof Legend
     */
    _createMinimizeIcon() {
        let stateIcon = document.createElement('img');
        stateIcon.src = Legend.minimizeMaximizeIcons().minimize;
        stateIcon.style.height = '30px';
        stateIcon.style.right = '0px';
        stateIcon.style.position = 'absolute';
        stateIcon.title = 'Minimize';
        stateIcon.style.cursor = 'pointer';
        stateIcon.onclick = () => { this._changeLegendState(true); };
        return stateIcon;
    }

    /**
     * Returns the maximize icon in an HTMLImageElement.
     * @returns {HTMLImageElement} - the maximize icon in an HTMLImageElement.
     * @private
     * @memberof Legend
     */
    _createMaximizeIcon() {
        let stateIcon = document.createElement('img');
        stateIcon.src = Legend.minimizeMaximizeIcons().maximize;
        stateIcon.style.height = '30px';
        stateIcon.style.right = '0px';
        stateIcon.style.position = 'absolute';
        stateIcon.title = 'Maximize';
        stateIcon.style.cursor = 'pointer';
        stateIcon.onclick = () => { this._changeLegendState(false); };
        return stateIcon;
    }

    /**
     * Change the legend state. If it's maximized then minimize, otherwise, maximize.
     * To minimize, it sets the main legend div display to none and shows the minimized legend.
     * To maximize, it sets the minimized legend div display to none and shows the main legend.
     * @param {boolean} isMaximized - If the legend is maximized or not.
     * @private
     * @memberof Legend
     */
    _changeLegendState(isMaximized) {
        if (isMaximized) {
            this.minimizedLegend.style.width = this.legendDiv.offsetWidth + 'px'; //Same width as main Legend
            this.legendDiv.style.display = 'none';
            this.minimizedLegend.style.display = 'grid';
        }
        else {
            this.legendDiv.style.display = 'grid';
            this.minimizedLegend.style.display = 'none';
        }
    }

    /**
     * Returns the state of each visibility icon of this Legend. 
     * @returns {Array<boolean>} - the state of each visibility icon of this Legend. 
     * @memberof Legend
     */
    getLegendVisibilityState() {
        return this.visibilityIconDivs.map((visDiv) => visDiv.active);
    }

    /**
     * When the legend state changes then it fires the LegendChangedEvent and sends information about the index that was changed and
     * the external name of the variable that is mapped by this Legend.
     * @param {number} index - The index of the clicked element.
     * @memberof Legend
     * @see https://stackoverflow.com/a/23725973
     * @see https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events
     */
    dispatchLegendChangedEvent(index) {
        // console.log(this.getLegendVisibilityState());
        // console.log(this._isCategoricalMapVariable());
        this.legendChangedEvent = new CustomEvent('LegendChangedEvent', {
            'detail': {
                index,
                externalName: this.mapVariable.getExternalName(),
                isCategorical: this._isCategoricalMapVariable()
            }
        });
        document.dispatchEvent(this.legendChangedEvent);
    }
}