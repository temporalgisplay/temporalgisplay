//Import for intellisense
import { TimeVariable } from '../VVs/TimeVariable';
import { GisplayError } from '../GisplayError';

import { GisplayDefaults } from '../GisplayDefaults';

const noUiSlider = require('../../lib/noUiSlider/nouislider');

/**
 * This class represents a time control element.
 * @export
 * @class TimeControl
 * @see https://github.com/IonDen/ion.rangeSlider
 * @see http://ionden.com/a/plugins/ion.rangeSlider/demo_interactions.html
 * @see http://seiyria.com/bootstrap-slider/
 * @see http://rangeslider.js.org/
 * @see https://refreshless.com/nouislider/examples/
 */
export class TimeControl {

    /**
     * Creates an instance of TimeControl.
     * @param {TimeVariable} timeVariable - The time variable.
     * @param {HTMLDivElement} timeControlDiv - The time control div element.
     * @memberof TimeControl
     */
    constructor(timeVariable, timeControlDiv) {
        /**
         * The time variable.
         * @type {TimeVariable}
         */
        this.timeVariable = timeVariable;
        /**
         * The time control div.
         * @type {HTMLDivElement}
         */
        this.timeControlDiv = timeControlDiv;
        /**
         * The id of the DOM div that will contain the time control.
         * @type {string}
         */
        this.timeControlId = timeControlDiv.id;
        /**
         * The title of the time control.
         * @type {string}
         */
        this.timeTitle = timeVariable.getInternalName();
        /**
         * The div that contains the timeline.
         * @type {HTMLDivElement}
         */
        this.timeLine = null;
        /**
         * This map holds the temporal granules and their respective index.
         * @type {Map<number, number>}
         */
        this.temporalGranuleToIndexMap = timeVariable.getTemporalGranules();
        /**
         * The event that is triggered when the range of time granules changes.
         * @type {Event}
         */
        this.rangeChangedEvent = new CustomEvent('TimeRangeChanged');

        /**
         * The array that holds the selected range. The range can be one if it's for instants or two elements if it's for intervals.
         * @type {Array<number>|number}
         */
        this.selectedRange;
        /**
         * Has instants or not.
         * @type {boolean}
         */
        this.hasInstantsBool = this.timeVariable.getTypeOfTimeControl() === GisplayDefaults.INSTANT();
        /**
         * Array with as many indices as the number of temporal granules.
         * @type {Array<number>}
         */
        this.timelineIndices = [...Array(this.timeVariable.sortTemporalGranules().length).keys()];
        // console.warn(this.timelineIndices);

        this.buildTimeControl();
        this.addKeyboardEvents();
    }

    /**
     * Method that builds the time control in the DOM.
     * @memberof TimeControl
     */
    buildTimeControl() {
        // console.warn(this.timeVariable.getTypeOfTimeControl());
        //Add CSS to build the time control
        switch (this.timeVariable.getTypeOfTimeControl()) {
            case GisplayDefaults.INSTANT():
                this._buildInstants();
                break;
            case GisplayDefaults.INTERVAL():
                this._buildIntervals();
                break;
            case GisplayDefaults.ANIMATION():
                break;
        }
    }

    /**
     * Build the instant controller.
     * @memberof TimeControl
     */
    _buildInstants() {
        let lastIndex = this.timelineIndices.length - 1;
        this.timeControlDiv.style.display = 'grid';
        this.timeControlDiv.style['grid-template-rows'] = lastIndex === 0 ? '1fr 1fr' : '25px 1fr';

        //                  TITLE
        let title = this._createTitleDiv();
        //                  TIMELINE (NoUiSlider)
        let timeline = document.createElement('div');
        timeline.id = this.timeControlId + '_timeline';
        if (lastIndex === 0) { // No need to NoUiSlider
            timeline.innerHTML = this.timeVariable.getSortedTemporalGranules()[0];
            timeline.style.textAlign = 'center';
            this.selectedRange = this.timeVariable.getSortedTemporalGranules()[0];
        } else { // Create NoUiSlider
            this._createNoUiSlider(timeline);
            this._addClickEventToPips(timeline);
            this._listenToTimeLineEvents(timeline);
        }
        this.timeControlDiv.appendChild(title);
        this.timeControlDiv.appendChild(timeline);
        this.timeLine = timeline;

        if (lastIndex > 0)
            this._setPipsValues(timeline);
        // this._addPips(timeline);
        // this._setPipsValues(timeline);
    }

    //Add pips to the 
    /*     _addPips(timeline) {
            let filterPipValues = (value, type) => {
                console.log(value, type);
                if (value === 0 || value === this.sortedTemporalGranules.length - 1 ||
                    value === Math.floor(this.sortedTemporalGranules.length / 2))
                    return 1;
                else
                    return 0;
            };
            timeline.noUiSlider.pips({
                mode: 'steps',
                // density: 3,
                filter: filterPipValues
            });
        } */

    //Set  the values of each pip to it's real value
    _setPipsValues(timeline) {
        window.no = timeline.noUiSlider;
        //Pipts to show should take in consideration:
        //1. type of pips (number or sting(date))
        //2. Number of pips
        //
        let sortedTGs = this.timeVariable.getSortedTemporalGranules();
        let pipsAreNumbers = +sortedTGs[0] === +sortedTGs[0];
        let pipsToShow;
        if (pipsAreNumbers && sortedTGs.length < 40) { //Numbers
            pipsToShow = sortedTGs;
        } else if (sortedTGs.length < 10) { //Less than 10 strings
            pipsToShow = this.timelineIndices;
        }
        else { //Strings but > than 10
            pipsToShow = [0, Math.floor(this.timelineIndices.length / 2), this.timelineIndices.length - 1];
        }

        let filterPipValues = (value, type) => {
            // console.log(value, type);
            if (pipsAreNumbers)
                return 1;
            if (pipsToShow.indexOf(value) > -1)
                return 1;
            else
                return 0;
        };
        timeline.noUiSlider.pips({
            mode: 'steps',
            density: 20,
            filter: filterPipValues
        });

        // let sortedTGs = this.timeVariable.getSortedTemporalGranules();
        let nodes = document.querySelectorAll('.noUi-value');
        // console.log(nodes);
        // console.error(sortedTGs);
        // console.warn(pipsToShow);
        for (let i = 0; i < nodes.length; i++) {
            // console.log(i, pipsToShow.indexOf(i));
            if (pipsAreNumbers)
                nodes[i].innerHTML = sortedTGs[i];
            else
                nodes[i].innerHTML = sortedTGs[pipsToShow[i]];
        }

        this._addClickEventToPips(timeline);
        // nodes.forEach((el) => { el.innerHTML = sortedTGs[i++]; });

        /*  let sortedTGs = this.timeVariable.getSortedTemporalGranules();
         let pips = document.querySelector('.noUi-pips');
         console.log(pips);
         let i = 0;
         pips.forEach((el) => { el.innerHTML = sortedTGs[i++]; }); */
    }

    /**
     * Adds click event to the pips values.
     * @param {HTMLDivElement} timeline - The timeline div element. 
     * @see https://stackoverflow.com/a/46648942/
     * @memberof TimeControl
     */
    _addClickEventToPips(timeline) {
        let values = timeline.getElementsByClassName('noUi-value');
        for (let val of values) {
            // console.log(val.innerHTML);
            val.gisplayInstant = val.innerHTML;
            val.style.cursor = 'pointer';
            val.onclick = () => {
                let instant = val.gisplayInstant;
                timeline.noUiSlider.set(this.timeVariable.getSortedIndexFromStr(+instant === +instant ? (+instant) : instant));
                this.verifyChanges();
            };
        }
    }

    /**
     * Enable click on timeline values. This is used because the noUiSlider API doesnt come with this.
     * @param {HTMLDivElement} timeline - The timeline div.
     * @private
     * @memberof TimeControl
     */
    _listenToTimeLineEvents(timeline) {
        timeline.noUiSlider.on('end', () => {
            // console.warn("endEvent");
            this.verifyChanges();
        });
        timeline.noUiSlider.on('change', () => {
            // console.warn("changeEvent");
            this.verifyChanges();
        });
    }

    /**
     * Create the div that contains the title of the time control.
     * @returns {HTMLDivElement} - The div that contains the title of the time control.
     * @private
     * @memberof TimeControl
     */
    _createTitleDiv() {
        let title = document.createElement('div');
        title.style.display = 'inline-grid';
        title.style.alignItems = 'center'; // Center vertically
        title.style.textAlign = 'center'; //  Center horizontally
        title.innerHTML = this.timeTitle;
        title.style.userSelect = 'none'; // Disable text selection
        return title;
    }

    /**
     * Creates the NoUiSlider that is used for the timeline.
     * @param {HTMLDivElement} timeline - The timeline div.
     * @param {boolean} [tooltips=false] - To show tooltips for the selected temporal granule(s).
     * @see https://refreshless.com/nouislider/
     * @see https://jsfiddle.net/rr_alves/pxhsprvk/
     * @see https://stackoverflow.com/a/46648942
     * @memberof TimeControl
     */
    _createNoUiSlider(timeline, tooltips = true) {
        timeline.style.width = '90%';
        timeline.style.left = '5%';
        timeline.style.top = '18px';

        let lastIndex = this.timelineIndices.length - 1;
        let min = this.timelineIndices[0], // Start and end instants
            max = this.timelineIndices[lastIndex];
        let step = 100 / lastIndex;
        let range = {
            min: min,
            max: max
        };

        let percentage = 0;
        for (let i = 1; i < lastIndex; i++) { //Ignore first and last
            percentage += step;
            range[percentage] = this.timelineIndices[i];
        }

        let sortedTGs = this.timeVariable.getSortedTemporalGranules();
        let connect = this.hasInstants() ? [false, false] : [false, true, false];
        noUiSlider.create(timeline, {
            range: range,
            start: this.hasInstants() ? min : [min, max],
            snap: true,
            behaviour: 'tap-drag',
            format: {
                to: (value) => { return sortedTGs[value]; },
                from: (value) => { return value; }
            },
            tooltips: tooltips,
            connect: connect
        });
        this.selectedRange = timeline.noUiSlider.get();
    }

    /**
     * Creates a timeline with intervals.
     * @memberof TimeControl
     */
    _buildIntervals() {
        console.log("BUILD INTERVALS");
        if (this.timelineIndices.length === 1)
            this._buildInstants();
        else {
            this.timeControlDiv.style.display = 'grid';
            this.timeControlDiv.style['grid-template-rows'] = '25px 1fr';
            //                  TITLE
            let title = this._createTitleDiv();
            //                  TIMELINE (NoUiSlider)
            let timeline = document.createElement('div');
            timeline.id = this.timeControlId + '_timeline';
            this._createNoUiSlider(timeline);// Create NoUiSlider
            this._addClickEventToPips(timeline);
            this._listenToTimeLineEvents(timeline);

            this.timeControlDiv.appendChild(title);
            this.timeControlDiv.appendChild(timeline);
            this.timeLine = timeline;
            this._setPipsValues(timeline);
        }
    }

    /**
     * Has instants or not.
     * @returns {boolean}  - true, if the timeline is for instants, false, otherwise.
     * @memberof TimeControl
     */
    hasInstants() {
        return this.hasInstantsBool;
    }

    /**
     * The selected range of values. It can be only one if dealing with instants or an array of two elements if dealing with intervals.
     * @returns {number|Array<number>} - the selected instant or interval.
     * @memberof TimeControl
     */
    getSelectedRange() {
        return this.timeLine.noUiSlider.get();
    }

    /**
     * Verifies if there's any change in the selected range, and if there is then dispatchs the TimeRangeChanged event.
     * @memberof TimeControl
     */
    verifyChanges() {
        if (this.hasInstants() && this.selectedRange !== this.getSelectedRange()) {
            this.selectedRange = this.getSelectedRange();
            console.log(this.selectedRange);
            this.dispatchTemporalGranulesChangedEvent();
        } else {
            // console.log("verifyChanges interval");
            let savedRange = this.selectedRange; //Range we have saved
            let timelineRange = this.getSelectedRange(); //Range seen in the page
            // console.log(savedRange, timelineRange);
            if (savedRange[0] !== timelineRange[0] || savedRange[1] !== timelineRange[1]) {
                this.selectedRange = timelineRange;
                this.dispatchTemporalGranulesChangedEvent();
            }
        }
    }

    /**
     * Add arrow key left and right events to change the current selected instant. 
     * Possible problem if there's more than one instance of the Gisplay API.
     * @see https://stackoverflow.com/a/17929007
     * @see https://stackoverflow.com/a/10722635 - Tabindex for multiple instances
     * @see https://stackoverflow.com/a/148444
     * @see https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes - Key codes
     * @memberof TimeControl
     */
    addKeyboardEvents() {
        document.onkeydown = (e) => {
            let timelineInstant = this.timeLine.noUiSlider.get();
            console.log("keyEvent", timelineInstant);
            if (e.keyCode === 37 || e.keyCode === 65) //left key or a
                this.timeLine.noUiSlider.set(this.timeVariable.findPreviousTGIndex(timelineInstant));
            else if (e.keyCode === 39 || e.keyCode === 68) //right key or d
                this.timeLine.noUiSlider.set(this.timeVariable.findNextTGIndex(timelineInstant));
            else if (e.keyCode === 38) //up key
                this.animateInterval(timelineInstant[1] - timelineInstant[0]);
            else if (e.keyCode === 40 || e.keyCode === 83) //down key or s
                this.animateInstant();
            this.verifyChanges();
        };
    }

    /**
     * When the range of selected temporal granules changes then fires the TimeRangeChanged event.
     * @memberof TimeControl
     */
    dispatchTemporalGranulesChangedEvent() {
        document.dispatchEvent(this.rangeChangedEvent);
    }

    /**
     * Returns the indices of the active temporal granules. One index if we're using instants or multiple for intervals.
     * @returns {Array<number>|number} - the indices of the active temporal granules. One index if we're using instants or multiple for intervals.  
     * @memberof TimeControl
     */
    getActiveTemporalGranules() {
        /*  console.warn(this.selectedRange, this.timeLine.noUiSlider.get());
         let isNumber = +this.selectedRange === +this.selectedRange;
         let isArray = Array.isArray(this.selectedRange);
         let hasInstants = this.hasInstants();
         console.log(isNumber, isArray, hasInstants); */
        let range = this.selectedRange;
        if (this.hasInstants() || this.timelineIndices.length === 1)
            return [this.timeVariable.getGranuleIndexFromStr(range)];
        else {
            // console.log(this.timeVariable.getRangeIndices(range[0], range[1]));
            return this.timeVariable.getRangeIndices(range[0], range[1]);
        }
    }

    /**
     * Animate the interval, starting at the current position and in each iteration jumps the interval.
     * @param {number} intervalSize - The amount of time to jump in each iteration.
     * @param {number} [currentValue=this.timeLine.noUiSlider.get()[0]] - The current position of the first handler.
     * @memberof TimeControl
     */
    animateInterval(intervalSize, currentValue = this.timeLine.noUiSlider.get()[0]) {
        setTimeout(() => {
            currentValue += intervalSize;
            if (currentValue < this.max) {
                this.timeLine.noUiSlider.set([currentValue, currentValue + intervalSize]);
                this.verifyChanges();
                this.animateInterval(intervalSize, currentValue);
            }
        }, 1000);
    }

    /**
     * Animates the instant, meaning it will move the slider one instant into the future in each iteration.
     * @param {number} [currentValue=this.timeLine.noUiSlider.get()] - The currentValue of the animation
     * @memberof TimeControl
     */
    animateInstant(currentValue = this.timeLine.noUiSlider.get()) {
        setTimeout(() => {
            let sortedTGS = this.timeVariable.getSortedTemporalGranules();
            if (currentValue !== sortedTGS[sortedTGS.length - 1]) {
                this.timeLine.noUiSlider.set(this.timeVariable.findNextTGIndex(this.timeLine.noUiSlider.get()));
                this.verifyChanges();
                this.animateInstant(this.timeLine.noUiSlider.get());
            }
        }, 1000);
    }
}