//Imports for Intellisense TODO: DELETE
import { GisplayOptions } from '../GisplayOptions';
import { CategoricalVariable } from '../VVs/CategoricalVariable';
import { ContinuousVariable } from '../VVs/ContinuousVariable';
import { TimeVariable } from '../VVs/TimeVariable';

import { BGMapMapBox } from '../Maps/Background Maps/BGMapMapBox';
import { BGMapGoogleMaps } from '../Maps/Background Maps/BGMapGoogleMaps';
import { BGMapHereMaps } from '../Maps/Background Maps/BGMapHereMaps';
import { BGMapBingMaps } from '../Maps/Background Maps/BGMapBingMaps';
import { BGMapMapBoxGLJS } from '../Maps/Background Maps/BGMapMapBoxGLJS';

import { TimeControl } from './TimeControl';
import { Legend } from './Legend';


/**
 * Layout manager for this instance of the Gisplay API.
 * @export
 * @class Layout
 */
export class LayoutManager {

    /**
     * Creates an instance of LayoutManager.
     * @param {GisplayOptions} gisplayOptions - The Gisplay Options object.
     * @memberof LayoutManager
     */
    constructor(gisplayOptions) {
        /**
         * The z-index values for the background map, the div that hides/shows the background map, the
         * Gisplay API canvas and lastly the Legend. 
         * Since we want to stack from back to front: BGMap->BGMapVis->API->ToolTip->Legend, then it's values must have an ascending order. 
         * @type {{BGMap: number, BGMapVisibilityDiv: number, APICanvas: number, ToolTip: number, Legend: number}}
         */
        this.zIndexs = {
            BGMap: 1,
            BGMapVisibilityDiv: 10,
            APICanvas: 100,
            ToolTip: 101,
            Legend: 1000
        };

        this.createLoader();

        /**
         * All the user defined options.
         * @type {GisplayOptions}
         */
        this.globalOpts = gisplayOptions.getGlobalOptions();

        //Gisplay id and container
        /**
         * The id of this gisplay instance container.
         * @type {string}
         */
        this.gisplayId = this.globalOpts.container;
        /**
         * The Gisplay HTML Element (div), which will contain all the DOM elements for this instance of the Gisplay API.
         * @type {HTMLElement} 
         */
        this.gisplayContainer = document.getElementById(this.gisplayId);

        //BGMAPS etc
        /**
         * The id of the div that contains all background maps.
         * @type {string}
         */
        this.bgmapsId = this.gisplayId + "_bgmaps";
        /**
         * Variable to hold the div elements that contain each background map.
         * @type {Array<HTMLElement>}
         */
        this.bgmapsDivs = [];
        /**
         * The div that will hide the background maps.
         * @type {HTMLDivElement}
         */
        this.hideBGMapsDiv = null;

        //OTHER (legend, time control, other views of data)
        /**
         * Id of the div element that will be above the background maps. This element will contain the legend, time control, etc
         * @type {string}
         */
        this.otherViewsId = this.gisplayId + "_other";
        /**
         * The div that contains the other views (legend, time control, etc).
         * @type {HTMLDivElement}
         */
        this.otherViewsDiv = null;
        /**
         * The picking tooltip div.
         * @type {HTMLDivElement}
         */
        this.pickingTooltip = null;

        //TIME CONTROL
        /**
         * Has time control or not.
         * @type {boolean}
         */
        this.hasTimeControl = gisplayOptions.getTimeVariable() !== undefined;
        /**
         * The time control div id.
         * @type {string}
         */
        this.timeControlDivId = this.gisplayId + '_timeControl';
        /**
         * The div element that holds the time control.
         * @type {HTMLDivElement}
         */
        this.timeControlDiv = null;
        /**
         * The time control element.
         * @type {TimeControl}
         */
        this.timeControl = null;
        /**
         * The width of the time control div element.
         * @type {number} 
         */
        this.timeControlWidth = 70;

        //LEGENDS
        /**
         * The number of existing legends.
         * @type {number}
         */
        this.numLegends = gisplayOptions.getCategoricalVariables().length + gisplayOptions.getContinousVariables().length;
        /**
         * The id of the div element that contains the Legend
         * @type {string}
         */
        this.legendsDivId = this.gisplayId + "_legends";
        /**
         * The legend div.
         * @type {HTMLDivElement}
         */
        this.legendsDiv = null;
        /**
         * Array with all the legends.
         * @type {Array<HTMLDivElement>}
         */
        this.legendsDivs = [];
        /**
         * The width of the legends div.
         * @type {number}
         */
        this.legendsWidth = 30;

        /**
         * The background maps main HTML Element (div), which will contain all the DOM elements for the background maps.
         * @type {HTMLElement} 
         */
        this.bgmapsDivContainer = this.setupGisplayCSS();
        /**
         * Canvas that will is used to draw the thematic maps.
         * @type {HTMLCanvasElement}
         */
        this.canvas = this.createCanvas();

        // this.createDisbaleBGMapsIcon();
    }

    /**
     * Creates the loader that will be active until the map is drawn.
     * @memberof LayoutManager
     */
    createLoader() {
        let loader_wrapper = document.createElement('div');
        loader_wrapper.id = 'loader-wrapper';
        let loader = document.createElement('div');
        loader.id = 'loader';
        loader_wrapper.appendChild(loader);
        document.body.appendChild(loader_wrapper);
    }

    /**
     * Dismiss the loader element.
     * @memberof LayoutManager
     */
    dismissLoader() {
        let elem = document.getElementById("loader-wrapper");
        elem.parentElement.removeChild(elem);
    }

    /**
     * Sets up the background maps layout and creates the respective library elements.
     * @returns {Array<BGMapWrapper>} - the background maps that were created.
     * @memberof LayoutManager
     */
    setupBGMapsLayout() {
        this.createLayoutRec(this.globalOpts.layout, this.bgmapsDivContainer);
        return this._createBackgroundMaps();
    }

    /**
     * Recursively creates the layout given by the programmer.
     * @param {Object} obj - The layout object. 
     * @param {HTMLDivElement} parentDiv - The element where the object children will be placed. 
     * @returns 
     * @memberof LayoutManager
     */
    createLayoutRec(obj, parentDiv) {
        let orientation = obj.vertical || obj.horizontal;
        for (let [i, size] of orientation.sizes.entries()) {
            let div = (obj.vertical) ? this.createDiv(parentDiv, size, 100) : this.createDiv(parentDiv, 100, size); //Create descendant div
            let descendantObj = orientation.descendants[i];
            if (typeof descendantObj === 'string') //Leaf found	
                this.createDiv(div, 100, 100, descendantObj);
            else //Another vertical/horizontal element
                this.createLayoutRec(descendantObj, div);
        }
    }

    /**
     * Create a div element and appends it as a child to the parentDiv.
     * @param {HTMLDivElement} parentDiv - The parent div.
     * @param {number} w - Width of the div element.
     * @param {number} h - Height of the div element.
     * @param {string} id - The id that will be given to the new div element.
     * @returns {HTMLDivElement} - the created div element.
     * @memberof LayoutManager
     */
    createDiv(parentDiv, w, h, id) {
        let newDiv = document.createElement('div');
        if (id)
            newDiv.id = this.gisplayId + "_" + id;
        newDiv.className = !id ? "descendant" : '';
        newDiv.style.width = w + "%";
        newDiv.style.height = h + "%";
        newDiv.style.cssFloat = 'left';
        if (id) {
            this.bgmapsDivs.push(newDiv); //Save bgmap Div
            newDiv.style.border = '1px solid';
            newDiv.style.position = 'relative';
        }
        parentDiv.appendChild(newDiv);
        return newDiv;
    }

    /**
     * Creates the background map views. As many as the user defined in the options.
     * @returns {Array<BBGMapWrapper>} - the background maps that were created.
     * @private
     * @memberof LayoutManager
     */
    _createBackgroundMaps() {
        let viewportsMap = new Map(); //Calculate viewports for each bgmap
        for (let div of this.bgmapsDivs)
            viewportsMap.set(div.id, this.calculateViewPort(div));
        let bgmaps = []; //Create bgmaps
        for (let bgmapDiv of this.bgmapsDivs)
            bgmaps.push(this.createBGMapWrapper(this.globalOpts.provider, bgmapDiv, this.globalOpts.bounds[bgmapDiv.id.split(this.gisplayId + "_")[1]], viewportsMap.get(bgmapDiv.id)));
        return bgmaps;
    }

    /**
     * Setups the gisplay CSS
     * @returns {HTMLDivElement} - The div element where the background maps will be placed. 
     * @see https://css-tricks.com/snippets/css/complete-guide-grid/
     * @see https://alligator.io/css/css-grid-layout-fr-unit/
     * @see https://gridbyexample.com/examples/
     * @see https://stackoverflow.com/a/23437087
     * @memberof LayoutManager
     */
    setupGisplayCSS() {
        let gisplayContainerDiv = document.getElementById(this.gisplayId);
        gisplayContainerDiv.style.display = 'grid';
        // gisplayContainerDiv.setAttribute('style', 'grid-template-rows: 3fr 1fr');
        gisplayContainerDiv.style['grid-template-rows'] = this.hasTimeControl ? '88% 12%' : '100%';//minmax(200px,1fr)
        gisplayContainerDiv.style.width = '100%';
        gisplayContainerDiv.style.height = '100%';
        // gisplayContainerDiv.style.border = '1px solid red';

        //Create Background maps div
        let bgmapsDiv = document.createElement('div');
        bgmapsDiv.id = this.bgmapsId;
        bgmapsDiv.style.position = 'relative';
        bgmapsDiv.style.height = '100%';
        /*      bgmapsDiv.style.width = '800px';
             bgmapsDiv.style.height = '600px'; //HERE */
        this.bgmapsDivContainer = bgmapsDiv; //Save the bgmaps div for later use
        gisplayContainerDiv.appendChild(bgmapsDiv);

        //Create div for legend and time control and other elements (future)
        let otherViewsDiv = document.createElement('div');
        otherViewsDiv.id = this.otherViewsId;
        otherViewsDiv.style.display = 'grid';
        //Here we need to know how many instant in time control (if it exists) and how many legends
        let templateColumns = this.hasTimeControl ? '1fr 3fr' : '1fr'.repeat(this.numLegends); // -> goo.gl/B9JP3C 
        otherViewsDiv.style['grid-template-columns'] = templateColumns;
        otherViewsDiv.style['grid-column-gap'] = '5px';
        // otherViewsDiv.style.backgroundColor = '#757575';
        this.otherViewsDiv = otherViewsDiv; //Save the otherviews div for later use


        let temporalView = document.createElement('div');
        temporalView.id = this.otherViewsId;
        temporalView.style.width = '100%';
        temporalView.style.height = '100%';
        this.otherViewsDiv = temporalView;
        gisplayContainerDiv.appendChild(temporalView);

        // gisplayContainerDiv.appendChild(otherViewsDiv);

        return bgmapsDiv;
    }

    /**
     * Given one id it creates a canvas object where everything will be drawn.
     * @see https://stackoverflow.com/a/6862022/
     * @return {HTMLElement} Canvas object where everything will be drawn.
     */
    createCanvas() {
        // console.log(id, bgmapId);
        const gisplayCanvas = document.createElement('canvas');
        gisplayCanvas.id = this.gisplayId + "_canvas";//`_canvas${id}`;
        gisplayCanvas.style.position = 'absolute';
        gisplayCanvas.style.pointerEvents = 'none'; //https://stackoverflow.com/a/6862022/
        gisplayCanvas.style.zIndex = this.zIndexs.APICanvas; //Above sub canvas

        gisplayCanvas.width = this.bgmapsDivContainer.offsetWidth;
        gisplayCanvas.height = this.bgmapsDivContainer.offsetHeight;
        this.bgmapsDivContainer.insertBefore(gisplayCanvas, this.bgmapsDivContainer.firstChild); //Add as first child of mapDiv

        // window.addEventListener('resize', () => { this.resizeCanvas(); }, false); //Resize Canvas when the user changes the size of the window

        return gisplayCanvas;
    }

    /**
     * Calculates the viewport for the given mapId. Each viewport will be used to split the canvas in viewports.
     * @param {string} idGisplay - The gisplay instance id.
     * @param {string} bgmapId - The id of the map.
     * @see http://javascript.info/coordinates
     * @see https://stackoverflow.com/questions/442404/retrieve-the-position-x-y-of-an-html-element
     * @see https://stackoverflow.com/a/11634823/5869289
     * @returns {{left:number, bottom: number, width: number, height: number}} - (left, bottom) position and width and height.
     * @memberof Layout
     */
    calculateViewPort(bgmapDiv) {
        let rect = this.bgmapsDivContainer.getBoundingClientRect();
        let P1 = { l: rect.left, b: rect.bottom, w: rect.width, h: rect.height };
        rect = bgmapDiv.getBoundingClientRect();
        let P2 = { l: rect.left, b: rect.bottom, w: rect.width, h: rect.height };

        let left = Math.abs((P2.l - P1.l) / P1.w);
        let bottom = Math.abs((P2.b - P1.b) / P1.h);
        let width = Math.abs(P2.w / P1.w); // No abs needed
        let height = Math.abs(P2.h / P1.h); //No abs needed

        return { left, bottom, width, height }; // console.log(idProjecao + ": [(" + p2_p1Left + "," + p2_p1Bottom + ") --- (" + d_w + "," + d_h + ")]");
    }

    /**
     * Creates the background map given the provider name.
     * @param {string} providerName - The background map provider.
     * @param {string} bgmapDiv - The background map div.
     * @param {{SW: {lat:number, lng:number}}, {NE: {lat:number, lng:number}}} bound - The bounds for the background map.
     * @param {Array<number>} viewPort - The viewport for the background map.
     * @returns {BGMapWrapper} - The created background map wrapper.
     * @memberof LayoutManager
     */
    createBGMapWrapper(providerName, bgmapDiv, bound, viewPort) {
        // let copyrightNotice = true;
        switch (providerName) {
            case 'Google Maps':
            case 'GM':
                return new BGMapGoogleMaps(bgmapDiv, bound, viewPort);
            case 'Mapbox':
            case 'MB':
                return new BGMapMapBox(bgmapDiv, bound, viewPort);
            case 'MBGL':
                return new BGMapMapBoxGLJS(bgmapDiv, bound, viewPort);
            case 'Here Maps':
            case 'HM':
                return new BGMapHereMaps(bgmapDiv, bound, viewPort);
            case 'Bing Maps':
            case 'BM':
                return new BGMapBingMaps(bgmapDiv, bound, viewPort);
        }
    }

    /**
     * Gets information about the gisplay div.
     * @memberof LayoutManager
     */
    getBgmapsDiv() {
        return { width: this.bgmapsDivContainer.offsetWidth, height: this.bgmapsDivContainer.offsetHeight };
    }

    /**
     * Returns the canvas eleemnt.
     * @returns {HTMLCanvasElement} - the canvas element. 
     * @memberof LayoutManager
     */
    getCanvas() {
        return this.canvas;
    }

    /**
     * Change the width and height of the canvas, equal to the new width and height of the bgmapsContainer.
     * @see https://stackoverflow.com/a/28900478/
     * @memberof LayoutManager
     */
    resizeCanvas() {
        this.canvas.width = this.getBgmapsDiv().width;
        this.canvas.height = this.getBgmapsDiv().height;
    }

    /**
     * Returns the legend div id.
     * @returns {string} - the legend div id.
     * @memberof LayoutManager
     */
    getLegendId() {
        return this.legendsDivId;
    }

    /**
     * Creates the div that is used to hide the background map.
     * The div that is used to "disable" the background map div is just a div with the background set to black
     * that has a higher z-index than the background maps and a lower z-index than the Gisplay API Canvas.
     * @memberof LayoutManager
     */
    _createHiddenBGMapDiv() {
        let hideBGMapDiv = document.createElement('div');
        hideBGMapDiv.style.position = 'absolute';
        hideBGMapDiv.style.width = '100%';
        hideBGMapDiv.style.height = '100%';
        hideBGMapDiv.style.zIndex = this.zIndexs.BGMapVisibilityDiv;
        hideBGMapDiv.style.background = 'black';
        hideBGMapDiv.style.display = 'none'; //Show (Line not needed)
        hideBGMapDiv.style.pointerEvents = 'none'; // Ignore any events
        this.hideBGMapsDiv = hideBGMapDiv;
        this.bgmapsDivContainer.appendChild(hideBGMapDiv);
    }

    /**
     * Create the icon that stays at the top right corner of the background maps and is used to enable/disable the backgroun maps.
     * @memberof LayoutManager
     */
    createDisbaleBGMapsIcon() {
        this._createHiddenBGMapDiv(); //Create the div that is used to enable/disable the background maps

        let icons = {
            hide: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAwSSURBVHja7Zx7dBTVHcc/s0kwRiAYhIqAeEAEQmkrVqmPHiBBSBATkAoFa5Ejta2vVpCHgOQHIk1AoAoeD9KiRYI8pBJRESRUEJFHfRykgAo+QAilKkFQQkIy/SOzk8nuzs6d3dngWfe3J9l7797HfL47M/fOvb+7kLCEJSxhCUtYwhKWsIT9IE071wfQMCZNGE4Zr0h14CfJ5/rQGgS/G6W0AJ7k3sDPfOf64BoQH34d/GncC2DBh9IfnAD18L/goR+YAAH4veWT4Dxx3AsE4e8PlStuBVDDj1sBVPHjVAB1/LgUwA1+HArgDj/uBHCLH2cCuMePKwEiwY8jASLDP0cCSFNSSaERjUgx/k5xSL5sePwGFEBakUkmXckkk+Yhs5zmEIc4xEEOcZAdUh57/JgLIMncQB496MKFLovW8C4bKWWLfBc7/BgKIE3IIY+bXIMHWiXbKGUj26XKe/yYCCCtGEg+vWnkabUfkiufeo3v+ZygdGE8w0mx+fg7PmQPe9lHOZWWVxUZtOVS2hr/23BeUNlOzOJXXuN7KoD0YAL5Ic6pb3iNHexlD5+LblP4CLstNWlkkkU2PWlmyZPqPb5nl4D0ZQK9g5IPU0IJb0hlhLUm0Z0ssrmeNI5zvez1Gt8TAaQ/j9A9IPEDSijhHdtv3F0LjWjG13LWe/yoBZCLeJzh9ZJ0XqFQ3nIs2ZhLzFdjyimnnIMpOyf9V6FVz/CjFECG8QQXWRLOspxC2R2+1LSuep6ezzU2bX/BTm2tvkq+bgj8KASQNjzFAEvCaRbxmHwWFr1bzUjy6KBQfRXrWZK5ckjQUlYgPhpP05g1zLBeIjEWQDTuYiZNzYQanmKaHHMQbDq3u5yG369N67LUKkLwty8byAbgLYbJoQYRQDJYSZYlYS+jZGu4EoXpFQ/xp8BuDIAzHOYwlbTnUpJCFv6Iwf6LKtTJL5/Rzkj5ihHySswFkPas5QozWkUR0+VMuBJTh+pPBjz+VLFZK9Hf5Iu6J8AFKWXttA56BzpyC5cGSNBNKu2ufbmdxWZOndlMDD1k9kgA6cEa8yBgJ3fKB+Hy69rU6Uy0JlCirdDXhnvSW5G052buIds8trOZqUOq7W99MoonON8svpFB8k2MBJBBFJtNVTCZvwavt1utqMnpJeRZEjb5xk7ZqdbWI52q72YE6cDzMjz8nV9+zHIyzeh75IpCZ+paAPkzs82b2Jfkh7/uQdrzEl3N6B7Gy8vqrQHMuuD0z/WmBWucOz5JYz4jzegn9PN4Rkh8zOV+M7qfXKcGZrSo3GnenuDv3B3xgFix35c7eNp8DDtGf3nHMwEkmaXcaka3ku80fbUgpayUXxqRakbLE5HBuxv2SF9W0diInKS/bHGuX6FflmSet+C/QLbz7F3ZfBP/uJbbMPgg68nif0akCWvlBucWHM+AgG//McY5P+BMvUefbwQr6Ck7vMOXy8nRDhe8GKZMR9ZzmRE5Ra7TWeAgQAD+GJnjfNjTflrzb/88gza84HkP8Zuxm9bATBkfplwrNtJZVYKwl0AA/mgVfKgpMqdZHvUSH8iiNQAPyk/ClCyjN/uMSGOnCyGMAJJMcT38uSoHPjWbfkbwpYKHPcUn2T+G8BH2q5Cj6hLYCiDJFDPELb6u6UVG8Az3aRFOh9jd+iYfYpmRmj31Zm8ksBFAkurhj1HDh6lDucoIzpOD3uIDMJ6K2oAuDrUoShBSgCB8pWsfwBz3Hz9vRgzwkYPMNoLdp13phQQhBJAkihlqRh9Ux5fL6VYb0v7y0HHv8QGYg/HsWTPKsTYFCYIECMKfjbJpg4zAWX1hjPCRr1ltBG+bc75jjY4SBAggSSyx4I91gw/6QCOwSX1h0x0+AIuM9/STfRRqdZCgngCSxBKLQ/FYecwVwMX8ojaklcQQHzZgTL7p1yrVHFYCiwAB+OPc4QMD/LXprgVw9chTw9tG8DrF2sNIYAogSTxXD3+WWwhz+LnbbQfoeqJ7m/F+tSgu7dlL4LPgDzPzj48AH1oZ7wdijF8nQFpSR+VWbCTwhcSfGQE+XGK8l8UYn2RT4uoWzrnDS+ADSWKxBX9ChPgRCRDZKk9a3XxEhpsDDCWBT5JYbFndmyBFbqoMKcCR2OLD6NMYbjNac7US9hL4WGTBL4gcX1LNyahjiiWiWeMrr33T010f51EeNyONmeRjPXVT2z0lLWIBKjhtBJVOyyiXOA3PI831gFsGM8+MbOFWnxQzwpQgizWRS4CxMqe1jTV+Ybq5OqF8uZn4y8zpmrfIlVM+8EwCQwC9TWzxocp/t0FzJUAAfo6c8neD3kjgX5t1ECD69f1q/4iDZBcCyC3B+HUDIS8kUBLAC/cG8yI7M/ErF/jL65/8tcG6oXDUEmh+ATrYP6Z6492h32gEPneBX/ftbyVXTvo/sT4MRSmBtssIXHAyL3QOb/AXpHCTEVyriD+IZeai2VZy6vADH4ejkmDKdoyNifpvYocPR3uZ3oOrFfGX2+EHT4hEdxYsNd5z5KLAj7xzbdL9805fZb6pgD/Qgv92IH6oKbFoJCg23pO1oQG1eoYvaQw2gi8PqXbMPZAVFvx+gfihJ0UjlkD28W5tSB9SL91Lv74HaGkEV0ePbzctHvlZ4D8HLF2hl/gzmuvjjOCBVg4OUZJfDz8nFL79wkikEiyldnRuOsJ469ZYOdF0zpv8+7DOUJLPShN/Gzl2fkNhVoflNv5huq5t5GannRtGqe7cw4mmk0af9h5frmCX4Uj/TsHV4RbeJI8XLPj97N2mwi6PRyaBpbyn+IXpFdv8845an4JS+5zq+E7L41F1it7ir0iqWG5Ou65zwF+piu/oIhMgQakoz8B4iw97ZpvL7se4K0y7v2OVuVlnuxM+Nu6pFnvjg14HyDeEakN+r5ffKG9ofF3zFTDBiFSSa+ePLlqvQorML3U7fZ1dJtW8xKz3gqPkyvsNiS9pPGtx1Bgpz9rkS2WxJd92+skJ59qVvLelmMHmhNfFbJawa3Le4k9vyxYL1mxb/BZstOTbwI0q+C48ReVa1pguz1Vk2bkehcJfkFJ2m1aWtmXst+7gxcdvKTJHfvAMo6QmZM5OvEp7M7qUO1Rdpt24ynbiNdMBbadco4oPso0eQDkLk+dNVvbpn9pPn0mdM1QNY+08FWQwCy0bNOcyRn2vkjtn6Yt5lVq/jM3SUxm/DXXQZ1nlmztlu0M7jchiDNYL7YQ2rCDk07+0Zj4DzajudlHXrbt8ExYxiP3cKxvU8KEwvWJXwA6AbTzDvpSPJwWtIUkG/cmnH03qJX9MnuwjyETjDxRadq6Uc6f80x1RJDtGmnM8+EoMd+uTa3jOssmizr7lAPv5WDuhX0JrWtOaVkG35ePMyJh3f4gtGdKFhVxvSXibYaI8SRaFACFFcbjzS7I2Ui8w3BzV7Qzzz3s0lLeRpDKOSZbdyTUUMSWSbVOeCKDW8UkqoxgZtMXSzr7kRWaE2oUmGfyR+/iRJamM26VUsV7vBXDX70/rWjOCfDqGafkjSniJraE6PGnHA4ziAkvSWZ6kQK3Pj4kAkQ17CtMrrtKu1juRQQYXkkwZRziilXHE9/7DH9q0dCVjuTVgu/dm7g2/aynGAth0fH1IZ5/8J0pt61ppzwBuIbDjLeNBWRpRhV4JYIMvFBgp61nH6xKRyySAJHEdAxhg2RDlt4PM4W/icmTpsQB2J798ao4XAarZyTrW8Z5UKNfclCvoSl9yQi6172IWyyLbKOuhAGH28b1OqIclnTI+tbyO4SPJ8kqlA53oTCc6m+5WgVbN6zwur3mDHpUAYYc9zXiWnBA/ghGd7aCY5er7AWMqgMI+vvO5gT704Wcut0sHWw27WE1xtDNKHgrgyquzOVn0IVtpy3x9O8t7bGITWyLxO46hAJH1+3IZfehMS1rSkha0tPmJnUoO8JHxete/gh9bc/s06NFsj6TTgpa0pDlnOGm+jorjat85FcDrmd7vg7mZEYpDfDdzgnGJr757PE7xVdcF4hZfSYB4xlfZPR7X+M67x+Mc38k/oCv/im98p7XBOfGOH95FJoVvjF9/ilv88L8fUMWqeMd3+knNkZTQmOVufYMSlrCEJSxhCUtYwhKWsIR97+3/QGhHE2dximMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTctMTEtMDFUMTI6MDA6MDkrMDE6MDDTOj2LAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE3LTExLTAxVDEyOjAwOjA5KzAxOjAwomeFNwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=',
            view: `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfhCggRBQGJXF9dAAAJH0lEQVR42u2ca3BV1RXHf+Qh5MFLEBp5pEBuC6EFHNECSsKMMAMMBBRUcKYztgMMUSqtA3yoNcDQWqrTmcrTqanGqgMOMoqMrR+CA2TKK61Ea4GSAGUEAoUSIAkJQnL74d67s8/JzTn7nLNP0sf+n2/3rrX2/v/PY6+9z9oHDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz+L9Ctk9sbSIRh9CKbLLLIJovbNNJIA4008k+qOUXT/5oAvZnM94gQIUJPV+so56immmPs5wta/5sF6EsBhRQyjhSfEa5RwV72UUVL2ELoxT08QwUtRDUdVynlEVLD6KruK6A3j7KAqY6dvc1VGmigngYaSSObnmSTTU/6Osa+yA62cfA/V4B8VrGA7kn/q+Uo1dRQTTVnO7yge5FHhAh5jOQ+7kpqc4rf8AY39coQHJPYRWuSi/csb7OIiI+IGUyhhD00Jol6mdX062rKbZhFRZJOVvI8wzRET6eQrVxpF7+RDeR2NXUYw752XTtOieI5zyCPb3Zw09hlmMk71NtaamYdmV1Hvi+buGPp0B3e4wEXrz48xet8wl+5Gvdq5TJVfMwW5rrQyWQJJ9vdZPO7gnwKS7hs6UgTWxjh6JPLc5Rz23HAa2I3ixjo2PI8jti8ysnvXPrDOWTpQD3rGODoMYqPPIz7LbzFUMd4Uyi3eNymxHe65RkLuW5p/F3udbTP4be2W0XlaOYVl7xgLqctHnsZFD75LN6wNHqUhx3t01iTZCD7ktcoYTGzuJ8HmcNS1lJqoxPLAH/imKn0oISbkv0VZodLfywnpObqKHZJT+9mj4VQKwdY6TBCjGUNVTYR3nd5MA5lp8X+VaVRxRceo0lq6IDrKDyaUzYq31JqZzyfWvw+Y4iLx9M0SPYHw0mSlkqTm1Z+SZqL/WxuSJ3azwRPrc3gc8n7IhNd7L/NUUsmMlSpFQ9YbenONFf7Ikmum3zfR4sprLDEcMsvuvOq1MdzjNZHPoWtUug/OY7SMXxXytnOMd53y9O5JuKcdxlrAOZID9yrPKSH/l28L9H/iAxXj/6cEfaHyEli0Z08CnhcQcqRUuZXqdD2BP4lXTWzdNDfJdH/ncKiRLo0OzhED9u/mSxgh3hk3WKhgpxtj9JtCj3O5ythf4sinfRfUvL5hXTxW89+OsVcsA1ylQoRR0uP08UK9kM4rkcCmX4ry5V8BonE5Kbt3h9lySESx3alqLPF4/ACWQr2/aTZgm8JrPSLFb1KhY/1yT/Dlj7H8nfVzABeFF4/U7LvLc1WfEngj36+yPn3W36fbpkLnKGMZUz0NJNPpybufYN7wpfAH32kGZ+c9oySBrM7vNTBWp8bnhQxNih6+JbAL/1cKeltQ7p07x/nQV/kAbpRGY/SqDAcBpDAL314TnjJd/azEn3VjifHVBFJ/Wx6lsA/fcQSxQHptywuiovf/9mPoRvn47FKPXh5kiAI/T5isWul9OtTHrMIZ2yOx7rkafVHWYIg9GWq8nw/kUaf8fnos2KaaGOSJz8lCYLRh9fjvl9aYiaS3jIN9CGduni8Fzx6ukoQlD78Me79mvTbcBFzmRYB4JMkrWiQIDh9xFJWifTbRBHVbUFDFYkVyd0+fDuUQAd9uJRkulIkkl5d728SU63PfHknlUAP/XTxalSeg/enuV1iFAzLxDjgD0kk2CFl6St8d2ywiHG/5feFVLJdecrjjnniVKXrkqCIW+KHQ/T2GTZHxAia7jjjiXgrLQHqRVZIp3wH6JEgVcz35oQqwHIS6wJ+USzVMexKZCc6JEis9SwNVYD1qK8kKdPXI8Ff4t5rQxXgrXgrH+qlr0OC3T4mKt6RmHBt1k0/uASJicrpEOlniHX/VfrpB5VgrvAcG5oAc0Qb48KgH0yCTPHqdE1oApTFWzgbFv1gEiSeAlUh0U8VtWIbw6MfRIJFwsv/20AnzBfxp4ZJ378EA8Xri09DoJ/G3+PR6zykwb7o+5egTPjM0C5AsYi9Jnz6fiUYIh6En2uu2WpbXL1IdmfQ9yvBrzTMLJNhk4irOmEPTN8uwRGlyps+4v18C9O10V8senHCtTQnhuU66NslOKFUnLxM2F9jpBb6BXwtYqqUwXUTU6bA9O0SnOc7Ch7bhP1J+gemP0IqyV2vYJ8mJkxa6APMkooR65jsap/Bn4X9qYDFSgUS/d0KD9YssTodW4rT8TYCeEhUdkdp4glX+8HUCvsbAao3F0sX/9/o5Wo/yFJGvVXnSDSac5bQPVzsJ0hXTQsv+ljBy5Ke/FGuuFSiA8y01K+v1kc+hqFS5U2ULxjlYj/eIlkNT3rYn5RGsRj3o0Q5Rp6LRzqvSM/9lnDWpfpxUOpUIz9wsc+xFdVXMlVBhFTmi6Q3dvzBNQfJtbTUxGNh0Ad7NWaUnS4lqd35vcU+ynk2M62DGyKDOZS12x30a5c7OZVi8cYwNlh7Wo/wvm1uNm9KKVET63mZZgf7p/l5uzr+axzmArXUUksqOeSQw6AkVUM1rOIDx948zEbL4sib/IhGz5w8YhB7LefoNHMd7TP4aZIaMbfjEs+6PDrv5V2Lx3WFsktNSKHEtvennCmOHv3ZkHQHYPKjjrUuU54BrLPtITvE8M6iH0O+bddOlCPMc7xfMyiiVLxETX6cZSNTXc78CLZYdi5EucySztsxJGM+Z20ETrLE5U1wCpN4ga18yGG+4jbN/IMD7GQjKxWWOh/gvXZb9Ta57C0KFZmsi78BbjvqeYeZiqmP+kM4QoklE4kd+xjTdeQTyE16f19hK4W+3+HKGMbzokZQPip0lMPrQj9W2zZSJtKlPZQwxVeFYIRFvN3uJosSpZVdHsukOoTO7fOZ/JAfd5Czf81RTsS3z9dwo4MIqeTGt89HuC/pVgu4xXZe5piuTuv/hMZEFvI433C0qaNefELhDlniEwp3O94yLZSznQ+4rr3P2pHKI5RKU+igRwsVPKNYJe4RYX5EJZVxFDKFyfTxGaGVKvaxj/3UhdXJzviMTgpjKCCfCBEGK7RYTzXVVHOYivAv987+kFIGI4gwQHxGKYt08RmlBm5whmrfFWAGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYd49+1hS5Bbj9eKwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNy0xMC0wOFQxNzowNTowMSswMjowMAP/KtMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTctMTAtMDhUMTc6MDU6MDErMDI6MDByopJvAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==`
        };

        let visibilityIconDiv = document.createElement('div');
        visibilityIconDiv.style.position = 'absolute';
        visibilityIconDiv.style.left = '95%';
        visibilityIconDiv.style.top = '3%';
        visibilityIconDiv.style.zIndex = this.zIndexs.Legend + 1;
        visibilityIconDiv.style.cursor = 'pointer';

        let imageDiv = document.createElement('img');
        imageDiv.src = `${icons.view}`;
        imageDiv.style.width = '32px';
        imageDiv.style.height = '32px';
        imageDiv.active = true;

        visibilityIconDiv.onclick = () => {
            imageDiv.active = !imageDiv.active;
            imageDiv.src = `${imageDiv.active ? icons.view : icons.hide}`; //Change icon
            imageDiv.style.filter = !imageDiv.active ? 'invert(100%)' : 'invert(0%)'; //Invert color of inactive icon
            imageDiv.active ? this._enableBackgroundMaps() : this._disableBackgroundMaps(); //Change hidden div status
        };

        visibilityIconDiv.appendChild(imageDiv);

        this.bgmapsDivContainer.appendChild(visibilityIconDiv);
    }

    /**
     * Disable the background maps.
     * To disable the background maps just "enable" (set the display to block) the div that is stacked above the background maps.
     * @memberof LayoutManager
     */
    _disableBackgroundMaps() {
        this.hideBGMapsDiv.style.display = 'block';
    }

    /**
     * Enable/Reanable the background maps. 
     * Just hides the div that was stacked above the background maps. 
     * @memberof LayoutManager
     */
    _enableBackgroundMaps() {
        this.hideBGMapsDiv.style.display = 'none';
    }

    /*
    #####################################################################
    #######################     TIME CONTROL     ######################
    #####################################################################
    */
    /**
     * Creates the time control
     * @param {TimeVariable} timeVariable 
     * @memberof LayoutManager
     */
    createTimeControl(timeVariable) {
        this.timeControlDiv = document.createElement('div');
        this.timeControlDiv.id = this.timeControlDivId;
        this.timeControlDiv.style.borderBottom = "1px solid";
        this.timeControlDiv.style.height = '100%';
        this.otherViewsDiv.appendChild(this.timeControlDiv);
        this.timeControl = new TimeControl(timeVariable, this.timeControlDiv);
    }

    /**
     * Returns the time control object.
     * @returns {TimeControl} - the time control object.
     * @memberof LayoutManager
     */
    getTimeControl() {
        return this.timeControl;
    }

    /**
     * Returns the granules that are active in the time control.
     * @returns {Array<number>|number} 
     * @memberof LayoutManager
     */
    getActiveTemporalGranules() {
        return this.getTimeControl().getActiveTemporalGranules();
    }

    /*
    #####################################################################
    #######################        LEGEND          ######################
    #####################################################################
    */
    /**
     * Creates a legend for the given map variable (Categorical or Continuous).
     * @param {ContinuousVariable|CategoricalVariable} mapVariable - The map variable used to create the legend.
     * @param {HTMLImageElement} shapesImage - The image with the shapes.
     * @memberof LayoutManager
     */
    createLegend(mapVariable, shapesImage) {
        if (!this.legendsDiv) { // CRIAR O DIV DA LEGENDA
            let legendsDiv = document.createElement('div');
            legendsDiv.id = this.legendsDivId;

            legendsDiv.style.position = 'absolute';
            legendsDiv.style.zIndex = this.zIndexs.Legend;
            legendsDiv.style.bottom = '40px';
            legendsDiv.style.right = '10px';

            this.legendsDiv = legendsDiv;
            this.bgmapsDivContainer.appendChild(this.legendsDiv);
        }
        let legend = new Legend(mapVariable, this.legendsDiv, shapesImage);
        this.legendsDivs.push(legend.getLegendDiv());
        // let legendDivId = this.legendsDivId + '_' + mapVariable.getExternalName();
        return legend;// new Legend(mapVariable, this.legendsDiv);
    }

    /*
    #####################################################################
    #######################     WEBGL PICKING      ######################
    #####################################################################
    */
    /**
     * Create the tooltip in the clicked position.
     * @param {number} clickX - The clicked X position
     * @param {number} clickY - The clicked Y postion.
     * @param {BGMapWrapper} bgmap - The background map that was clicked.
     * @param {string} message - The message to show.
     * @memberof GisplayMap
     */
    createToolTip(clickX, clickY, bgmap, message) {
        this.deletePickingTooltip();

        let pickingTooltip = this.pickingTooltip || document.createElement('div');
        pickingTooltip.id = 'picking_tooltip';
        pickingTooltip.style.position = 'absolute';
        pickingTooltip.style.left = clickX + 'px';
        pickingTooltip.style.top = clickY + 'px';
        pickingTooltip.style.zIndex = this.zIndexs.ToolTip;

        // pickingTooltip.id = 'show-tip';
        pickingTooltip.setAttribute('data-balloon-visible', '');

        //left, right up, down defined by the distance to the left right etc
        let w = bgmap.getWidth(),
            minDist = 100; //Min distance to any border
        let tooltipDirection = 'up';
        if (clickX <= minDist)
            tooltipDirection = 'right';
        else if (clickX >= w - minDist && clickY <= minDist)
            tooltipDirection = 'down';
        else if (clickX >= w - minDist)
            tooltipDirection = 'left';
        else if (clickY <= minDist)
            tooltipDirection = 'down';
        pickingTooltip.setAttribute('data-balloon-pos', tooltipDirection);
        pickingTooltip.setAttribute('data-balloon-break', '');
        pickingTooltip.setAttribute('data-balloon', message);
        bgmap.getDiv().appendChild(pickingTooltip);
        /*  tooltip.onmouseenter = () => {
             tooltip.parentElement.removeChild(tooltip);
         }; */
        this.pickingTooltip = pickingTooltip;
    }

    /**
     * Hide the picking tooltip
     * @memberof LayoutManager
     */
    hidePickingToolTip() {
        this.pickingTooltip.style.display = 'none';
    }

    /**
     * Delete the picking tooltip from the DOM.
     * @memberof LayoutManager
     */
    deletePickingTooltip() {
        let pickingTooltip = document.getElementById('picking_tooltip');
        if (pickingTooltip) {
            pickingTooltip.parentNode.removeChild(pickingTooltip);
            this.pickingTooltip = null;
        }
    }
}