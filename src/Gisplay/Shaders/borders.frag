#version 300 es 
precision mediump float;

uniform vec3 polygonColor;
uniform float polygonAlpha;

layout(location = 0) out vec4 out_0;

void main() {
	out_0 = vec4(polygonColor / 255.0, polygonAlpha);
}