#version 300 es
/* Polygons map vertex shader */

#define PI radians(180.0)

/* Mandatory part */
in vec2 position;	/* Lat, Lon coordinates of point */
uniform mat3 Mproj;			/* Projection Matrix */

/* Map specific part */

/* Global constants */

/* Picking information */
in vec4 pickingColor;
out vec4 v_PickingColor;

vec4 project()
{
    float phi = position.y * (PI / 180.0);
    float yval = PI -log( tan((PI/4.0) + phi/2.0) );
    vec3 f = vec3(position.x, yval, 1.0);
    vec3 res = Mproj * f;
    return vec4(res.x, -res.y , 0.0, 1.0);
}

void main() {
	gl_Position = project();
    v_PickingColor = pickingColor;
}
