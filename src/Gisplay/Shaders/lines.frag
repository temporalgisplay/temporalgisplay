#version 300 es
precision mediump float;

uniform vec3 lineColor;
uniform float lineAlpha;

/* Picking information */
uniform bool isPicking;
in vec4 v_PickingColor;
uniform vec4 MVCTGPickingColor;

layout(location = 0) out vec4 out_0;
layout(location = 1) out vec4 out_1;

void main() {
	if(isPicking){
		out_0 = v_PickingColor / 255.0;
		out_1 = MVCTGPickingColor / 255.0;
	}
	else
		out_0 = vec4(lineColor / 255.0, lineAlpha);
}