#version 300 es
precision mediump float;

uniform vec3 pointColor;
uniform float pointAlpha;

uniform int pointShape;

uniform sampler2D tex;     // Texture with the shapes

/* Picking information */
uniform bool isPicking;
in vec4 v_PickingColor;
uniform vec4 MVCTGPickingColor;

layout(location = 0) out vec4 out_0;
layout(location = 1) out vec4 out_1;

vec2 coords_for_shape()
{
    float s = float(pointShape);
    return vec2(mod(s, 4.0) * 0.25,floor(s/4.0) * 0.25);
}

void main() {
    vec2 orig = coords_for_shape();
    vec2 tcoord = orig + gl_PointCoord * 0.25;

    float a = texture(tex, tcoord).a;
    if( a < 1.0 )
        discard;
    else {
        if(isPicking) {
            out_0 = v_PickingColor / 255.0;
            out_1 = MVCTGPickingColor / 255.0;
        }
        else
            out_0 = vec4(pointColor / 255.0, pointAlpha);
    }
}
