#version 300 es 
precision mediump float;

uniform vec3 polygonColor;
uniform float polygonAlpha;

uniform int polygonPattern;
uniform int patternSize;

uniform sampler2D patterns;

/* Picking information */
uniform bool isPicking;
in vec4 v_PickingColor;
uniform vec4 MVCTGPickingColor;

layout(location = 0) out vec4 out_0;
layout(location = 1) out vec4 out_1;

vec2 coords_for_pattern()
{
    float p = float(polygonPattern);
    return vec2(mod(p, 4.0) * 0.25,floor(p/4.0) * 0.25);
}

void main() {
	if(isPicking) {
		out_0 = v_PickingColor / 255.0;
		out_1 = MVCTGPickingColor / 255.0;
	}
	else if(polygonPattern == -1) 
		out_0 = vec4(polygonColor / 255.0, polygonAlpha);
	else {
		vec2 orig = coords_for_pattern();
		float ps = float(patternSize);
		vec2 xy = vec2(gl_FragCoord.x, -gl_FragCoord.y); /* Invert y value, otherwise it would invert the texture*/
		vec2 tcoord = orig + mod(xy, ps) / ps * 0.25;
		float a = texture(patterns, tcoord).a;
		if(a < 0.5)
			discard;
		else
			out_0 = vec4(polygonColor / 255.0, a);
	}
}