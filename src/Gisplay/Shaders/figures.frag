#version 300 es 
precision mediump float;

uniform vec3 figureColor;
uniform float figureAlpha;

uniform int figureIndex;
uniform float figureOrientation;

uniform sampler2D tex;     // Texture with the shapes

/* Picking information */
uniform bool isPicking;
in vec4 v_PickingColor;
uniform vec4 MVCTGPickingColor;

layout(location = 0) out vec4 out_0;
layout(location = 1) out vec4 out_1;

vec2 coords_for_shape()
{
    float s = float(figureIndex);
    return vec2(mod(s, 4.0) * 0.25,floor(s/4.0) * 0.25);
}

vec2 rotate()
{
    float vRotation = figureOrientation * radians(180.0) / 180.0; //https://stackoverflow.com/a/31687958
    float mid = 0.5;
    vec2 rotated = vec2(cos(vRotation) * (gl_PointCoord.x - mid) + sin(vRotation) * (gl_PointCoord.y - mid) + mid,
                        cos(vRotation) * (gl_PointCoord.y - mid) - sin(vRotation) * (gl_PointCoord.x - mid) + mid);
    return rotated;
}

void main() {
    vec2 orig = coords_for_shape();
    vec2 rotated = rotate();
    vec2 tcoord = orig + rotated * 0.25;

    float a = texture(tex, tcoord).a;
    if( a < 1.0 )
        discard;
    else {
         if(isPicking) {
            out_0 = v_PickingColor / 255.0;
            out_1 = MVCTGPickingColor / 255.0;
        }
        else
            out_0 = vec4(figureColor / 255.0, figureAlpha);
    }
}