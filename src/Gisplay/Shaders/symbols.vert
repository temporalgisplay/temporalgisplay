#version 300 es
/* Symbols map vertex shader */

#define PI radians(180.0)

/* Mandatory part */
in vec2 position;	        /* Lat, Lon coordinates of point */
uniform mat3 Mproj;			        /* Projection Matrix */

/* Map specific part */
uniform float minPixels;            /* Min size of any point */
uniform float maxPixels;            /* Max size of any point */
uniform float minData;              /* Minimum of all size values in the data */
uniform float maxData;              /* Maximum of all size values in the data */
in float symbolSize;		    /* Symbol size aka the data that the point contains */

/* Global constants */

/* Picking information */
in vec4 pickingColor;
out vec4 v_PickingColor;

vec4 project()
{
    float phi = position.y * (PI / 180.0);
    float yval = PI -log( tan((PI/4.0) + phi/2.0) );
    vec3 f = vec3(position.x, yval, 1.0);
    vec3 res = Mproj * f;
    return vec4(res.x, -res.y , 0.0, 1.0);
}

float calcSize()
{
    if(maxData == 0.0)                                               /* Means there's only a size for all symbols */
        return symbolSize;
    float normalized = (symbolSize - minData) / (maxData - minData); /* https://stats.stackexchange.com/a/144342 */
    return normalized * (maxPixels - minPixels) + minPixels;         /* between minPixels and maxPixels */
}

void main() {
	gl_Position = project();
	gl_PointSize = calcSize(); 
    v_PickingColor = pickingColor;
}
