import { GisplayDefaults } from '../GisplayDefaults';
import { GisplayError } from '../GisplayError';

/**
 * Contains all the information about the shaders currently in use.
 * @export
 * @class ShadersInfo
 */
export class ShadersInfo {

    /**
     * Creates an instance of ShadersInfo.
     * @param {Array<{name:string, type:string, qualifier: string}>} vertexKeys - The vertex shader keys.
     * @param {Array<{name:string, type:string, qualifier: string}>} fragmentKeys - The fragment shader keys.
     * @memberof Shaders
     */
    constructor(vertexKeys, fragmentKeys) {
        let vv_type = "vv_type"; //If we want to change from vv_type to other thing just change here
        let shaderMap = new Map();
        if (vertexKeys)
            for (const key of vertexKeys)
                shaderMap.set(key[vv_type], key);
        if (fragmentKeys)
            for (const key of fragmentKeys)
                shaderMap.set(key[vv_type], key);
        // console.log(shaderMap);

        if (!vertexKeys && !fragmentKeys)
            throw new GisplayError("No vertex or fragment shader variable information was found on shaders.json.");

        /**
         * Contains all information that was processed for each variable in the vertex and fragment shaders.
         * @type{Map<string, {name: string, type: number, qualifier: string}>}
         */
        this.shaderVariablesMap = shaderMap;
        /**
         * The position string on the vertex shader. All Gisplay shaders use the string 'position' as default.
         * @type {string='position'}
         */
        this.positionString = GisplayDefaults.POSITION();
        /**
         * The matrix projection string on the vertex shader. All Gisplay shaders use the string 'Mproj' as default.
         * @type {string='Mproj'}
         */
        this.matrixProjectionString = GisplayDefaults.MPROJ();
        /**
         * The color string on the fragment shader. Can be overriden by the user on the shaders.json file and respective shader.frag file.
         * @type {string='color'}
         */
        this.colorString = shaderMap.get(GisplayDefaults.COLOR()) ? shaderMap.get(GisplayDefaults.COLOR()).name : GisplayDefaults.COLOR();
        /**
         * The opacity string on the fragment shader.
         * @type {string='opacity'}
         */
        this.opacityString = shaderMap.get(GisplayDefaults.OPACITY()) ? shaderMap.get(GisplayDefaults.OPACITY()).name : GisplayDefaults.OPACITY();
        /**
         * The texture pattern string on the fragment shader.
         * @type {string}
         */
        this.textureString = shaderMap.get(GisplayDefaults.TEXTURE()) ? shaderMap.get(GisplayDefaults.TEXTURE()).name : GisplayDefaults.TEXTURE();
        /**
         * The shape string on the fragment shader. 
         * @type {string}
         */
        this.shapeString = shaderMap.get(GisplayDefaults.SHAPE()) ? shaderMap.get(GisplayDefaults.SHAPE()).name : GisplayDefaults.SHAPE();
        // console.error(shaderMap, this.shapeString);
        /**
         * The size string on the vertex shader.
         * @type {string}
         */
        this.sizeString = shaderMap.get(GisplayDefaults.SIZE()) ? shaderMap.get(GisplayDefaults.SIZE()).name : GisplayDefaults.SIZE();
        /**
         * The orientation string on the fragment shader.
         * @type {string}
         */
        this.orientationString = shaderMap.get(GisplayDefaults.ORIENTATION()) ? shaderMap.get(GisplayDefaults.ORIENTATION()).name : GisplayDefaults.ORIENTATION();
    }

    /**
     * The name of the position variable on the vertex shader. By default all Gisplay API shaders 
     * use 'position' as the name for the position variable.
     * @returns 
     * @memberof ShadersInfo
     */
    getPositionName() {
        return this.positionString;
    }

    /**
     * The name of the matrix projection variable on the vertex shader. By default all Gisplay API shaders
     * use 'Mproj' as the name for the matrix projection variable.
     * @returns 
     * @memberof ShadersInfo
     */
    getMatrixProjectionName() {
        return this.matrixProjectionString;
    }

    /**
     * The name of the color variable on the fragment shader. The API will try to use 'color' if none is given but there's no 
     * way to know if this is correct, and if it isn't then errors may occur.
     * @returns {string} - the name of the color variable on the fragment shader.
     * @memberof ShadersInfo
     */
    getColorName() {
        return this.colorString;
    }

    /**
     * The name of the opacity variable on the fragment shader. The API will try to use 'opacity' if none is given but there's no
     * way to know if this is correct, and if it isn't then errors may occur.
     * @returns {string} - the name of the opacity variable on the fragment shader.
     * @memberof ShadersInfo
     */
    getOpacityName() {
        return this.opacityString;
    }

    /**
     * The name of the size variable on the vertex shader. The API will try to use 'size' if none is given but there's no
     * way to know if this is correct, and if it isn't then errors may occur.
     * @returns {string} - the name of the size variable on the vertex shader.
     * @memberof ShadersInfo
     */
    getSizeVarName() {
        return this.sizeString;
    }

    /**
     * The name of the shape variable on the fragment shader. The API will try to use 'shape' if none is given but there's no
     * way to know if this is correct, and if it isn't then errors may occur.
     * @returns {string} - the name of the shape variable on the fragment shader.
     * @memberof ShadersInfo
     */
    getShapeName() {
        return this.shapeString;
    }

    /**
     * The name of the texture pattern variable on the fragment shader.  The API will try to use 'texture' if none is given but there's no
     * way to know if this is correct, and if it isn't then errors may occur.
     * Keep in mind that the texture string is not provided by the programmer but instead the pattern index.
     * @returns {string} - The name of the texture pattern variable on the fragment shader. 
     * @memberof ShadersInfo
     */
    getTextureName() {
        return this.textureString;
    }

    /**
     * The name of the orientation variable on the fragment shader.  The API will use 'orientation' if none is given.
     * @returns {string} - the name of the orientation variable on the fragment shader.
     * @memberof ShadersInfo
     */
    getOrientationName() {
        return this.orientationString;
    }

    /**
     * Returns the mapping between the visual variables and their associated information. 
     * @returns {Map<string, {name: string, type: number, qualifier: string}>} - the map of variables and their associated information. 
     * @memberof ShadersInfo
     */
    getShaderVariablesMap() {
        return this.shaderVariablesMap;
    }
}