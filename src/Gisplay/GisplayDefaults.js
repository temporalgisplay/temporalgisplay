import defaultsFile from './defaults.json';
import { GisplayError } from './GisplayError';

/**
 * Class that exports the Gisplay default values.
 * @export
 * @class GisplayDefaults
 */
export class GisplayDefaults {

    /**
     * Returns the available class break methods.
     * @returns {string[]} - All available class break methods under Gisplay API.
     * @memberOf GisplayOptions
     */
    static getAvailableClassBreaksMethods() {
        return defaultsFile.availableClasssBreakMethods; //["quantiles", "equalintervals", "k-means"];
    }

    /**
     * Returns a list of available background map providers.
     * @returns {string[]} - All available background map providers under Gisplay API.
     * @memberof GisplayOptions
     */
    static getAvailableBackgroundProviders() {
        return defaultsFile.bgmapsProviders;/*['Google Maps', 'GM', //All variants of Google Maps
            'Mapbox', 'MB', //All variants of Mapbox
            'Here Maps', 'HM',
            'Bing Maps', 'BM'];*/
    }

    /**
     * Returns the default class method to calculate classes for continuous variables.
     * @static
     * @returns {string} - Returns quantiles, the default method for class calculation.
     * @memberof GisplayDefaults
     */
    static getDefaultClassBreakMethod() {
        return defaultsFile.defaultClassBreakMethod;//'equalintervals';
    }

    /**
     * Returns the list of available granularities.
     * @static
     * @returns 
     * @memberof GisplayDefaults
     */
    static getAvailableGranularities() {
        return ['monthOfYear', 'dayOfYear', 'dayOfMonth', 'hourOfDay', 'minuteOfHour', 'year', 'month', 'day', 'value'];
    }

    /**
     * Returns the available color brewer methods.
     * @returns {Array<string>} - the available color brewer methods.
     * @memberof GisplayDefaults
     */
    static getAvailableColorBrewerMethods() {
        return [
            'colorbrewer-' + GisplayDefaults.SEQUENTIAL().toLocaleLowerCase(),
            'colorbrewer-' + GisplayDefaults.DIVERGENT().toLocaleLowerCase(),
            'colorbrewer-' + GisplayDefaults.QUALITATIVE().toLocaleLowerCase()
        ];
    }

    /**
     * Convert the time string to the granularity we want.
     * @param {string} timeString - The time as it was read from the file. 
     * @returns {number} - The granul
     * @memberof CSVDataWorker
     */
    static getGranule(timeString) {
        let value;
        // console.warn(timeString, this.timeGranularity);
        /*
            //if cyclic then ...
            day of year -> dayOfYear (n temos )
            month of year -> monthOfYear (temos)
            day of month -> dayOfMonth (temos)
            hour of day -> hourOfDay (temos)
            minute of hour -> minuteOfHour (temos)
            
            //else continuous
            year -> 2015, 2016 ...
            month -> 2015_01, 2015_02 ... 2016_01 etc
            day -> 2015_01_01, 2015_01_02 ...
            hour -> 2015_01_01_01, 2015_01_01_01 ...
            minute
            value
        */
        switch (this.timeGranularity) {
            //CYCLIC
            case 'monthOfYear': //Mes do ano
                value = (new Date(timeString).getMonth() + 1); //GOOD
                break;
            case 'dayOfYear':
                value = -1; //CHANGE to calculate day of year
                break;
            case 'dayOfMonth':
                value = new Date(timeString).getDate(); //GOOD
                break;
            case 'hourOfDay':
                value = new Date(timeString).getHours(); //GOOD
                break;
            case 'minuteOfHour':
                value = new Date(timeString).getMinutes(); //GOOD
                break;

            //CONTINUOUS
            case 'year':
                value = new Date(timeString).getFullYear();
                break;
            case 'month': //Mes do ano
                value = new Date(timeString).getFullYear() + "_" + (new Date(timeString).getMonth() + 1);
                break;
            case 'day':
                value = new Date(timeString).getFullYear() + "_" + (new Date(timeString).getMonth() + 1) + "_" + new Date(timeString).getDate();
                // value = Number(new Date(timeString).getFullYear() + "" + new Date(timeString).getMonth() + "" + new Date(timeString).getDay());
                break;
            case 'value':
                value = +timeString;
                break;
        }
        // console.log(value);
        return value;
    }

    /*
    #####################################################################
    #######################     VISUAL VARIABLES   ######################
    #####################################################################
    */
    /**
     * The color visual variable.
     * @static
     * @returns {string} - the string 'color'.
     * @memberOf GisplayDefaults
     */
    static COLOR() {
        return defaultsFile.visualVariables.COLOR;
    }

    /**
     * The saturation visual variable.
     * @static 
     * @returns {string} - The saturation visual variable string. 
     * @memberOf GisplayDefaults
     */
    static OPACITY() {
        return defaultsFile.visualVariables.OPACITY;
    }

    /**
     * The shape visual variable.
     * @static
     * @returns {string} - The shape visual  variable.
     * @memberOf GisplayDefaults
     */
    static SHAPE() {
        return defaultsFile.visualVariables.SHAPE;
    }

    /**
     * The position visual variable.
     * @static
     * @returns {string} - The position visual variable.
     * @memberOf GisplayDefaults
     */
    static POSITION() {
        return defaultsFile.visualVariables.POSITION;
    }

    /**
     * The variable that is used to hold the matrix projection 
     * @static
     * @returns {string} - the projection matrix variable name on all shaders.
     * @memberOf GisplayDefaults
     */
    static MPROJ() {
        return defaultsFile.defaultShadersVariables.projectionMatrix;
    }

    /**
     * The texture visual variable.
     * @static
     * @returns {string} - The texture visual variable.
     * @memberOf GisplayDefaults
     */
    static TEXTURE() {
        return defaultsFile.visualVariables.TEXTURE;
    }

    /**
     * The size visual variable.
     * @static
     * @returns {string} - The size visual variable.
     * @memberOf GisplayDefaults
     */
    static SIZE() {
        return defaultsFile.visualVariables.SIZE;
    }

    /**
     * The orientation visual variable.
     * @static
     * @returns {string} - The orientation visual  variable.
     * @memberOf GisplayDefaults
     */
    static ORIENTATION() {
        return defaultsFile.visualVariables.ORIENTATION;
    }

    /**
     * Returns the index of the given shape.
     * @static
     * @param {string} shapeName - The name of the given shape.
     * @returns {number} - the index of the given shape.
     * @throws {GisplayError} - If the given shape does not exist.
     * @memberof GisplayDefaults
     */
    static findShapeIndex(shapeName) {
        // console.warn(shapeTextureName);
        let shapeIndex = defaultsFile.shapes[shapeName];
        if (shapeIndex >= 0)
            return shapeIndex;
        throw new GisplayError(`Shape with name: ${shapeName}, not found.`);
    }

    /**
     * Returns the index of the given pattern.
     * @static
     * @param {string} patternName - The name of the given pattern.
     * @returns {number} - the index of the given pattern.
     * @throws {GisplayError} - If the given pattern does not exist.
     * @memberof GisplayDefaults
     */
    static findPatternIndex(patternName) {
        let patternIndex = defaultsFile.patterns[patternName];
        if (patternIndex >= 0)
            return patternIndex;
        throw new GisplayError(`Pattern with name: ${patternName}, not found.`);
    }

    /**
     * Returns the index of the given figure name.
     * @static
     * @param {string} figureName - The figure name. 
     * @returns {number} - the index of the given figure name.
     * @throws {GisplayError} - If the given figure does not exist.
     * @memberof GisplayDefaults
     */
    static findFigureIndex(figureName) {
        let figureIndex = defaultsFile.figures[figureName];
        if (figureIndex >= 0)
            return figureIndex;
        throw new GisplayError(`Figure with name: ${figureName}, not found.`);
    }

    /**
     * Returns the default color.
     * @static
     * @returns {Array<number>}  - the default color. 
     * @memberof GisplayDefaults
     */
    static getDefaultColor() {
        return defaultsFile.defaultColor;
    }

    /**
     * Returns the index of the default shape.
     * @static
     * @returns {number} - the index of the default shape .
     * @memberof GisplayDefaults
     */
    static getDefaultShapeIndex() {
        return this.findShapeIndex(defaultsFile.defaultShape); //GisplayDefaults.CIRCLE_FULL.name);
    }

    /**
     * Returns the index of the default texture.
     * @static
     * @returns {number} - the index of the default texture.
     * @memberof GisplayDefaults
     */
    static getDefaultTextureIndex() {
        return this.findShapeIndex(defaultsFile.defaultTexture);
    }

    /**
     * Returns the index of the default figure.
     * @static
     * @returns {number} - the index of the default figure.
     * @memberof GisplayDefaults
     */
    static getDefaultFigureIndex() {
        return this.findFigureIndex(defaultsFile.defaultFigure);
    }

    /**
     * Returns the default size of a point for the Gisplay API.
     * @static
     * @returns {number=15} - the default size of a point for the Gisplay API.
     * @memberof GisplayDefaults
     */
    static getDefaultSizeValue() {
        return defaultsFile.defaultSize;
    }

    /**
     * Returns the minimum size for points.
     * @static
     * @returns {number} -  the minimum size for points.
     * @memberof GisplayDefaults
     */
    static getMinSizeValue() {
        return defaultsFile.minSize;
    }

    /**
     * Returns the maximum size for points.
     * @static
     * @returns {number} -  the maximum size for points.
     * @memberof GisplayDefaults
     */
    static getMaxSizeValue() {
        return defaultsFile.maxSize;
    }

    /**
     * Returns the default orientation of a figure or shape in the Gisplay API.
     * @static
     * @returns {number=0} - the default orientation of a figure or shape in the Gisplay API. 
     * @memberof GisplayDefaults
     */
    static getDefaultOrientationValue() {
        return defaultsFile.defaultOrientation;
    }

    /*
    #####################################################################
    #######################     IMAGE DEFAUTLS     ######################
    #####################################################################
    */
    /**
     * Returns the size of the any shape image.
     * @static
     * @returns {number} - the size of the any shape image.
     * @memberof GisplayDefaults
     */
    static getShapeImageSize() {
        return defaultsFile.shapesImageSize;
    }

    /**
     * Returns the size of the any pattern image.
     * @static
     * @returns {number} - the size of the any pattern image.
     * @memberof GisplayDefaults
     */
    static getPatternImageSize() {
        return defaultsFile.patternsImageSize;
    }

    /**
     * Returns the size of the any figure image.
     * @static
     * @returns {number} - the size of the any figure image.
     * @memberof GisplayDefaults
     */
    static getFigureImageSize() {
        return defaultsFile.figuresImageSize;
    }

    /*
    #####################################################################
    #######################     WEBGL DEFAUTLS     ######################
    #####################################################################
    */
    /**
     * The default alpha value for the 
     * @returns 
     * @memberof GisplayDefaults
     */
    static getDefaultAlphaValue() {
        return defaultsFile.defaultAlpha;
    }

    /**
     * Returns the number of bytes for each element of a Float32Array.
     * @static
     * @returns {number} - the number of bytes for each element of a Float32Array.
     * @memberof GisplayDefaults
     */
    static getFloat32BytesPerElement() {
        return Float32Array.BYTES_PER_ELEMENT;
    }

    /**
     * The number of bytes for each element of a Uint8Array (Unsigned int).
     * @static
     * @returns {number} - the number of bytes for each element of a Uint8Array (Unsigned int).
     * @memberof GisplayDefaults
     */
    static getUint8BytesPerElement() {
        return Uint8Array.BYTES_PER_ELEMENTM;
    }

    /**
     * Returns the names of the borders files (vertex and fragment shaders).
     * @static
     * @returns {{borderVertexFileName: string, borderFragmentFileName:string}}
     * @memberof GisplayDefaults
     */
    static getBordersShadersFileNames() {
        return { borderVertexFileName: 'borders.vert', borderFragmentFileName: 'borders.frag' };
    }

    /**
     * Returns the default color for the borders.
     * @static
     * @returns {Array<number>} - the default color for the borders.
     * @memberof GisplayDefaults
     */
    static getDefaultBordersColor() {
        return [0, 0, 0];
    }

    /*
    #####################################################################
    #######################     WEBGL SIZE     ######################
    #####################################################################
    */
    /**
     * The min data name to be used in all shaders that use size as a visual variable.
     * @static
     * @returns {string} - min data name to be used in all shaders that use size as a visual variable.
     * @memberof GisplayDefaults
     */
    static getMinSizeDataName() {
        return 'minData';
    }

    /**
     * The max data name to be used in all shaders that use size as a visual variable.
     * @static
     * @returns {string} - max data name to be used in all shaders that use size as a visual variable.
     * @memberof GisplayDefaults
     */
    static getMaxSizeDataName() {
        return 'maxData';
    }

    static getMinSizePixels() {
        return { name: 'minPixels', value: defaultsFile.minSize };
    }

    static getMaxSizePixels() {
        return { name: 'maxPixels', value: defaultsFile.maxSize };
    }

    /**
     * Returns the type of primitive we want.
     * @static
     * @returns 
     * @memberof GisplayDefaults
     */
    static getPrimitive() {
        return {
            POINT: 0,
            POLYGON: 1,
            LINE: 2,
            CSVPOINT: 3,
            CSVIDS: 4
        };
    }

    /**
     * Returns true if the given primitive is polygon.
     * @static
     * @param {number} geoPrimitive - The identifier of the primitive. 
     * @returns {boolean} - true if the given primitive is polygon.
     * @memberof GisplayDefaults
     */
    static hasPolygons(geoPrimitive) {
        return geoPrimitive === this.getPrimitive().POLYGON;
    }

    /**
     * Returns true if the given primitive is line.
     * @static
     * @param {number} geoPrimitive - The identifier of the primitive. 
     * @returns {boolean} - true if the given primitive is line.
     * @memberof GisplayDefaults
     */
    static hasLines(geoPrimitive) {
        return geoPrimitive === this.getPrimitive().LINE;
    }

    /**
     * Returns true if the given primitive is line.
     * @static
     * @param {number} geoPrimitive - The identifier of the primitive. 
     * @returns {boolean} - true if the given primitive is line.
     * @memberof GisplayDefaults
     */
    static hasPoints(geoPrimitive) {
        return geoPrimitive === this.getPrimitive().POINT;
    }

    /**
     * Returns true if the given primitive is points coming from the CSV Parser.
     * @static
     * @param {number} geoPrimitive - The identifier of the primitive. 
     * @returns {boolean} - true if the given primitive is points coming from the CSV Parser.
     * @memberof GisplayDefaults
     */
    static hasCSVPoints(geoPrimitive) {
        return geoPrimitive === this.getPrimitive().CSVPOINT;
    }

    /*
    #####################################################################
    #######################      WEBGL PICKING     ######################
    #####################################################################
    */
    /**
     * Convert the given RGBA color color to it's respective integer value. 
     * @param {number} r - The red value.
     * @param {number} g - The green value.
     * @param {number} b - The blue value.
     * @param {number} a - The alpha value.
     * @returns {number} - the integer value converted from the given RGBA value.
     * @see https://github.com/mcwhittemore/rgb-to-int/blob/master/index.js
     * @memberof GisplayDefaults
     */
    static RGBAToNumber(r, g, b, a) {
        // return r * Math.pow(256, 3) + g * Math.pow(256, 2) + b * Math.pow(256, 1) + a;
        return r * Math.pow(256, 3) + (g << 16 | b << 8 | a); // g<<16 === g * 2^16
    }

    /**
     * Convert the given number to it's RGBA representation.
     * @static
     * @param {number} num - The number to be converted. 
     * @returns {Array<number>} - the RGBA representation of the given number.
     * @see https://math.stackexchange.com/a/1636055
     * @see https://developer.mozilla.org/pt-PT/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
     * @memberof GisplayDefaults
     */
    static numberToRGBA(num) {
        let r = Math.floor(num / Math.pow(256, 3));
        let g = Math.floor(num / Math.pow(256, 2) % 256);
        let b = Math.floor(num / 256 % 256);
        let a = num - (Math.pow(256, 3) * r + Math.pow(256, 2) * g + 256 * b);
        return [r, g, b, a];
    }

    /**
     * The name for the boolean variable that is used on the shader to know if we want to draw to picking texture or normal draw.
     * @static
     * @returns {string} - he name for the boolean variable that is used on the shader to know if we want to draw to picking texture or normal draw.
     * @memberof GisplayDefaults
     */
    static isPickingName() {
        return 'isPicking';
    }

    /**
     * The name of the variable that holds the RGBA colors used for picking.
     * @static
     * @returns {string} - the name of the variable that holds the RGBA colors used for picking.
     * @memberof GisplayDefaults
     */
    static pickingColorName() {
        return 'pickingColor';
    }

    /**
     * Returns the name of the picking identifier for the MVC/TG.
     * @static
     * @returns {string} - the name of the picking identifier for the MVC/TG.
     * @memberof GisplayDefaults
     */
    static pickingMVCTGIdentifierName() {
        return "MVCTGPickingColor";
    }

    /*
    #####################################################################
    #######################     TIME DEFAUTLS     ######################
    #####################################################################
    */
    /**
     * Returns the instant time variable.
     * @static
     * @returns {string} - the instant time variable.
     * @memberOf GisplayDefaults
     */
    static INSTANT() {
        return defaultsFile.timeVariables.INSTANT;
    }

    /**
     * Returns the interval time variable.
     * @static
     * @returns {string} -  the interval time variable.
     * @memberOf GisplayDefaults
     */
    static INTERVAL() {
        return defaultsFile.timeVariables.INTERVAL;
    }

    /**
     * The animation time variable.
     * @static
     * @returns {string} - the animation time variable.
     * @memberOf GisplayDefaults
     */
    static ANIMATION() {
        return defaultsFile.timeVariables.ANIMATION;
    }

    /*
    #####################################################################
    #######################     DATA NATURE     ######################
    #####################################################################
    */
    /**
     * Returns the sequential data nature.
     * @static
     * @returns {string} - the sequential data nature.
     * @memberof GisplayDefaults
     */
    static SEQUENTIAL() {
        return 'sequential';
    }

    /**
     * Returns the divergent data nature.
     * @static
     * @returns {string} - the divergent data nature.
     * @memberof GisplayDefaults
     */
    static DIVERGENT() {
        return 'divergent';
    }

    /**
     * Returns the qualitative data nature.
     * @static
     * @returns {string} - the qualitative data nature.
     * @memberof GisplayDefaults
     */
    static QUALITATIVE() {
        return 'qualitative';
    }

    /*
    #####################################################################
    #######################     MESSAGES CSV       ######################
    #####################################################################
    */
    static MESSAGES_CSV() {
        return {
            TO_START: 'start', //From TP to Ws to start processing their part
            END_START: 'end_start', //FROM each W to TP flag that the W as terminated its part

            /*  TO_REMAINING_ROWS11: 'remaining_rows', //FROM TP to first W to process remaining rows
             END_REMAINING_ROWS1: 'end_remaining_rows', //FROM first W to TP to flag worker processed remaining rows */

            TO_LIMITS_CAT: 'limits_categories', //FROM TP to Ws to get the min and max and categories for each cat and cont vars 
            END_LIMITS_CAT: 'end_limits_categories', //FROM each W to TP with min/max and categories

            TO_LIMITS_CAT_RES: 'limits_categories_res', //FROM TP to Ws with the resulting classes/categories and time granules
            END_LIMITS_CAT_RES: 'end_limits_categories_res', //FROM each W to TP meaning the W has all the MVCs and TGs in place

            TO_JOIN_DATA: 'join_data', //FROM TP To Ws with information about MVCs and TGs in order to join the final rows
            END_JOIN_DATA: 'end_join_data', //FROM Ws To TP as a flag to receive data

            TO_GENERATE_RGBA: 'generate_rgba',//FROM TP To Ws to generate RGBA information for each row
            END_GENERATE_RGBA: 'end_generate_rgba', //FROM each W to TP meaning the W generated all RGBA values

            TO_SEND_DATA: 'send_data', //FROM TP to one W at a time to receive its data
            END_SEND_DATA: 'end_send_data' //FROM W to TP with the MVCs and TGs created 
        };
    }

    /**
     * Clone a nested array in Javascript.
     * @static
     * @param {Array<any>} arr - The given array. 
     * @returns {Array<any>} - the cloned nested array.
     * @memberof GisplayDefaults
     */
    static cloneNestedArray(arr) {
        let i, copy;
        if (Array.isArray(arr)) {
            copy = arr.slice(0);
            for (let i = 0; i < copy.length; i++)
                copy[i] = this.cloneNestedArray(copy[i]);
            return copy;
        } else if (typeof arr === 'object')
            throw new Error('Cannot clone array containing an object!');
        else
            return arr;
    }
}