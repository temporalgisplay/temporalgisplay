import shadersFile from './Shaders/shaders.json';
import defaultsFile from './defaults.json';
const vertexShadersFiles = require.context("./Shaders", false, /^\.\/.*\.vert$/); //All .vert files
const fragmentShadersFiles = require.context("./Shaders", false, /^\.\/.*\.frag$/); //All .frag files
import shapesImage from '../img/novas.png'; //Shapes
import patternsImage from '../img/patterns.png'; //Patterns
import figuresImage from '../img/figures.png'; //Figures

import loader from '../css/loader.css'; //Gisplay Loader 
import fonts from '../css/fonts.css'; //Fonts to use
import errorModal from '../css/error.css'; //The error modal CSS
import noUiSlider from '../lib/noUiSlider/nouislider.css'; //Time Control Slider CSS

import balloonCSS from '../lib/balloon.css/balloon.css'; //CSS for Tooltips

import { ShadersInfo } from './Shaders/ShadersInfo';

/**
 * Gisplay Dynamic Loader implementation.
 * @export
 * @class GisplayDynamicLoader
 */
export class GisplayDynamicLoader {
    constructor(libraryName, vertexShaderFileName, fragmentShaderFileName) {
        // console.log(libraryName, vertexShaderFileName, fragmentShaderFileName);
        /**
         * The number of expected elements to load.
         * E.g. Mapbox has 1 CSS element and 1 JS element and none was already loaded then we 2 elements to load. 
         * @type {number}
         */
        this.numLibraryElemsToLoad = 0;
        if (!this._isLibraryLoaded(libraryName))
            this.loadLibrary(libraryName);
        else
            this.libraryElementLoaded();

        /**
         * The image with shapes.
         * @type {HTMLImageElement}
         */
        this.shapesImage = this.loadImage(shapesImage, 'shapesImageLoaded');//this.loadShapesImage();
        /**
         * The image with patterns.
         * @type {HTMLImageElement}
         */
        this.patternsImage = this.loadImage(patternsImage, 'patternsImageLoaded');//this.loadPatternsImage();
        /**
         * The image with figures.
         * @type {HTMLImageElement}
         */
        this.figuresImage = this.loadImage(figuresImage, 'figuresImageLoaded');

        /**
         * The information about the shader.
         * @type {ShadersInfo}
         */
        this.shadersInfo = this.loadShadersOptionsFile(vertexShaderFileName, fragmentShaderFileName);
    }

    /**
     * Verifies if the library CSS and JS are loaded into the HTML. If they are then there's no need to load it again
     * so this will return true, otherwise, returns false.
     * @param {string} libraryName - The name of the library (e.g.  Mapbox). 
     * @returns {boolean} - true if the js and css (if any) of the library were already loaded by the user, false, otherwise.
     * @see https://stackoverflow.com/a/37820644
     * @see https://css-tricks.com/snippets/javascript/async-script-loader-with-callback/
     * @memberOf GisplayDynamicLoader
     */
    _isLibraryLoaded(libraryName) {
        let scriptsSource = this.loadDefaultsFile().bgmapsUrls[libraryName].js;
        let stylesSource = this.loadDefaultsFile().bgmapsUrls[libraryName].css;
        if (stylesSource)
            this.numLibraryElemsToLoad += stylesSource.length;
        if (scriptsSource)
            this.numLibraryElemsToLoad += scriptsSource.length;
        // console.warn(this.numLibraryElemsToLoad);
        switch (libraryName) {
            case 'MB':
                return this._hasScript(scriptsSource) && this._hasStyle(stylesSource);
            case 'GM':
                return this._hasScript(libraryName);
            case 'HM':
                {
                    for (let src of scriptsSource)
                        if (!this._hasScript(src))
                            return false;
                    return true;
                }
            case 'BM':
                return this._hasScript(scriptsSource);
        }
    }

    /**
     * Given the script source, verifies if it exists on the DOM or not.
     * @param {string} scriptSource - The source of the script.
     * @returns {boolean} - true, if the script exists in the DOM, false, otherwise.
     * @private
     * @memberof GisplayDynamicLoader
     */
    _hasScript(scriptSource) {
        return document.querySelectorAll(`[src='${scriptSource}']`).length === 1;
    }

    /**
     * Given the source for the stylesheet, verifies if it exists on the DOM or not.
     * @param {string} libraryName - The source of the stylesheet.
     * @returns {boolean} - true, if the style exists in the DOM, false, otherwise.
     * @private
     * @memberof GisplayDynamicLoader
     */
    _hasStyle(styleSource) {
        return document.querySelectorAll(`[href='${styleSource}']`).length === 1;
    }

    /**
     * This method should be used to asynchronously or synchronously load the library for Mapbox, Google etc
     * so this way we won't need to load it on the main html file.
     * @param {string} libraryName - Name of the library to load. 
     * @memberOf LayoutManager
     */
    loadLibrary(libraryName) {
        // console.error("Load Library >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        let scriptsSource = this.loadDefaultsFile().bgmapsUrls[libraryName].js;
        let stylesSource = this.loadDefaultsFile().bgmapsUrls[libraryName].css;
        switch (libraryName) {
            case 'MB':
            case 'MBGL':
                {
                    this._loadStyleSheet(stylesSource[0]);
                    this._loadScript(scriptsSource[0]);
                }
                break;
            case 'GM':
                this._loadScript(scriptsSource[0]);
                break;
            case 'HM':
                this._loadHereMapsRecursive(this.loadDefaultsFile().bgmapsUrls[libraryName].js, this.numLibraryElemsToLoad);
                break;
            case 'BM':
                this._loadScript(scriptsSource[0]);
                break;
        }
    }

    /**
     * Load script dynamically.
     * @param {string} scriptSource - The script source.
     * @private
     * @memberof GisplayDynamicLoader
     */
    _loadScript(scriptSource) {
        let js = document.createElement('script');
        js.src = scriptSource;
        js.onload = (e) => { this.libraryElementLoaded(e); };
        document.head.appendChild(js);
    }

    /**
     * Load stylesheet dynamically.
     * @param {string} styleSheetSource - The stylesheet source. 
     * @private
     * @memberof GisplayDynamicLoader
     */
    _loadStyleSheet(styleSheetSource) {
        let css = document.createElement("link");
        css.rel = "stylesheet";
        css.type = "text/css";
        css.href = styleSheetSource;
        css.onload = (e) => { this.libraryElementLoaded(e); };
        document.head.appendChild(css);
    }

    /**
     * Recursive method to load Here Maps library scripts. This method is recursive because the three scripts for here maps 
     * must be loaded in order.
     * @param {Array<string>} jsFiles -  The src for each of the js files for here maps.
     * @private
     * @see https://stackoverflow.com/a/38841736
     * @memberof GisplayDynamicLoader
     */
    _loadHereMapsRecursive(jsFiles, numberOfScripts) {
        let js = document.createElement('script');
        js.src = jsFiles[0];
        js.onload = (e) => {
            this.libraryElementLoaded(e);
            jsFiles = jsFiles.slice(1, numberOfScripts);
            if (jsFiles.length > 0)
                this._loadHereMapsRecursive(jsFiles);
        };
        document.head.appendChild(js);
    }

    /**
     * Callback for the load event of the CSS or JS library elements.
     * @param {Event} e - The load callback event.
     * @memberOf GisplayDynamicLoader
     */
    libraryElementLoaded(e) {
        // console.error(e);
        if (--this.numLibraryElemsToLoad <= 0) {
            console.warn("All library elements loaded");
            document.dispatchEvent(new CustomEvent("libraryLoaded", {}));
        }
    }

    /**
     * Loads the json file with shaders options.
     * @returns {ShadersInfo} - the json file with shaders options.
     * @memberOf GisplayDynamicLoader
     */
    loadShadersOptionsFile(vertexShaderFileName, fragmentShaderFileName) {
        // console.log(shadersFile, vertexShaderFileName, fragmentShaderFileName);
        let vertexKeys = shadersFile.vertex[vertexShaderFileName];
        let fragmentKeys = shadersFile.fragment[fragmentShaderFileName];
        return new ShadersInfo(vertexKeys, fragmentKeys);
    }

    /**
     * Returns the information about the shader.
     * @returns {ShaderInfo} - the information about the shader.
     * @memberof GisplayDynamicLoader
     */
    getShadersInfo() {
        return this.shadersInfo;
    }

    /**
     * Given the name of the vertex shader and the fragment shader then loads their content into memory.
     * @param {string} vertexShaderFileName - The name of the vertex shader file.
     * @param {string} fragmentShaderFileName - The name of the fragment shader file.
     * @returns {{vertexShaderCode:string, fragmentShaderCode:string}} - the vertex and fragment shaders code.
     * @see https://stackoverflow.com/a/41410938
     * @memberOf GisplayDynamicLoader
     */
    loadShaders(vertexShaderFileName, fragmentShaderFileName) {
        // console.log(vertexShaderFileName, fragmentShaderFileName);
        // this.loadShadersOptionsFile(vertexShaderFileName, fragmentShaderFileName);
        let vertexShaderCode = '';
        let fragmentShaderCode = '';
        for (let vertexShaderName of vertexShadersFiles.keys()) {
            // console.log(vertexShaderName);
            if (vertexShaderName.split('./')[1] === vertexShaderFileName)
                vertexShaderCode = vertexShadersFiles(vertexShaderName);
        }
        if (!vertexShaderCode)
            throw new Error(`The file name: ${vertexShaderFileName}, for the vertex shader wasn\'t  found on the Shaders directory.`);

        for (let fragmenShaderName of fragmentShadersFiles.keys()) {
            // console.log(fragmenShaderName);
            if (fragmenShaderName.split('./')[1] === fragmentShaderFileName)
                fragmentShaderCode = fragmentShadersFiles(fragmenShaderName);
        }
        if (!fragmentShaderCode)
            throw new Error(`The file name: ${fragmentShaderFileName}, for the fragment shader wasn\'t  found on the Shaders directory.`);

        //Create Shaders class based on the fileName
        return { vertexShaderCode, fragmentShaderCode };
    }

    /**
     * Load the given image and then fire the given event.
     * @param {string} srcImage - The src of the image to load. 
     * @param {string} eventToFire - The name of the event to fire after loading the image. 
     * @returns {Image} - the loaded image.
     * @memberof GisplayDynamicLoader
     */
    loadImage(srcImage, eventToFire) {
        let resImage = new Image();
        // console.log(srcImage);
        resImage.src = srcImage;
        resImage.onload = () => { document.dispatchEvent(new CustomEvent(eventToFire, {})); };
        return resImage;
    }

    /**
     * Loads the json file with the default values.
     * @returns {JSON} - the json file with the default values.
     * @memberOf GisplayDynamicLoader
     */
    loadDefaultsFile() {
        return defaultsFile;
    }

    /**
     * Returns the image with the shapes.
     * @returns {HTMLImageElement}
     * @memberOf GisplayDynamicLoader
     */
    getShapesImage() {
        return this.shapesImage;
    }

    /**
     * Returns the image with the patterns.
     * @returns {HTMLImageElement}
     * @memberOf GisplayDynamicLoader
     */
    getPatternsImage() {
        return this.patternsImage;
    }

    /**
     * Returns the image with the figures.
     * @returns {Image} - the image with the figures.
     * @memberof GisplayDynamicLoader
     */
    getFiguresImage() {
        return this.figuresImage;
    }
}