
/**
 * Custom Error to create a popup message to inform the user about a particular problem.
 * @export
 * @class GisplayError
 * @extends {Error}
 * @see https://medium.com/@xjamundx/custom-javascript-errors-in-es6-aa891b173f87
 */
export class GisplayError extends Error {

    /**
     * Creates an instance of GisplayError.
     * @param {string} message - The message to provide to the user.
     * @see https://github.com/codrops/ModalWindowEffects
     * @see https://codepen.io/SMLMRKHLMS/pen/rVYRLQ 
     * @memberof GisplayError
     */
    constructor(message) {
        super(message);
        // Error.captureStackTrace(this, GisplayError);
        console.error("GisplayError");
        // document.getElementById('head').innerHTML = '';

        // this.createErrorDialog(message);
        /*  let modal = document.createElement('div');
         modal.className = 'modal';
         let content = document.createElement('div');
         content.className = 'content';
         content.innerHTML = message;
 
         modal.appendChild(content);
         document.body.appendChild(modal); */

        let modal = document.createElement('div');
        modal.className = 'gisplayModal';

        let overlay = document.createElement('div');
        overlay.className = 'gisplayModalOverlay';

        let content = document.createElement('div');
        content.className = 'gisplayModalContent';

        let contentTitle = document.createElement('div'); //Title
        let errorFoundParagraph = document.createElement('p'); //Paragraph in title
        errorFoundParagraph.innerHTML = 'Error found:';
        contentTitle.appendChild(errorFoundParagraph);
        let contentMessage = document.createElement('p'); //Content message
        contentMessage.innerHTML = message;
        let checkDevTools = document.createElement('button'); //Open devtools button
        checkDevTools.innerHTML = 'Open Developer tools for more information';

        content.appendChild(contentTitle);
        content.appendChild(contentMessage);
        content.appendChild(checkDevTools);

        modal.appendChild(overlay);
        modal.appendChild(content);
        document.body.appendChild(modal);
    }


    createErrorDialog(message) {
        console.log("wut");
        let modal = document.createElement('div');
        modal.className = 'modal';
        let content = document.createElement('div');
        content.className = 'content';
        content.innerHTML = message;

        modal.appendChild(content);
        document.body.appendChild(modal);
    }
}