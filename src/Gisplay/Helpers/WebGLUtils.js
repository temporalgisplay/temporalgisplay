/**
 * Class with static methods that will help with WebGL related stuff(Matrices, web mercator projection and shaders).
 * Always remeber WebGL is column major when reading the matrix code.
 * @see http://ptgmedia.pearsoncmg.com/images/chap3_9780321902924/elementLinks/03fig27.jpg
 * @static 
 * @class WebGLUtils
 */
export class WebGLUtils {
    /**
     * Calculates the scale and offset(X and Y) for the Web Mercator projection.
     * @static
     * @param {number} longitudeCenter - Longitude of the given position.
     * @param {number} latitudeCenter - Latitude of the given position.
     * @param {number} zoom - Current zoom level of the background map.
     * @param {number} tileSize - The size of each tile in the background map. Usually is 256. If different should be given in the API options.
     * @param {number} width - Width of the current canvas.
     * @param {number} height - Height of the current canvas.
     * @returns {{scale: number, offsetX: number, offsetY: number}} - Returns the scale, offsetX and offsetY of the point given using the Web Mercator projection.
     * @see https://bl.ocks.org/enjalot/fb7f3d696167e9b83a72#viewport.js
     * @see https://en.wikipedia.org/wiki/Web_Mercator
     * @memberof WebGLUtils
     */
    static webMercatorProjection(longitudeCenter, latitudeCenter, zoom, tileSize, width, height) {
        // console.log(longitudeCenter, latitudeCenter, zoom, tileSize,  width, height);
        let PI = Math.PI;
        let scale = ((tileSize / 2) / PI) * Math.pow(2, zoom);
        let lambda = longitudeCenter * (PI / 180); // Convert longitude to radians
        let phi = latitudeCenter * (PI / 180); // Convert latitude to radians

        let xCenter = scale * (lambda + PI);
        let yCenter = scale * (PI - Math.log(Math.tan((PI / 4) + (phi / 2))));
        let offsetX = (width / 2) - xCenter;
        let offsetY = (height / 2) - yCenter;

        return { scale: scale, offsetX: offsetX, offsetY: offsetY };
    }

    /**
     * This is the result matrix from the multiplication of M1*M2*M3
     * @static
     * @param {number} scale - The scale calculated with WebMercator projection.
     * @param {number} width - The width of the canvas.
     * @param {number} height - The height of the canvas.
     * @param {number} offsetX - The offsetX calculated with WebMercator projection.
     * @param {number} offsetY - The offsetY calculated with WebMercator projection.
     * @returns {Float32Array} The resulting matrix (M1*M2*M3) in a single matrix to send to WebGL in order to calculate the resulting position.
     * @see Rui's thesis
     * @memberof WebGLUtils
     */
    static finalMatrix(scale, width, height, offsetX, offsetY) {
        let p0 = (2 * Math.PI * scale) / (width * 180);
        let p2 = ((2 * Math.PI * scale) / width) + ((2 * offsetX) / width) - 1;
        let p4 = (2 * scale) / height;
        let p5 = ((2 * offsetY / height) - 1);
        return new Float32Array([
            p0, 0, 0,
            0, p4, 0,
            p2, p5, 1
        ]);
    }

    /**
     * Creates and compiles a shader.
     * @static
     * @param {string} type - Type of shader. Options are: VERTEX_SHADER or FRAGMENT_SHADER;
     * @param {string} source_code - The shader source code.
     * @param {WebGLRenderingContext} gl - Webgl object used by the Map class.
     * @returns {WebGLShader} - The shader(vertex of fragment).
     * @memberof WebGLUtils
     */
    static createAndCompileShader(type, source_code, gl) {
        let shader = gl.createShader(type);
        gl.shaderSource(shader, source_code);
        gl.compileShader(shader);
        console.log(gl.getShaderInfoLog(shader));
        return shader;
    }

    /**
     * Initializes:
     * 1)WebGLProgram, 2) Generates shadders, 3) Attaches shaders to the program, 4) links program, 5) use program.
     * @static
     * @param {{gl: WebGLRenderingContext, program: WebGLProgram}} webgl - The webgl rendering context and program.
     * @param {{vertexShaderCode:string, fragmentShaderCode:string}} shaderSourceCode - The dynamic shaders given by the programmer.
     * @returns {WebGLProgram} - the program with the given shaders attached to it.
     * @memberof WebGLUtils
     */
    static createWebGLProgram(gl, shaderSourceCode) {
        let program = gl.createProgram();

        const vertex_shader = this.createAndCompileShader(gl.VERTEX_SHADER, shaderSourceCode.vertexShaderCode, gl);
        const fragment_shader = this.createAndCompileShader(gl.FRAGMENT_SHADER, shaderSourceCode.fragmentShaderCode, gl);

        gl.attachShader(program, vertex_shader);
        gl.attachShader(program, fragment_shader);

        gl.linkProgram(program);
        gl.useProgram(program);
        return program;
    }

    /**
     * Sets up webgl.
     * @static
     * @param {HTMLCanvasElement} canvas - The canvas element.
     * @param {{width:number, height: number}} bgmapDivSizes - The width and height of the background maps div. 
     * @param {{vertexShaderCode:string, fragmentShaderCode:string}} shaderSourceCode - The code for the vertex and fragment shaders.
     * @param {{vertexShaderCode:string, fragmentShaderCode:string}} bordersSourceCode - The code for the vertex and fragment shaders for the borders.
     * @returns {{gl: WebGLRenderingContext, program: WebGLProgram, bordersProgram: WebGLProgram}} - The webgl rendering context and program. 
     * @memberof WebGLUtils
     */
    static setupWebGL(canvas, bgmapDivSizes, shaderSourceCode, bordersSourceCode) {
        let webgl = {
            gl: null,
            program: null,
            bordersProgram: null
        };
        webgl.gl = canvas.getContext("webgl2");
        webgl.gl.viewport(0, 0, bgmapDivSizes.width, bgmapDivSizes.height);
        webgl.gl.disable(webgl.gl.DEPTH_TEST);
        
        webgl.program = this.createWebGLProgram(webgl.gl, shaderSourceCode);
        webgl.bordersProgram = this.createWebGLProgram(webgl.gl, bordersSourceCode);

        window.canvas = canvas;

        return webgl;
    }
}