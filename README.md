# **Temporal Gisplay** 

The Temporal Gisplay API is a client side API to visualize thematic maps on the Web, using Javascript and WebGL 2, that is able to deal with datasets with over a million points.

Temporal Gisplay has the following special characteristics:

* Interaction with zoom and pan operations.
* Filter data with legend, with multiple available legends.
* Multiple projections, which enable the simultaneous visualization of spatial region physically separated like Portugal and it's islands. 
* Multiple background maps, which is the only component that needs a connection with a server.
* Temporal data with a temporal controller.
![Temporal Controller](https://i.imgur.com/j34bqS9.png)

Right now, the Temporal Gisplay has the following thematic maps:

1. [Choropleth](docs/choropleth.md)
2. [DotMap](docs/dotmap.md)
3. [Proportional Symbols](docs/propsymbols.md)
4. [Figures Map](docs/figuresmap.md)
5. [Lines Map](docs/linesmap.md)

---

To use the Temporal Gisplay API, the programmer has a set of options available. The API tries to provide the best defaults in terms of cartography and data visualization based on the current best practices. 

[Options explained](docs/options.md)

---

## Installation

## Code Documentation 